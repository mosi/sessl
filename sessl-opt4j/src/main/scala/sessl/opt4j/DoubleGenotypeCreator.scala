/*
 * Copyright 2016 University of Rostock
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package sessl.opt4j

import org.opt4j.core.genotype.{DoubleBounds, DoubleGenotype}
import org.opt4j.core.problem.Creator
import sessl.optimization.BoundedSearchSpaceDimension

class DoubleGenotypeCreator extends Creator[DoubleGenotype] {
  val rng = Opt4JSetup.createRNG()

  override def create(): DoubleGenotype = {

    // check that all dimensions of the search space are specified with doubles
    require(Opt4JSetup.searchSpace.forall(dim => {
        dim.isInstanceOf[BoundedSearchSpaceDimension[_]] && {
          val d = dim.asInstanceOf[BoundedSearchSpaceDimension[_]]
          d.lowerBound.isInstanceOf[Double] &&
          d.upperBound.isInstanceOf[Double] &&
          d.stepSize.isInstanceOf[Double]
        }

      }),
      "Double Genotype Creator can only be used with double search space")

    // we checked the types above
    val lowerBounds = Opt4JSetup.searchSpace map {
      case x: BoundedSearchSpaceDimension[_] =>
        x.lowerBound.asInstanceOf[Double]
    }
    val upperBounds = Opt4JSetup.searchSpace map {
      case x: BoundedSearchSpaceDimension[_] =>
        x.upperBound.asInstanceOf[Double]
    }
    val genotype = new DoubleGenotype(new DoubleBounds(lowerBounds.toArray, upperBounds.toArray))
    genotype.init(rng, Opt4JSetup.searchSpace.size)
    genotype
  }
}
