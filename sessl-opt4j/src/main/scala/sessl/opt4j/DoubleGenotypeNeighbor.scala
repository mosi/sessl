/*
 * Copyright 2016 University of Rostock
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package sessl.opt4j

import com.google.inject.Inject
import org.opt4j.core.Genotype
import org.opt4j.core.common.random.Rand
import org.opt4j.core.genotype.DoubleGenotype
import org.opt4j.operators.neighbor.Neighbor
import org.opt4j.operators.normalize.NormalizeDouble

class DoubleGenotypeNeighbor @Inject()(searchSpace: SearchSpace, random: Rand, normalize: NormalizeDouble) extends Neighbor[Genotype] {
  override def neighbor(genotypeGeneric: Genotype): Unit = {
    val genotype = genotypeGeneric.asInstanceOf[DoubleGenotype]
    require(genotype.size() == searchSpace.searchSpace.size, "Genotype size and search space not the same")
    val j = random.nextInt(genotype.size())
    for(i <- 0 until genotype.size()) {
      if(random.nextDouble() < 1.0/genotype.size() || i == j) {
        val bounds = searchSpace.searchSpace(i)
        val offset = bounds.stepSize * (if (random.nextDouble() < 0.5) -1 else 1)
        val next = genotype.get(i) + offset
        genotype.set(i, next)
      }
    }
    normalize.normalize(genotype)
  }
}
