/*
 * Copyright 2016 University of Rostock
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package sessl.opt4j

import com.google.inject.{AbstractModule, TypeLiteral}
import org.opt4j.core.Genotype
import org.opt4j.operators.neighbor.Neighbor

object DoubleNeighborModule extends AbstractModule {
  object neighborLit extends TypeLiteral[Neighbor[Genotype]]
  override def configure(): Unit = {
    bind(neighborLit).to(classOf[DoubleGenotypeNeighbor])
  }
}
