/*
 * Copyright 2017 University of Rostock
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package sessl.examples

import sessl.mlrules.{Experiment, HybridSimulator, Observation}

/**
  * @author Tom Warnke
  */
object BifurcationAnalysisExperiment extends App {

  import sessl._
  import sessl.analysis.bifurcation._

  analyze((setup, objective) =>
    execute(new Experiment with Observation {

      model = this.getClass.getResource("/bistable.mlrj").toURI
      simulator = HybridSimulator()
      replications = 1
      stopCondition =
        SchrubensSteadyStateEstimator("A",
                                      interval = 1.0,
                                      allowedSignificance = 0.0001)

      val (param, predObs, initialState) = setup

      set("k" <~ param)
      set("beta" <~ predObs)
      set("init" <~ initialState)

      val steady_state = observe(steadyState)
      val final_state = observe(finalState)

      withReplicationsResult { result =>
        val means = result[Double](steady_state)
        objective <~ ((param, means.sum / means.size,
                       result(final_state).head.toString))
      }

    })) using new BifurcationAnalysisSetup {
    initialState = "200.0 A + 1.0 Null"
    initialParameterValue = -0.5
    initialObservedValue = 0.0
    parameterGenerator = new ParameterGenerator {
      val params: Iterator[Double] =
        (Range.BigDecimal(-0.5, 0.5, 0.1).toList ++ Range
          .BigDecimal(0.5, -0.5, -0.1)).toIterator.map(_.toDouble)

      override def nextParamAndObs(param: Double,
                                   obs: Double): Option[(Double, Double)] = {
        if (params.hasNext)
          Some((params.next(), 0.0))
        else
          None
      }
    }
    withAnalysisResult(result => println(result.mkString("\n")))
  }
}
