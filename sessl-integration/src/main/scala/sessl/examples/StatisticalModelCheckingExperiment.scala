/*
 * Copyright 2017 University of Rostock
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package sessl.examples

import sessl._
import sessl.verification._
import sessl.verification.mitl._

/**
  * @author Tom Warnke
  */
object StatisticalModelCheckingExperiment extends App {

  import sessl.ml3._

  sessl.execute {
    new Experiment with Observation with StatisticalModelChecking {

      model = this.getClass.getResource("/sir.ml3").toURI
      simulator = NextReactionMethod()

      set("a" <~ 0.03)
      set("b" <~ 0.05)

      stopTime = 200

      val s = 950 * "new Person(status := 'susceptible')"
      val i = 50 * "new Person(status := 'infected')"
      initializeWith(s + "," + i) += BarabasiAlbert("Person", "network", 5)

      observeAt(range(0, 1, 200))
      val inf = observe(agentCount("Person", "ego.status = 'infected'"))

      test = SequentialProbabilityRatioTest(p = 0.8,
        alpha = 0.05,
        beta = 0.05,
        delta = 0.05)

      prop = MITL(
        // the number of infected people is above 400 at least once between times 20 and 30,
        // but reaches 0 between 120 and 180 time units after that
        F(0, 30)(
          (OutVar(inf) > Constant(400)).U(0, 0)(
            F(120, 180)(OutVar(inf) === Constant(0)))
        )
      )

      withCheckResult { result =>
        println(result.satisfied)
      }
    }
  }
}
