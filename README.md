# SESSL: Simulation Experiment Specification via a Scala Layer

[![pipeline status](https://git.informatik.uni-rostock.de/mosi/sessl/badges/develop/pipeline.svg)](https://git.informatik.uni-rostock.de/mosi/sessl/commits/develop)

- Development requirements:
  - Maven 3 (http://maven.apache.org/)
- Compile and run all tests with: mvn test
- License: Apache 2.0
- For more information, a quick start project, and usage examples see [sessl.org](http://sessl.org/)
