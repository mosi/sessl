/*
 * Copyright 2013 Roland Ewald
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package sessl.ssj

import org.junit.Assert._
import org.junit.Test
import sessl._
import sessl.reference.MockExperiment
import sessl.util.Logging
import umontreal.ssj.functions.MathFunction

/**
 * Tests for {@link SSJOutputAnalysis}.
 *
 * @author Roland Ewald
 */
@Test class SSJOutputFittingTest extends Logging {

  def applyFunc(f: MathFunction, times: Iterable[Double]): Iterable[Double] = times.map(f.evaluate(_))

  @Test def testLeastSquaresPolyNomFitting(): Unit = {
    var linearFit: Option[Array[Double]] = None
    var quadraticFit: Option[Array[Double]] = None
    sessl.execute {
      new SSJTestExperiment {
        withRunResult {
          result =>
            {
              val tr = result.trajectory(y)
              linearFit = Some(fit(result.trajectory(y), Polynomial(1)).getCoefficients)
              quadraticFit = Some(fit(result.trajectory(y), Polynomial(2)).getCoefficients)
              logger.info("Trajectory to be fitted:" + tr)
              logger.info("Quadratic polynom fitted with least-squares evaluated for trajectory:" + applyFunc(fit(tr, Polynomial(2)), result.times(y)))
            }
        }
      }
    }
    assertTrue("Both fits should be defined.", linearFit.isDefined && quadraticFit.isDefined)
    assertEquals("Two coefficients define f(x) = a*x+b", 2, linearFit.get.size)
    assertEquals("Three coefficients define f(x) = a*x^2+b*x+c", 3, quadraticFit.get.size)
  }

  @Test def testBSplineFitting(): Unit = {
    var bSpline: Option[MathFunction] = None
    var approxBSpline: Option[MathFunction] = None
    sessl.execute {
      new SSJTestExperiment {
        withRunResult {
          result =>
            {
              val tr = result.trajectory(y)
              bSpline = Some(fit(tr, BSpline(2)))
              approxBSpline = Some(fit(tr, ApproximatedBSpline(2, 3)))
              logger.info("Trajectory to be fitted:" + tr)
              logger.info("BSpline approximation for this trajectory:" + applyFunc(approxBSpline.get, result.times(y)))
            }
        }
      }
    }
    List(bSpline, approxBSpline).map(x => assertTrue(x.isDefined))
  }

  @Test def testCubicSplineFitting(): Unit =  {
    var cubicSpline: Option[MathFunction] = None
    sessl.execute {
      new SSJTestExperiment {
        withRunResult {
          result =>
            {
              val tr = result.trajectory(y)
              cubicSpline = Some(fit(tr, SmoothingCubicSpline(.5)))
              logger.info("Trajectory to be fitted:" + tr)
              logger.info("Cubic spline approximation for this trajectory:" + applyFunc(cubicSpline.get, result.times(y)))
            }
        }
      }
    }
    assertTrue(cubicSpline.isDefined)
  }

}

object SSJOutputFittingTest {
  val observationRange = range(.0, .1, .9)
}

/** The test experiment to be used to generate some data. */
class SSJTestExperiment extends MockExperiment with SSJOutputAnalysis {
  stopTime = 1.0
  scan("numOfSpecies" <~ (10, 15))
  val y = observe(t => math.pow(2.0, t))
  observeAt(SSJOutputFittingTest.observationRange)
}