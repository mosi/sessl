/*
 * Copyright 2017 University of Rostock
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package sessl.ssj

import org.junit.Assert._
import org.junit.Test
import sessl._
import sessl.reference._

/**
  * @author Tom Warnke
  */
@Test class LatinHyperCubeTest {

  @Test def testDifferentNumericTypes(): Unit = {

    val exp = new Experiment with LHCSampling {

      lhc(5)("x" <~ interval(0, 5), "y" <~ interval(0.0, 10.0), "z" <~ interval(5.0f, 25.0f))

      assertEquals(5, createVariableSetups().size)

    }
  }

  @Test def testSeveralIterations(): Unit = {

    val exp = new Experiment with LHCSampling {

      lhc(5, iterations = 3)("x" <~ interval(0, 5l), "y" <~ interval(0.0, 10.0), "z" <~ interval(5.0f, 25.0f))

      assertEquals(15, createVariableSetups().size)

    }
  }
}
