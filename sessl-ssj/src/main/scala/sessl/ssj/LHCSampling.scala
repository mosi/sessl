/*
 * Copyright 2017 University of Rostock
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package sessl.ssj

import sessl.{AbstractExperiment, MultipleVars, VarInterval, VarSeq, Variable}
import umontreal.ssj.hups.LatinHypercube
import umontreal.ssj.rng.{BasicRandomStreamFactory, MRG32k3a}

/**
  * @author Tom Warnke
  */
trait LHCSampling extends AbstractExperiment {

  /** Use Latin Hypercube sampling repeatedly to create a number of parameter configuration for the given model
    * parameters.
    *
    * @example {{{
    *   lhc(3)("x" <~ interval(0, 5), "y" <~ interval(0.0, 10.0)) // samples 3 points in the parameter space
    *  	lhc(3, iterations = 5)("x" <~ interval(0, 5), "y" <~ interval(0.0, 10.0)) // samples 15 points in the parameter space
    *  }}}
    *
    * */
  def lhc(numPoints: Int, iterations: Int = 1)(vars: Variable[_]*): Unit = {

    // check and convert input
    require(
      vars.forall(_.isInstanceOf[VarInterval[_]])
    )
    val variables = vars map (_.asInstanceOf[VarInterval[_]])

    // sample values via lhc
    val lhcs = for {
      _ <- 0 until iterations
    } yield sample(numPoints, variables)

    // concatenate lhcs
    // this yields lists with all values for *one* parameter
    val mergedLhcs = lhcs.transpose.map {vs: Seq[VarSeq[Double]] =>
      val v = VarSeq(vs.head.name, vs.flatMap(_.values))
      MultipleVars(List(v))
    }

    // combine everything so that the first value of the first variable, the first value of the second variable etc.
    // will be used to generate one configuration
    val allConfigurations = mergedLhcs.fold(MultipleVars(List()))(_ and _)

    scan(allConfigurations)
  }

  private def translate(variable: VarInterval[_], fraction: Double): Double =
    (variable.to - variable.from) * fraction + variable.from

  /** Create a mapping of each variable to a number of values of that variable */
  private def sample(numPoints: Int, vars: Seq[VarInterval[_]]): Seq[VarSeq[Double]] = {
    // construct the latin hypercube
    val ssjLHC = new LatinHypercube(numPoints, vars.size)
    val rndStream = new BasicRandomStreamFactory(classOf[MRG32k3a]).newInstance
    ssjLHC.randomize(rndStream)

    // transpose lhc array of coordinates
    // each valuation has a value in (0, 1) for each variable
    // that means that their order doesn't matter
    val valuations = ssjLHC.getArray.transpose

    val configurations = vars zip valuations map { case (variable: VarInterval[_], fractions) =>
      // calculate points in the interval
      val valuesInInterval = fractions map (f => translate(variable, f))
      VarSeq(variable.name, valuesInInterval)
    }

    configurations
  }
}
