/*
 * Copyright 2013 Roland Ewald
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package sessl.ssj

import sessl.AbstractExperiment
import sessl.AbstractReport
import sessl.BoxPlotView
import sessl.DataView
import sessl.ExperimentResults
import sessl.HistogramView
import sessl.LinePlotView
import sessl.ReportNode
import sessl.ReportSectionNode
import sessl.ScatterPlotView
import umontreal.ssj.charts.HistogramChart
import umontreal.ssj.charts.ScatterChart
import umontreal.ssj.charts.XYChart
import umontreal.ssj.charts.XYLineChart
import umontreal.ssj.charts.BoxChart
import umontreal.ssj.functions.MathFunction
import sessl.Trajectory

/**
 * Support for SSJ-based charts generated as reports.
 *
 * @author Roland Ewald
 */
trait Report extends AbstractReport {
  this: AbstractExperiment =>

  def reportFit[M <: MathFunction](data: (String, Trajectory[_]), af: ApproximationForm[M], sectionName: String = "") = {
    val secName = if (sectionName.isEmpty) "Fit for '" + data._1 + "' using " + af else sectionName
    val fitData = (af.toString, SSJOutputAnalysis.fitAndEval(data._2, af))
    reportSection(secName) {
      linePlot(data, fitData)(title = secName)
    }
  }

  override def generateReport(results: ExperimentResults): Unit = {
    topmostElements.foreach(e => createView(reportName, e))
  }

  /** Creates top-most elements of report (adds dummy sections to data views on top-most level). */
  def createView(name: String, node: ReportNode): Unit = node match {
    case section: ReportSectionNode => {
      section.children.foreach(e => createView(name + "-" + section.name, e))
      if (!section.description.isEmpty)
        logger.warn("Can't display sections, omitting description '" + section.description + "'")
    }
    case view: DataView => createChart(name, view)
    case _ => throw new IllegalArgumentException("Element " + node + " not supported.")
  }

  /** Creates the charts, stores them to .tex files. */
  private[this] def createChart(name: String, view: DataView): Unit = {

    import sessl.util.ScalaToJava._

    /** Storage function. */
    def store(c: XYChart) = c.toLatexFile(name + ".tex", 12, 8)

    view match {
      case v: ScatterPlotView =>
        store(
          new ScatterChart(v.title, v.xLabel, v.yLabel, Array(v.xData.toArray, v.yData.toArray)))
      case v: HistogramView =>
        store(
          new HistogramChart(v.title, v.xLabel, v.yLabel, v.data.toArray))
      case v: LinePlotView =>
        store(
          new XYLineChart(v.title, v.xLabel, v.yLabel, v.data.map(_._2.toArray).toArray))
      case _ => throw new IllegalArgumentException("Data view " + view + " not yet supported.")
    }
  }

}