/*
 * Copyright 2013 Roland Ewald
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package sessl.ssj

import sessl.AbstractExperiment
import sessl.ExperimentConfiguration
import sessl.Trajectory
import umontreal.ssj.functions.MathFunction

/**
 * Provides support for output analysis with SSJ.
 *
 * See SSJ manual for documentation: http://www.iro.umontreal.ca/~simardr/ssj/doc/pdf/guidefunctionfit.pdf
 *
 * @author Roland Ewald
 */
trait SSJOutputAnalysis extends ExperimentConfiguration {
  this: AbstractExperiment =>

  def fit[M <: MathFunction](data: Trajectory[_], af: ApproximationForm[M]): M = SSJOutputAnalysis.fit(data, af)

  def fitAndEval[M <: MathFunction](data: Trajectory[_], af: ApproximationForm[M]): Trajectory[_] = SSJOutputAnalysis.fitAndEval(data, af)

}

object SSJOutputAnalysis {

  /** Fits a function to the given data. */
  def fit[M <: MathFunction](data: Trajectory[_], af: ApproximationForm[M]): M = af.fitToData(data)

  /** Fits a function to the given data, evaluates it for the given trajectories' time points. */
  def fitAndEval[M <: MathFunction](data: Trajectory[_], af: ApproximationForm[M]): Trajectory[_] = {
    val fitted = fit(data, af)
    val times = data.map(_._1)
    times.map(t => (t, fitted.evaluate(t)))
  }

}