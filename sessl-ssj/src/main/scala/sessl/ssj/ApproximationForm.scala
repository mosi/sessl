/*
 * Copyright 2013 Roland Ewald
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package sessl.ssj

import sessl.Trajectory
import umontreal.ssj.functionfit.LeastSquares
import umontreal.ssj.functions.MathFunction
import sessl.Misc

/**
 * Constructs to represent approximation forms.
 * @author Roland Ewald
 */
trait ApproximationForm[+M <: MathFunction] {

  /** Needs to be implemented by the approximation form. */
  def approximate(times: Array[Double], values: Array[Double]): M

  /** Converts and checks SESSL data for usage, the fits approximation form. */
  def fitToData(data: Trajectory[_]): M = {
    Misc.requireNumericTrajectories(data)
    val converted = convertValues(data)
    approximate(converted._1, converted._2)
  }

  /** Convert trajectory to tuple of double[] (times + values).*/
  private[this] def convertValues(t: Trajectory[_]): (Array[Double], Array[Double]) =
    (t.map(_._1).toArray, t.map(_._2.asInstanceOf[Number].doubleValue).toArray)
}

case class Polynomial(degree: Int) extends ApproximationForm[umontreal.ssj.functions.Polynomial] {
  override def approximate(times: Array[Double], values: Array[Double]) =
    new umontreal.ssj.functions.Polynomial(LeastSquares.calcCoefficients(times, values, degree): _*)
}

case class BSpline(degree: Int) extends ApproximationForm[umontreal.ssj.functionfit.BSpline] {
  override def approximate(times: Array[Double], values: Array[Double]) = umontreal.ssj.functionfit.BSpline.createInterpBSpline(times, values, degree)
}

case class ApproximatedBSpline(degree: Int, h: Int) extends ApproximationForm[umontreal.ssj.functionfit.BSpline] {
  override def approximate(times: Array[Double], values: Array[Double]) = umontreal.ssj.functionfit.BSpline.createApproxBSpline(times, values, degree, h)
}

case class SmoothingCubicSpline(rho: Double) extends ApproximationForm[umontreal.ssj.functionfit.SmoothingCubicSpline] {
  require(rho >= 0 && rho <= 1, "Parameter rho needs to be in [0,1].")
  override def approximate(times: Array[Double], values: Array[Double]) = new umontreal.ssj.functionfit.SmoothingCubicSpline(times, values, rho)
}

