/*
 * Copyright 2017 University of Rostock
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package sessl.mlrules

import org.jamesii.mlrules.experiment.{Job, SimulationRun, Experiment => MLRulesExperiment}
import org.jamesii.mlrules.model.Model
import org.jamesii.mlrules.observation.{Instrumenter, Observer}
import sessl._

import scala.collection.JavaConverters._
import scala.collection.mutable.ListBuffer
import scala.concurrent.{Future, Promise}

/**
  * @author Tom Warnke
  */
class Experiment extends AbstractExperiment with DynamicSimulationRuns with StopConditions {

  type ObserverGenerator = (Job, Model) => Observer

  override type RunOutput = Unit

  /** default: use one core if not set otherwise.
    * Can be overwritten by the ParallelExecution trait */
  private[mlrules] var parallelThreadsToUse = 1

  /** list of functions to create observers for a job. can be filled by the
    * observation trait */
  private[mlrules] val observerGenerators: ListBuffer[ObserverGenerator] = ListBuffer()

  private[this] var experiment: MLRulesExperiment = _

  override protected def basicConfiguration(): Unit = {}

  /** All variables assignments specified */
  override lazy val variableAssignments: Seq[Map[String, AnyRef]] = {

    // create assignments from variables to scan and variables to set
    val assignments: Seq[Map[String, AnyRef]] = super.variableAssignments

    // convert integer assignments to double
    // convert assignment values to AnyRef (equivalent to Object)
    assignments.map(assignment => assignment.mapValues {
      case i: Integer => i.toDouble.asInstanceOf[AnyRef]
      case other => other
    })
  }


  /** Called to execute the experiment. */
  override protected[sessl] def executeExperiment(): Unit = {

    setMLRulesLoggingLevelToWarning()

    /* use standard simulator as default */
    if (simulators.isEmpty)
      simulator = StandardSimulator()

    experiment = new MLRulesExperiment(
      modelLocation.get,
      simulator.asInstanceOf[MLRulesSimulator].get,
      SESSLInstrumenter,
      simulatorStopConditionFactory,
      parallelThreadsToUse)

    startBlocking()

    logger.info("All jobs finished.")
  }

  override def startSimulationRun(runId: Int, seed: Long, assignment: Map[String, AnyRef]): Future[Unit] = {
    val promise = Promise[Unit]()
    val assignmentWithSeed = assignment.updated(Model.RNG, seed.asInstanceOf[AnyRef])
    experiment.addJob(new Job(runId, assignmentWithSeed.asJava) {
      override def onFailure(t: Throwable): Unit = fail(t)

      override def onSuccess(run: SimulationRun): Unit = promise.success(())
    })
    promise.future
  }

  afterExperiment { _ =>
    experiment.finish()
  }

  /** Instrumenter singleton */
  object SESSLInstrumenter extends Instrumenter {

    /** If observation is configured and the observation trait created
      * variable observers, these are returned.
      * */
    override def create(job: Job, model: Model): java.util.Set[Observer] = {
      val observers = for (observerGenerator <- observerGenerators)
        yield observerGenerator(job, model)
      observers.toSet.asJava
    }
  }

  def setMLRulesLoggingLevelToWarning(): Unit = {
    import java.util.logging.{Level, Logger}
    Logger.getGlobal.setLevel(Level.WARNING)
  }
}

