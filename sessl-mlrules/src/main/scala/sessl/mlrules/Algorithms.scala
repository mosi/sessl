/*
 * Copyright 2017 University of Rostock
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package sessl.mlrules

import org.jamesii.mlrules.simulator.factory._
import sessl.util.CreatableFromVariables
import sessl.{Integrator, Simulator}

trait MLRulesSimulator extends Simulator {
  def get:SimulatorFactory
}

case class StandardSimulator(useDependencyGraph: Boolean = true, useRuntimeCompilation: Boolean = false) extends MLRulesSimulator with CreatableFromVariables[StandardSimulator] {
  override def get: SimulatorFactory = new StandardSimulatorFactory(useDependencyGraph, useRuntimeCompilation)
}

case class SimpleSimulator(useDependencyGraph: Boolean = true, useRuntimeCompilation: Boolean = false) extends MLRulesSimulator with CreatableFromVariables[SimpleSimulator] {
  override def get: SimulatorFactory = new SimpleSimulatorFactory(useDependencyGraph, useRuntimeCompilation)
}

case class AdvancedStaticSimulator() extends MLRulesSimulator with CreatableFromVariables[AdvancedStaticSimulator] {
  override def get: SimulatorFactory = new AdvancedStaticSimulatorFactory()
}

case class LinkSimulator(useDependencyGraph: Boolean = true, useRuntimeCompilation: Boolean = false) extends MLRulesSimulator with CreatableFromVariables[LinkSimulator] {
  override def get: SimulatorFactory = new LinkSimulatorFactory(useDependencyGraph, useRuntimeCompilation)
}

case class HybridSimulator(integrator: HybridSimIntegrator = DormandPrince(),
                           epsilonMin: Double = 0.01,
                           epsilonMax: Double = 0.03,
                           initialQuantile: Double = 0.03,
                           fixedQuantile: Boolean = true,
                           useRuntimeCompilation: Boolean = false)
        extends MLRulesSimulator with CreatableFromVariables[HybridSimulator] {
  override def get: SimulatorFactory = new HybridSimulatorFactory(
      integrator.get,
      epsilonMin,
      epsilonMax,
      initialQuantile,
      fixedQuantile,
      useRuntimeCompilation)
}

case class TauLeapingSimulator(useRuntimeCompilation: Boolean = false) extends MLRulesSimulator with CreatableFromVariables[TauLeapingSimulator] {
  override def get: SimulatorFactory = new TauLeapingSimulatorFactory(useRuntimeCompilation)
}

trait HybridSimIntegrator extends Integrator {
  def get: HybridSimulatorFactory.Integrator
}

case class DormandPrince() extends HybridSimIntegrator {
  override def get: HybridSimulatorFactory.Integrator =
    HybridSimulatorFactory.Integrator.DORMAND_PRINCE
}

case class GraggBulirschStoer() extends HybridSimIntegrator {
  override def get: HybridSimulatorFactory.Integrator =
    HybridSimulatorFactory.Integrator.GRAGG_BULIRSCH_STOER
}