/*
 * Copyright 2017 University of Rostock
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package sessl.mlrules

import java.util.Optional
import java.{lang, util}

import org.jamesii.mlrules.experiment.Job
import org.jamesii.mlrules.model.Model
import org.jamesii.mlrules.observation._
import org.jamesii.mlrules.simulator.{Simulator => MLRSimulator}
import org.jamesii.mlrules.util.Pair
import sessl.AbstractObservation.Observable
import sessl.util.SimpleObservation

import scala.collection.JavaConverters._

/**
  * A trait for observing outputs of an ML-Rules simulation.
  * Currently, four types of observations are supported:
  * <ul>
  *   <li> Counting entities with `count`
  *   <li> Obtaining the distribution of an attribute with `attribute`
  *   <li> If using certaing steady state stop conditions, the `steadyState` value can be observed
  *   <li> The `finalState` can be observed (as a string)
  * </ul>
  * The former two types of observations are recorded at every observation time specified with `observeAt`.
  * The latter two types of observations are recorded at the end of a simulation run.
  *
  * @example
  * {{{
  * val exp = new Experiment with Observation {
  *
  *   // observe how many X entities are in the model
  *   observe("numX" ~ count("X"))
  *
  *   // observe the distribution of the first attribute of all Y entities in the model
  *   observe("attOfY" ~ attribute("Y", 0)
  *
  *   // simulation runs until the count of A entities reaches a steady state
  *   stopCondition = SchrubensSteadyStateEstimator("A", interval = 1.0)
  *   // observe the value of the steady state (of the count of A entities)
  *   observe("steadyStateValue" ~ steadyState)
  *
  *   // observe the final state of the simulation run as a string
  *   observe("finalStateString" ~ finalState)
  * }
  * }}}
  *
  * @author Tom Warnke
  */
trait Observation extends SimpleObservation with SteadyStateObservation with FinalStateObservation {
  this: Experiment =>

  private case class CountObservable(spec: String) extends Observable[Double]
  private case class AttributeObservable[T](spec: String, attr: Int) extends Observable[Map[String, T]]

  def count(spec: String): Observable[Double] = CountObservable(spec)

  def attribute[T](spec: String, attr: Int): Observable[Map[String, T]] = AttributeObservable(spec, attr)

  /**
    * Setting the flags that control a proper call hierarchy, calling event handlers if installed.
    */
  override def configure(): Unit = {
    super.configure()

    // a function to create a new observer for a run id
    def generateObserver: ObserverGenerator = (job: Job, _: Model) => {

      val observer = newTimePointListObserver

      val listeners = observables.collect {
        case obs: CountObservable => newSpeciesCountListener(job.getID, obs)
        case obs: AttributeObservable[_] => newAttributeListener(job.getID, obs)
      }

      for (l <- listeners) observer.register(l)

      observer
    }

    // prepend the defined function to the observer generators
    observerGenerators += generateObserver
  }

  // wrap observer - conversion to java types
  def newTimePointListObserver: TimePointListObserver = {
    val timesArrayList = new util.ArrayList[lang.Double]()
    timesArrayList.addAll(observationTimes.map(Double.box).asJava)
    new TimePointListObserver(timesArrayList)
  }

  // wrap listeners
  def newAttributeListener[T](runId: Int, obs: AttributeObservable[T]): AttributeListener =
    new AttributeListener(obs.spec, obs.attr, (t: Pair[lang.Double, util.Map[String, AnyRef]]) =>
      addValueFor(runId, obs, (t.fst(), t.snd().asScala.toMap.map {
        case (id, value) => (id, value.asInstanceOf[T])
      })))

  def newSpeciesCountListener(runId: Int, obs: CountObservable): SpeciesCountListener =
    new SpeciesCountListener(obs.spec, (t: Pair[java.lang.Double, java.lang.Double]) =>
      addValueFor(runId, obs, (t.fst(), t.snd()))
    )

  protected class SESSLRunFinishedObserver extends Observer {
    override def update(simulator: MLRSimulator): Unit = {}

    override def nextObservationPoint(): Optional[java.lang.Double] = java.util.Optional.empty()
  }
}
