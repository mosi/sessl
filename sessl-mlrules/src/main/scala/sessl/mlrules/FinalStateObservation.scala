/*
 * Copyright 2018 University of Rostock
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package sessl.mlrules

import org.jamesii.mlrules.experiment.stop.StopCondition
import org.jamesii.mlrules.observation.{Listener, Observer}
import sessl.AbstractObservation.Observable

/**
  * @author Tom Warnke
  */
trait FinalStateObservation {
  this: Experiment with Observation =>

  private[this] val finalStateObservable = new Observable[String] {}

  lazy val finalState: Observable[String] = {
    val obs: ObserverGenerator = (job, model) => {

      def cleanStateString(state: String): String = {
        if(!state.startsWith("$$ROOT$$[") || !state.endsWith("]")) {
          throw new IllegalStateException("Malformed state string")
        }
        state.substring(9, state.length - 1)
      }

      class FinalStateListener(runId: Int) extends Listener {

        override def isActive: Boolean = true

        override def notify(observer: Observer): Unit = {}

        override def finish(stopCondition: StopCondition): Unit = {
          val stateString = model.getSpecies.toString
          addValueFor(runId, finalStateObservable, (0.0, cleanStateString(stateString.trim())))
        }
      }

      val obs = new SESSLRunFinishedObserver
      obs.register(new FinalStateListener(job.getID))
      obs
    }
    observerGenerators += obs
    finalStateObservable
  }
}
