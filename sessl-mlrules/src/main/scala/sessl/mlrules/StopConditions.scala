/*
 * Copyright 2017 University of Rostock
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package sessl.mlrules

import org.jamesii.mlrules.experiment.stop._
import org.jamesii.mlrules.observation.SpeciesCountFunction
import org.jamesii.mlrules.simulator.{Simulator => MLRSimulator}
import sessl._

import scala.collection.JavaConverters._

/**
  * @author Tom Warnke
  */
trait StopConditions {
  this: Experiment =>

  def simulatorStopConditionFactory: StopConditionFactory =
    (sim: MLRSimulator) => createNewSimulatorStopCondition(getStoppingCondition, sim)

  private def getStoppingCondition: StoppingCondition = {
    require(fixedStopTime.isDefined || stoppingCondition.isDefined,
      "No stopping condition is specified (use, e.g., stopTime= 1.0 or stopCondition=...).")
    require(!(fixedStopTime.isDefined && stoppingCondition.isDefined),
      "Both a fixed stop time (" + fixedStopTime.get + ") and a stopping condition (" +
              stoppingCondition.get + ") are set - only one is allowed. Use '" +
              AfterSimTime(fixedStopTime.get) +
              "' to add the fixed stop time condition to the conditions.")
    if (fixedStopTime.isDefined)
      AfterSimTime(fixedStopTime.get)
    else stoppingCondition.get
  }

  /**
    * @param stoppingCondition a sessl stopping condition
    * @return the corresponding simulator stop condition
    */
  private def createNewSimulatorStopCondition(stoppingCondition: StoppingCondition,
                                              sim: MLRSimulator):
  StopCondition = stoppingCondition match {
    case ssse: SchrubensSteadyStateEstimator => ssse.create(sim)
    case st: AfterSimTime => new SimTimeStopCondition(st.time, sim)
    case wc: AfterWallClockTime => new WallClockStopCondition(wc.asMilliSecondsOrUnitless)
    case and: ConjunctiveStoppingCondition => new AndStopCondition(Seq
    (createNewSimulatorStopCondition(and.left, sim),
      createNewSimulatorStopCondition(and.right, sim)).asJava)
    case or: DisjunctiveStoppingCondition => new OrStopCondition(Seq
    (createNewSimulatorStopCondition(or.left, sim),
      createNewSimulatorStopCondition(or.right, sim)).asJava)
    case _ => throw new IllegalArgumentException("Stopping criterion '" +
            stoppingCondition + "' not supported.")
  }

  case class SchrubensSteadyStateEstimator(observable: SpeciesCountFunction => java.lang.Double,
                                           interval: Double,
                                           batchCount: Int,
                                           groupBatchCount: Int,
                                           allowedSignificance: Double)
          extends StoppingCondition {

    val factory = new SchrubensSteadyStateConditionFactory(
      observable(_),
      interval,
      batchCount,
      groupBatchCount,
      allowedSignificance
    )

    def create(sim: MLRSimulator): StopCondition = factory.create(sim)
  }

  object SchrubensSteadyStateEstimator {
    // auxiliary constructor with just a species string
    def apply(observable: String,
              interval: Double,
              batchCount: Int = 8,
              groupBatchCount: Int = 4,
              allowedSignificance: Double = 0.05): SchrubensSteadyStateEstimator =
      apply(count => count(observable), interval, batchCount, groupBatchCount, allowedSignificance)
  }
}
