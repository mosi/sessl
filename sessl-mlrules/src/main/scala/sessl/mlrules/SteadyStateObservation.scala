/*
 * Copyright 2018 University of Rostock
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package sessl.mlrules

import org.jamesii.mlrules.experiment.stop.{AndStopCondition, OrStopCondition, SchrubensSteadyStateCondition, StopCondition}
import org.jamesii.mlrules.observation.{Listener, Observer}
import sessl.AbstractObservation.Observable
import scala.collection.JavaConverters._

/**
  * @author Tom Warnke
  */
trait SteadyStateObservation {
  this: Experiment with Observation =>

  private[this] val steadyStateObservable = new Observable[Double] {}

  lazy val steadyState: Observable[Double] = {
    val obs: ObserverGenerator = (job, _) => {

      class SteadyStateListener(runId: Int) extends Listener {
        override def isActive: Boolean = true

        override def notify(observer: Observer): Unit = {}

        override def finish(stopCondition: StopCondition): Unit = stopCondition match {
          case sssc: SchrubensSteadyStateCondition if sssc.stop() =>
            val mean = sssc.getSteadyStateMean
            val stopTime = sssc.getStopTime
            addValueFor(runId, steadyStateObservable, (stopTime, mean))
          case and: AndStopCondition => for (cond <- and.getConditions.asScala) finish(cond)
          case or: OrStopCondition => for (cond <- or.getConditions.asScala) finish(cond)
          case _ => // do nothing
        }
      }

      val obs = new SESSLRunFinishedObserver
      obs.register(new SteadyStateListener(job.getID))
      obs
    }
    observerGenerators += obs
    steadyStateObservable
  }

}
