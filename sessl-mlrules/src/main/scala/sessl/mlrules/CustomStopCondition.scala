/*
 * Copyright 2018 University of Rostock
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package sessl.mlrules

import org.jamesii.mlrules.experiment.stop.{StopCondition, StopConditionFactory}
import org.jamesii.mlrules.observation.SpeciesCountFunction
import org.jamesii.mlrules.simulator.Simulator

/**
  * A trait for user-defined stop conditions.
  * Other stop conditions defined with
  * `stopCondition = ...`
  * or
  * `stopTime = ...`.
  * are ignored.
  *
  * @example
  * {{{
  *   new Experiment with CustomStopCondition {
  *     /* ... */
  *
  *     // stop as soon as the number of A entities anywhere in the model is between 50 and 60
  *     customStopCondition = count => count("*&#47;A") > 50 && count("*&#47;A") < 60
  *   }
  * }}}
  *
  * @author Tom Warnke
  */
trait CustomStopCondition {
  this: Experiment =>

  private[this] var customStoppingCondition = Option.empty[SpeciesCountFunction => Boolean]

  override def simulatorStopConditionFactory: StopConditionFactory =
    (sim) => new CustomStopCondition(sim, customStoppingCondition.get)

  def customStopCondition_=(predicate: SpeciesCountFunction => Boolean): Unit = {
    customStoppingCondition = Some(predicate)
  }

  def customStopCondition: SpeciesCountFunction => Boolean = customStoppingCondition.get

  class CustomStopCondition(sim: Simulator,
                            predicate: SpeciesCountFunction => Boolean)
          extends StopCondition {

    val function = new SpeciesCountFunction(sim.getModel.getSpecies)

    override def update(): Unit = {}

    override def stop(): Boolean = predicate(function)
  }
}
