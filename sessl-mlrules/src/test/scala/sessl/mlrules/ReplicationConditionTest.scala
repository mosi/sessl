/*
 * Copyright 2018 University of Rostock
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package sessl.mlrules

import org.junit.Assert.assertTrue
import org.junit.Test
import sessl.{MeanConfidenceReached, execute}

/**
  * @author Tom Warnke
  */
@Test class ReplicationConditionTest {

  @Test def testReplicationCondition(): Unit = {

    var mean = 0.0

    val exp = new Experiment with Observation with ParallelExecution {

      model = this.getClass.getResource("/steadyState.mlrj").toURI

      val a = observe(count("A"))
      observeAt(100)

      stopTime = 100

      replicationCondition = MeanConfidenceReached(a, relativeHalfWidth = 0.05, confidence = .95)

      withReplicationsResult{ r =>
        mean = r.mean(a)
        println(mean)
      }
    }

    execute(exp)

    assertTrue(mean != 0.0)
    // mean should be 50
    assertTrue(mean > 40 && mean < 60)

  }

}
