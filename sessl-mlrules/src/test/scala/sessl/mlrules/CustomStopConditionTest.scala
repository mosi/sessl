/*
 * Copyright 2018 University of Rostock
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package sessl.mlrules

import org.junit.Test
import junit.framework.Assert._

/**
  * @author Tom Warnke
  */
@Test class CustomStopConditionTest {

  @Test def testCustomStopCondition(): Unit = {

    var lastA = Option.empty[Double]

    import sessl._

    val exp = new Experiment with Observation with CustomStopCondition {

      model = this.getClass.getResource("/steadyState.mlrj").toURI
      simulator = SimpleSimulator()

      customStopCondition = count => count("*/A") > 50 && count("*/A") < 60

      val a = observe(count("A"))
      observeAt(range(0.0, 0.01, 1000.0))

      withRunResult(result => lastA = Some(result(a)))
    }

    execute(exp)

    assertTrue(lastA.isDefined)
    assertTrue(lastA.get <= 51 && lastA.get >= 49)
  }

}
