package sessl.mlrules

import org.junit.Test
import sessl._
import junit.framework.Assert._

@Test class DeterminismTest {

  @Test def testSameResults(): Unit = {
    def runExp = {
      var expResults = List.empty[Trajectory[Double]]
      execute {
        new Experiment with Observation {
          model = this.getClass.getResource("/steadyState.mlrj").toURI
          simulator = SimpleSimulator()
          stopTime = 100
          replications = 3
          seed = 42
          val a = observe(count("A"))
          observeAt(range(0.0, 1, stopTime))
          withReplicationsResult{ r =>
            expResults = r.trajectories(a).toList
          }
        }
      }
      expResults
    }

    val results1 = runExp
    Thread.sleep(100)
    val results2 = runExp

    assertTrue(results1 == results2)
  }

  @Test def testDifferentResults(): Unit = {
    def runExp = {
      var expResults = List.empty[Trajectory[Double]]
      execute {
        new Experiment with Observation {
          model = this.getClass.getResource("/steadyState.mlrj").toURI
          simulator = SimpleSimulator()
          stopTime = 100
          replications = 3
          //seed = 42
          val a = observe(count("A"))
          observeAt(range(0.0, 1, stopTime))
          withReplicationsResult{ r =>
            expResults = r.trajectories(a).toList
          }
        }
      }
      expResults
    }

    val results1 = runExp
    Thread.sleep(100)
    val results2 = runExp

    assertFalse(results1 == results2)
  }

}
