/*
 * Copyright 2017 University of Rostock
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package sessl.mlrules

import java.net.URI

import junit.framework.Assert._
import org.junit.Test
import sessl._

/**
  * @author Tom Warnke
  */
@Test class ObservationsTest {

  val testModel: URI = this.getClass.getResource("/observationTestModel.mlrj").toURI

  @Test def testObservationAtTimeZero(): Unit = {
    var res = Option.empty[List[Double]]

    execute {
      new Experiment with Observation with ExpressionObservation {
        model = testModel
        simulator = SimpleSimulator()
        stopTime = 100
        observeAt(0.0)

        observe("b4" ~ count("B(4.0)"))
        val b5 = observe(count("B(5.0)"))
        val anyB = observe(count("*/B(_)"))
        val anyBInAnyA = observe(count("A(_)/B(_)"))
        val anyB2InAnyA = observe(count("A(_)/B(2.0)"))
        val anyB2InAnyAx = observe(count("A(x)/B(2.0)"))
        val fun = observe(Expr(V => V(anyB) - V("b4") - V(b5)))

        withRunResult(result => res = Some(List(
        result("b4"),
        result(b5),
        result(anyB),
        result(anyBInAnyA),
        result(anyB2InAnyA),
        result(anyB2InAnyAx)
        )))
      }
    }

    assert(res.nonEmpty)
    res.foreach { result =>
      assertEquals(2.0, result(0))
      assertEquals(0.0, result(1))
      assertEquals(74.0, result(2))
      assertEquals(72.0, result(3))
      assertEquals(48.0, result(4))
      assertEquals(24.0, result(5))
    }
  }

  @Test def testAttributeObservation(): Unit = {
    var aTrajectory: Trajectory[Map[String, String]] = List.empty
    var bTrajectory: Trajectory[Map[String, Double]] = List.empty

    execute {
      new Experiment with Observation {
        model = testModel
        simulator = SimpleSimulator()
        stopTime = 0
        observeAt(0)

        // define observables first, then call observe on them
        val aFirstAttribute = attribute[String]("A", 0)
        val bFirstAttribute = attribute[Double]("*/B", 0)

        observe(aFirstAttribute, bFirstAttribute)

        withRunResult(result => {
          aTrajectory = result.trajectory(aFirstAttribute)
          bTrajectory = result.trajectory(bFirstAttribute)
        })
      }
    }

    assertTrue(aTrajectory.nonEmpty && bTrajectory.nonEmpty)

    aTrajectory.foreach { case (time, map) =>
      assertEquals(6, map.size)
      assertEquals(3, map.values.count(_ == "x"))
      assertEquals(3, map.values.count(_ == "y"))
    }

    bTrajectory.foreach { case (time, map) =>
      assertEquals(74, map.size)
      assertEquals(2, map.values.count(_ == 4.0))
      assertEquals(24, map.values.count(_ == 1.0))
    }

  }
}
