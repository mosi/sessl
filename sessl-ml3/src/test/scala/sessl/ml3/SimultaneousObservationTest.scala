/*
 * Copyright 2017 University of Rostock
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package sessl.ml3

import java.io.StringWriter

import junit.framework.Assert._
import org.junit.Test
import sessl._

@Test class SimultaneousObservationTest {

  val output = new StringWriter()

  @Test def runExperiment(): Unit = {

    val exp = new Experiment with Observation with CSVOutput {

      model = this.getClass.getResource("/simultaneous.ml3").toURI

      stopTime = 20

      initializeWith(
        "new Person(status := \"off\", number := 1, number2 := \"one\");" +
          "new Person(status := \"off\", number := 2, number2 := \"two\");" +
          "new Person(status := \"off\", number := 3, number2 := \"three\")"
      )

      val obs1 = observe("number" ~ (expression[Int]("ego.number")
        at Change("Person", "status", "ego.status = 'on'")))
      val obs2 = observe("number2" ~ (expression[Int]("ego.number2")
        at Change("Person", "status", "ego.status = 'on'")))

      observeAt(10)
      val obs3 = observe("status" ~ (expressionDistribution("Person", "ego.number", "ego.number != 2")))

      withRunResult(result => {
        CSVTrajectoryWriter.write(result, output, Map("obs1" -> obs1, "obs2" -> obs2, "obs3" -> obs3))
      })
    }
    execute(exp)
    assertEquals(
      """time,obs1,obs2,obs3
        |10.0,1,one,1
        |10.0,2,two,
        |10.0,3,three,3
        |""".stripMargin, output.toString)
  }
}

