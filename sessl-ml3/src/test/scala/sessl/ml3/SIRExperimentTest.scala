/*
 * Copyright 2017 University of Rostock
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package sessl.ml3

import junit.framework.Assert._
import org.junit.Test
import sessl._


@Test class SIRExperimentTest {

  @Test def emptyInitialization(): Unit = {
    execute {
      new ML3SIRExperiment {
        initializeWith(Empty)
      }
    }
  }

  @Test def simpleInitialization(): Unit = {
    execute {
      new ML3SIRExperiment {
        initializeWith(Expressions(s + ";" + i))
      }
    }
    execute {
      new ML3SIRExperiment {
        initializeWith(Expressions(s, i))
      }
    }
    execute {
      new ML3SIRExperiment {
        initializeWith(s + ";" + i)
      }
    }
  }

  @Test def csvDegreeDistributionInitialization(): Unit = {
    execute {
      new ML3SIRExperiment {
        val csvFile = this.getClass().getResource("/degrees.csv").getPath
        initializeWith(s + ";" + i) += DegreeDistribution("Person", "network", csvFile)
      }
    }
  }

  @Test def BarabasiAlbertInitialization(): Unit = {
    execute {
      new ML3SIRExperiment {
        initializeWith(s + ";" + i) += BarabasiAlbert("Person", "network", 3)
      }
    }
  }

  @Test def constantErdosRenyiInitialization(): Unit = {
    execute {
      new ML3SIRExperiment {
        initializeWith(s + ";" + i) += ErdosRenyi("Person", "network", 0.001)
      }
    }
  }

  @Test def expressionErdosRenyiInitialization(): Unit = {
    execute {
      new ML3SIRExperiment {
        initializeWith(s + ";" + i) += ErdosRenyi("Person", "network", "?a1.age/(?a1.age+?a2.age+1)")
      }
    }
  }

  @Test def wattsStrogatzInitialization(): Unit = {
    execute {
      new ML3SIRExperiment {
        initializeWith(s + ";" + i) += WattsStrogatz("Person", "network", 3, 0.1)
      }
    }
  }

  @Test def simpleObservation(): Unit = {
    var numTimePoints = 0
    var numPersons = 0
    var numAttributedPersons = 0
    var infections = 0
    var lastDistribution = Map.empty[Long, String]

    val exp = new ML3SIRExperiment with Observation {

      initializeWith(s + ";" + i) += ErdosRenyi("Person", "network", 0.001)

      observeAt(range(1, 1, 100))
      val numPerson = observe(agentCount("Person"))
      val susceptible = observe(agentCount("Person", "ego.status = 'susceptible'"))
      val infected = observe(agentCount("Person", "ego.status = 'infected'"))
      val recovered = observe(agentCount("Person", "ego.status = 'recovered'"))

      val infection = observe(expression[Int]("1") at Change("Person", "status", "ego.status = 'infected'"))

      val statusDistribution = observe(expressionDistribution[String]("Person", "ego.status"))

      withRunResult(result => {
        numTimePoints = result.times(numPerson).size
        numPersons = result(numPerson)
        numAttributedPersons = result(susceptible) + result(infected) + result(recovered)
        infections = result.values(infection).flatMap(_.values).sum
        lastDistribution = result(statusDistribution)
      })
    }
    execute(exp)
    assertEquals(100, numTimePoints)
    assertEquals(1000, numPersons)
    assertEquals(1000, numAttributedPersons)
    assertEquals(1000, lastDistribution.size)
  }
}

