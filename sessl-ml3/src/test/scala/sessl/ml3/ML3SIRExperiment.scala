/*
 * Copyright 2017 University of Rostock
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package sessl.ml3

import sessl._

class ML3SIRExperiment extends Experiment{

  model = this.getClass.getResource("/sir.ml3").toURI
  simulator = NextReactionMethod()

  replications = 1

  set("a" <~ 0.03)
  set("b" <~ 0.05)

  stopCondition = Custom((state, time) => state.getAgentsAlive.isEmpty || time >= 100)

  val s = 950 * "new Person(status := \"susceptible\")"
  val i = 50 * "new Person(status := \"infected\")"
}

