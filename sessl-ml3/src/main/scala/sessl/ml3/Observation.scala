/*
 * Copyright 2017 University of Rostock
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package sessl.ml3

import java.{lang, util}

import org.apache.commons.lang3.tuple
import org.jamesii.core.util.misc.Pair
import org.jamesii.ml3.experiment.Job
import org.jamesii.ml3.model.values.IValue
import org.jamesii.ml3.observation._
import org.jamesii.ml3.simulator.simulators.ISimulator
import sessl.AbstractObservation.Observable
import sessl._
import sessl.util.SimpleObservation

import scala.collection.JavaConverters._

/**
  * This trait adds two ML3-specific observation methods.
  * First, observations triggered by events are possible.
  * As these events are triggered by a rule that fires for a specific agent, the value of
  * expressions that are evaluated on the agent can be observed.
  * Second, time-triggered observations of expressions that are evaluated for all agents of
  * a certain type are possible, which result in a distribution of observed values for each
  * observation time.
  *
  * @example
  * {{{
  * val exp = new Experiment with Observation {
  *
  *   // observe at certain time steps
  *   observeAt(range(0, 1, 500)
  *
  *   observe("numPerson" ~ agentCount("Person"))
  *   observe("numPersonWithFilter" ~ agentCount("Person", "ego.status = 'active'"))
  *   observe("statusDistribution" ~ expressionDistribution("Person", "ego.status"))
  *
  *   // observe the status each time a Person dies
  *   observe("statusAtDeath" ~ expression("ego.status") at Death("Person"))
  * }
  * }}}
  *
  * @author Tom Warnke
  */
trait Observation extends SimpleObservation {
  this: Experiment =>

  private case class AgentCountObservable(agentType: String, filter: String)
          extends Observable[Int]
  private case class ExpressionObservable[T](expression: String, event: EventType)
          extends Observable[Map[Long, T]]
  private case class ExpressionDistributionObservable[T](agentType: String, expression: String, filter:String)
          extends Observable[Map[Long, T]]

  final class ProtoExpressionObservable[T](expression: String) {
    def at(event: EventType): Observable[Map[Long, T]] = ExpressionObservable[T](expression, event)
  }

  def agentCount(agentType: String, filter: String = "true"): Observable[Int] =
    AgentCountObservable(agentType, filter)

  def expression[T](expression: String): ProtoExpressionObservable[T] =
    new ProtoExpressionObservable[T](expression)

  def expressionDistribution[T](agentType: String, expression: String, filter: String = "true"): Observable[Map[Long, T]] =
    ExpressionDistributionObservable[T](agentType, expression, filter)

  /** Setting the flags that control a proper call hierarchy, calling event handlers if installed.*/
  override def configure(): Unit = {
    super.configure()

    // timed observations
    val timedListenerGenerators = observables.collect {
      case obs: AgentCountObservable => createAgentCountListener(_: Int, obs, _: Job)
      case obs: ExpressionDistributionObservable[_] => createExpressionDistributionListener(_: Int, obs, _: Job)
    }
    if (timedListenerGenerators.nonEmpty) {
      val generateObserver = (runId: Int, job: Job, simulator: ISimulator) => {
        val observer = createTimePointListObserver
        for (listenerGenerator <- timedListenerGenerators)
          observer.registerListener(listenerGenerator(runId, job))
        simulator.addObserver(observer)
      }
      instrumentations += generateObserver
    }

    // event-based observations
    val eventListenerGenerators = observables.collect {
      case obs: ExpressionObservable[_] => (obs.event, obs)
    }.groupBy(_._1)

    for ((event, obs) <- eventListenerGenerators) yield {

      // a function to generate the observer for this kind of event
      val observerGenerator = event match {
        case Creation(agentType, filter) => (job: Job) =>
          new AgentCreationObserver(agentType, filter, job.getModel, job.getParameters)
        case Death(agentType, filter) => (job: Job) =>
          new AgentDeathObserver(agentType, filter, job.getModel, job.getParameters)
        case Change(agentType, field, filter) => (job: Job) =>
          new AgentChangeObserver(agentType, field, filter, job.getModel, job.getParameters)
      }

      // a set of functions that each generate a listener
      val listenerGenerators = obs.map(_._2).map {
        obs: ExpressionObservable[_] =>
          (id: Int, job: Job) => createExpressionListener(id, obs, job)
      }

      // a function that construct the observer, the associated listeners, and attaches the whole
      // bunch to the simulator
      val generateObserver = (runId: Int, job: Job, simulator: ISimulator) => {
        val observer = observerGenerator(job)
        for (listenerGenerator <- listenerGenerators)
          observer.registerListener(listenerGenerator(runId, job))
        simulator.addObserver(observer)
      }

      // add the defined function to the observer generators
      instrumentations += generateObserver
    }

  }

  // wrap observer creation
  private[this] def createTimePointListObserver : IObserver = {
    val jTimes = observationTimes.map(d => d.asInstanceOf[java.lang.Double]).asJava
    new TimePointListObserver(jTimes)
  }

  // wrap listener creation
  private[this] def createAgentCountListener(runId: Int, obs: AgentCountObservable, job: Job):
  AgentCountListener = new AgentCountListener(
      obs.agentType,
      job.getModel,
      job.getParameters,
      obs.filter,
      (t: Pair[java.lang.Double, Integer]) => {
        addValueFor(
          runId,
          obs,
          (t.getFirstValue, t.getSecondValue).asInstanceOf[TimeStampedData[Int]])
      })


  private[this] def createExpressionListener[T](runId: Int, obs: ExpressionObservable[T], job: Job):
  AgentExpressionListener = new AgentExpressionListener(
      job.getModel,
      job.getParameters,
      obs.expression,
      (t: tuple.Triple[lang.Double, lang.Long, IValue]) =>
        addValueFor(
          runId,
          obs,
          (t.getLeft, Map(t.getMiddle -> ML3ValueToSesslValue(t.getRight))))
      )

  private[this] def createExpressionDistributionListener[T](runId: Int, obs: ExpressionDistributionObservable[T], job: Job):
  ExpressionDistributionListener = new ExpressionDistributionListener(
      obs.agentType,
      job.getModel,
      job.getParameters,
      obs.expression,
      obs.filter,
      (t: Pair[lang.Double, util.Map[lang.Long, IValue]])=>
        addValueFor(
          runId,
          obs,
          (t.getFirstValue, t.getSecondValue.asScala.toMap.map {
            case (id, v) => (id, ML3ValueToSesslValue(v))
          }))
      )

  sealed trait EventType

  case class Creation(agentType: String, filter: String = "true") extends EventType
  case class Death(agentType: String, filter: String = "true") extends EventType
  case class Change(agentType: String, field: String, filter: String = "true") extends EventType
}
