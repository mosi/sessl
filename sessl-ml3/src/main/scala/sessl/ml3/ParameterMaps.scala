/*
 * Copyright 2017 University of Rostock
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package sessl.ml3

import org.jamesii.ml3.parser.maps.MapBuilder

import scala.io.Source

/**
  * @author Tom Warnke
  */
trait ParameterMaps {
  this: Experiment =>

  def fromFile(fileName: String)(params: String*): Unit = {
    val mapBuilder = new MapBuilder(fileName, true)
    val columns = if (params.isEmpty) {
      // if no params are given, get all paramNames from the header
      val source = Source.fromFile(fileName)
      val cs = source.getLines().next().split(",").toList.tail
      source.close()
      cs
    } else params

    columns.foreach {
      paramName => {
        val map = mapBuilder.buildMap(0, paramName)
        paramMaps += (paramName -> map)
      }
    }
  }
}
