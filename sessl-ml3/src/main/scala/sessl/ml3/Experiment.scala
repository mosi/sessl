/*
 * Copyright 2017 University of Rostock
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package sessl.ml3

import org.jamesii.ml3.experiment.{Job, Experiment => ML3Experiment}
import org.jamesii.ml3.model.maps.IValueMap
import org.jamesii.ml3.model.values._
import org.jamesii.ml3.simulator.simulators.ISimulator
import sessl.{AbstractExperiment, DynamicSimulationRuns}

import scala.collection.JavaConverters._
import scala.collection.mutable
import scala.concurrent.{Future, Promise}


/**
  * @author Tom Warnke
  */
class Experiment extends AbstractExperiment
        with SupportStopConditions
        with DynamicSimulationRuns
        with InitialStateCreation {

  override type RunOutput = Unit

  /** default: use one core if not set otherwise.
    * Can be overwritten by the ParallelExecution trait */
  private[ml3] var parallelThreadsToUse = 1

  private[this] var experiment: ML3Experiment = _

  private[this] var theStartTime: Option[Double] = None

  /** functions to be called when creating a job, mainly for observation */
  protected val instrumentations = scala.collection.mutable.Set.empty[(Int, Job, ISimulator) => Unit]

  /** map-type parameters */
  protected[this] var paramMaps: mutable.Map[String, IValueMap] =
    scala.collection.mutable.Map[String, IValueMap]()

  /**
    * Abstract method to create the basic setup (as configure is already used for checking whether the traits
    * properly called super.configure()).
    * In this function, the experiment should be initialized to conform to all elements provided in this class.
    */
  override protected def basicConfiguration(): Unit = {}


  override lazy val variableAssignments: Seq[Map[String, AnyRef]] =
    super.variableAssignments

  def sesslValueToML3Value(v: Any): IValue = v match {
    case i: Int => new IntValue(i)
    case r: Double => new RealValue(r)
    case s: String => new StringValue(s)
    case b: Boolean => new BoolValue(b)
  }

  def ML3ValueToSesslValue(v: IValue): Any = v match {
    case i: IntValue => i.getValue
    case r: RealValue => r.getValue
    case s: StringValue => s.getValue
    case b: BoolValue => b.getValue
  }

  /** Called to execute the experiment. */
  override protected[sessl] def executeExperiment(): Unit = {

    if(theStartTime.isEmpty)
      theStartTime = Some(0)

    /* use standard simulator as default */
    if (simulators.isEmpty)
      simulator = FirstReactionMethod()

    experiment = new ML3Experiment(
      model,
      parallelThreadsToUse,
      simulatorStopCondition,
      initialStateBuilder,
      simulator.asInstanceOf[ML3Simulator].get,
      startTime)

    startBlocking()

    logger.info("All jobs finished.")
  }

  override def startSimulationRun(runId: Int, seed: Long, assignment: Map[String, AnyRef]): Future[Unit] = {
    val ml3Assignment = assignment.mapValues(v => sesslValueToML3Value(v))
    val promise = Promise[Unit]()
    val job: Job = new Job(experiment, ml3Assignment.asJava, paramMaps.asJava) {

      override def onFailure(t: Throwable): Unit = fail(t)

      override def onSuccess(): Unit = promise.success(())

      override def instrument(simulator: ISimulator): Unit = {
        for (instrumentation <- instrumentations)
          instrumentation(runId, this, simulator)
      }
    }
    // send the job to the simulation package
    experiment.addJob(job)
    promise.future
  }

  afterExperiment { _ =>
    experiment.finish()
  }

  def startTime_=(startTime: Double): Unit = {
    theStartTime = Some(startTime)
  }

  def startTime: Double = theStartTime.get
}
