/*
 * Copyright 2018 University of Rostock
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package sessl.ml3

import org.apache.commons.math3.random.RandomGenerator
import org.jamesii.ml3.experiment.init._
import org.jamesii.ml3.experiment.init.links._
import org.jamesii.ml3.experiment.init.links.probability.{ConstantProbabilityProvider, ExpressionProbabilityProvider}
import org.jamesii.ml3.model.agents.IAgentFactory
import org.jamesii.ml3.model.state.{IState, IStateFactory}
import org.jamesii.ml3.model.{Model, Parameters}

import scala.collection.mutable.ListBuffer

/**
  * A trait that manages the configuration of initial state creation.
  * One `IInitialStateBuilder` and several `IStateTransformers` can be supplied, which will
  * create and transform a state in the order specified.
  *
  * @author Tom Warnke
  */
trait InitialStateCreation extends StateBuilderFactories with NetworkTransformers {
  this: Experiment =>

  /** Creates the underlying builder on demand and applies the state transformations to it */
  private class SESSLInitialStateBuilder(builder: () => IInitialStateBuilder)
          extends IInitialStateBuilder {

    val transformers: ListBuffer[NetworkTransformer] = ListBuffer.empty[NetworkTransformer]

    override def buildInitialState(model: Model,
                                   stateFactory: IStateFactory,
                                   agentFactory: IAgentFactory,
                                   rng: RandomGenerator,
                                   parameters: Parameters): IState = {
      val state = builder().buildInitialState(model, stateFactory, agentFactory, rng, parameters)
      for (t <- transformers)
        t.apply(state, model, parameters, rng)
      state
    }
  }

  private[this] var theInitialStateBuilder: Option[SESSLInitialStateBuilder] = None

  def initialStateBuilder: IInitialStateBuilder =
    theInitialStateBuilder.getOrElse(
      throw new IllegalStateException("No specification of the initial state given"))

  def initializeWith(builder: () => IInitialStateBuilder): ListBuffer[NetworkTransformer] = {
    val builderWithTransformers = new SESSLInitialStateBuilder(builder)
    theInitialStateBuilder = Some(builderWithTransformers)
    builderWithTransformers.transformers
  }

  implicit class PopulationFactor(i: Int) {
    def *(agent: String): String = List.fill(i)(agent).mkString(";")
  }

  trait StateBuilderFactory extends (() => IInitialStateBuilder)

  trait NetworkTransformer extends ((IState, Model, Parameters, RandomGenerator) => Unit)

}

trait StateBuilderFactories {
  this: Experiment =>

  def Expressions(expressions: String*): StateBuilderFactory =
    () => new ExpressionStateBuilder(expressions.mkString(";"))

  def Empty: StateBuilderFactory =
    () => new EmptyStateBuilder

  def JSON(file: String): StateBuilderFactory =
    () => new JsonStateBuilder(file)

  def AttributeDistribution(popSize: Int, agentType: String, file: String): StateBuilderFactory =
    () => new AttributeDistributionStateBuilder(popSize, agentType, file, startTime)

  def initializeWith(builder: StateBuilderFactory): ListBuffer[NetworkTransformer] =
    initializeWith(() => builder.apply())

  def initializeWith(initialState: String*): ListBuffer[NetworkTransformer] =
    initializeWith(Expressions(initialState: _*))

}

trait NetworkTransformers {
  this: Experiment =>

  def BarabasiAlbert(agentType: String,
                     linkType: String,
                     numLinks: Int): NetworkTransformer =
    (state: IState,
     model: Model,
     parameters: Parameters,
     rng: RandomGenerator) => {
      val transformer = new BarabasiAlbertNetworkTransformer(
        agentType,
        rng,
        linkType,
        numLinks,
        numLinks)
      transformer.transform(state)
    }

  def BarabasiAlbert(agentType: String,
                     linkType: String,
                     initialCoreSize: Int,
                     attachmentLinks: Int): NetworkTransformer =
    (state: IState,
     model: Model,
     parameters: Parameters,
     rng: RandomGenerator) => {
      val transformer = new BarabasiAlbertNetworkTransformer(
        agentType,
        rng,
        linkType,
        initialCoreSize,
        attachmentLinks)
      transformer.transform(state)
    }

  def ErdosRenyi(agentType: String, linkType: String, p: Double): NetworkTransformer =
    (state: IState,
     model: Model,
     parameters: Parameters,
     rng: RandomGenerator) => {
      val transformer = new ErdosRenyiNetworkTransformer(
        agentType,
        rng,
        linkType,
        new ConstantProbabilityProvider(p))
      transformer.transform(state)
    }

  def ErdosRenyi(agentType: String, linkType: String, expression: String): NetworkTransformer =
    (state: IState,
     model: Model,
     parameters: Parameters,
     rng: RandomGenerator) => {
      val prob = new ExpressionProbabilityProvider(model, parameters, expression, startTime)
      val transformer = new ErdosRenyiNetworkTransformer(agentType, rng, linkType, prob)
      transformer.transform(state)
    }


  def DegreeDistribution(agentType: String, linkType: String, file: String): NetworkTransformer =
    (state: IState,
     model: Model,
     parameters: Parameters,
     rng: RandomGenerator) => {
      val dist = new CSVDegreeDistribution(rng, file)
      val transformer = new DegreeDistributionNetworkTransformer(agentType, rng, linkType, dist)
      transformer.transform(state)
    }

  def WattsStrogatz(agentType: String, linkType: String, k: Int, beta: Double): NetworkTransformer =
    (state: IState,
     model: Model,
     parameters: Parameters,
     rng: RandomGenerator) => {
      val transformer = new WattsStrogatzNetworkTransformer(agentType, rng, linkType, k, beta)
      transformer.transform(state)
    }
}