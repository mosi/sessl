/*
 * Copyright 2017 University of Rostock
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package sessl.ml3

import org.jamesii.ml3.model.state.IState
import org.jamesii.ml3.simulator.stop._
import sessl.{AfterSimTime, ConjunctiveStoppingCondition, DisjunctiveStoppingCondition, Never, StoppingCondition}

/**
  * @author Tom Warnke
  */
trait SupportStopConditions {
  this: Experiment =>

  def simulatorStopCondition = sesslStopToML3Stop(getStoppingCondition)

  def sesslStopToML3Stop(s: StoppingCondition): IStopCondition =
    s match {
      case t: AfterSimTime => new StopAtTimeCondition(t.time)
      case Never => StopConditions.NEVER
      case and: ConjunctiveStoppingCondition =>
        val s = new StopConditionConjunction()
        s.addStopCondition(sesslStopToML3Stop(and.left))
        s.addStopCondition(sesslStopToML3Stop(and.right))
        s
      case or: DisjunctiveStoppingCondition =>
        val s = new StopConditionDisjunction()
        s.addStopCondition(sesslStopToML3Stop(or.left))
        s.addStopCondition(sesslStopToML3Stop(or.right))
        s
      case Custom(predicate) =>
        (state: IState, time: Double) => predicate(state, time)
    }

  def getStoppingCondition: StoppingCondition = {
    require(fixedStopTime.isDefined || stoppingCondition.isDefined,
      "No stopping condition is specified (use, e.g., stopTime= 1.0 or stopCondition=...).")
    require(!(fixedStopTime.isDefined && stoppingCondition.isDefined),
      "Both a fixed stop time (" + fixedStopTime.get + ") and a stopping condition (" +
              stoppingCondition.get + ") are set - only one is allowed. Use '" + AfterSimTime(fixedStopTime.get) +
              "' to add the fixed stop time condition to the conditions.")
    if (fixedStopTime.isDefined)
      AfterSimTime(fixedStopTime.get)
    else stoppingCondition.get
  }

  case class Custom(predicate: (IState, Double) => Boolean) extends StoppingCondition

}
