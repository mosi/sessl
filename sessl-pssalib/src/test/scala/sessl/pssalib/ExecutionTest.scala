/*
 * Copyright 2017 University of Rostock
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package sessl.pssalib

import org.junit.Assert._
import sessl._

/**
  * @author Tom Warnke
  */
class ExecutionTest {

  val testModel = "/home/tom/tmp/pssalib-2.0.0/examples/tutorial/Proctor2005_annotated_Fig2.xml"

  def testObservation(): Unit = {

    var rTrajectory = Option.empty[Trajectory[Double]]
    var aTrajectory = Option.empty[Trajectory[Double]]

    val exp = new Experiment with Observation {
      model = testModel
      simulator = DM(path = "/home/tom/tmp/pssalib-2.0.0/pssa_cli")
      stopTime = 100
      replications = 1

      set("k1" <~ 5)

      observeAt(range(0, 10, stopTime))
      val r = observe("ROS")
      val a = observe("ATP")

      withRunResult { result =>
        rTrajectory = Some(result.trajectory(r))
        aTrajectory = Some(result.trajectory(a))
      }
    }

    execute(exp)

    assertEquals(11, rTrajectory.get.size)
    assertEquals(11, aTrajectory.get.size)
  }

}
