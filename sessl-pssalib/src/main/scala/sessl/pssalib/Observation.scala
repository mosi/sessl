/*
 * Copyright 2017 University of Rostock
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package sessl.pssalib

import java.io.File
import java.nio.charset.Charset

import org.apache.commons.csv.{CSVFormat, CSVParser}
import sessl.AbstractObservation.Observable
import sessl._

import scala.collection.mutable
import scala.language.implicitConversions

/**
  * Observe results by working with files created in the temporary directory.
  * Invokes the analyzer binary to create CSV files from the results.
  *
  * @author Tom Warnke
  */
trait Observation extends Experiment with AbstractObservation {

  private case class PSSALibObservable(varName: String) extends Observable[Double]

  implicit def stringToObservable(varName: String): Observable[Double] =
    getOrElseUpdate(varName, PSSALibObservable(varName))

  override def collectResults(
      runID: Int,
      removeData: Boolean = false): ObservationRunResultsAspect = {

    val resultFile = analyzer(dirForRun(runID))
    val trajectories = readFile(resultFile)
    new ObservationRunResultsAspect(runID, trajectories)
  }

  private def analyzer(runDir: File): File = {
    import scala.sys.process._

    val varNames = observables.collect {
      case PSSALibObservable(varName) => varName
    }.mkString(",")

    val logger = ProcessLogger(new File(runDir, "run.log"))

    val process = Process(Seq(
      sim.analyzerBinary,
      "--results", "trajectories",
      "--input-path", runDir.getAbsolutePath,
      "--output-path", runDir.getAbsolutePath,
      "--species", varNames
    ), sim.pathFile)

    process.!<(logger)
    logger.flush()

    val resultDir = new File(runDir, sim.directoryName)
    val resultFile = new File(resultDir, "trajectory_0_0.csv")
    require(resultFile.isFile, "error while collecting results")
    resultFile
  }

  private def readFile(resultFile: File): Map[Observable[_], Trajectory[_]] = {
    import scala.collection.JavaConverters._

    val parser = CSVParser.parse(resultFile, Charset.defaultCharset(), CSVFormat.DEFAULT.withHeader())

    val headerMap = parser.getHeaderMap.asScala

    val trajectories = headerMap.keys.map {
      varName => varName -> mutable.ArrayBuffer.empty[(Double, Any)]
    }.toMap

    for (record <- parser.iterator().asScala) {
      val time = record.get(0).toDouble
      headerMap.map { case (varName, idx) =>
        trajectories(varName).+=((time, record.get(idx)))
      }
    }

    val observableTrajectories = for (obs@PSSALibObservable(varName) <- observables) yield
      (obs, trajectories(varName).toList)
    observableTrajectories.toMap
  }

  override protected def additionalOptions: Seq[String] = {
    require(observationTimes.contains(0) && observationTimes.lengthCompare(1) > 0,
    "Use observeAt(range(0, dt, stopTime), where dt is the observation interval")
    super.additionalOptions ++ Seq("--dt", s"${observationTimes(1) - observationTimes.head}")
  }
}
