/*
 * Copyright 2017 University of Rostock
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package sessl.pssalib

import java.io.File

import sessl.Simulator

/**
  * The PSSALibSimulator instances contain information about the location of the simulator and analyzer binaries.
  * Default values (for Linux) are specified where possible.
  */
trait PSSALibSimulator extends Simulator {
  def path: String
  def argument: String
  def simulatorBinary: String
  def analyzerBinary: String
  val pathFile: File = new java.io.File(path)
  require(pathFile.isDirectory, "invalid path")
  def directoryName: String = argument.toUpperCase
}

case class DM(path: String,
              simulatorBinary: String = "./simulator",
              analyzerBinary: String = "./analyzer") extends PSSALibSimulator {
  override val argument = "dm"
}
case class PDM(path: String,
               simulatorBinary: String = "./simulator",
               analyzerBinary: String = "./analyzer") extends PSSALibSimulator {
  override val argument = "pdm"
}
case class SPDM(path: String,
                simulatorBinary: String = "./simulator",
                analyzerBinary: String = "./analyzer") extends PSSALibSimulator {
  override val argument = "spdm"
}
case class PSSA_CR(path: String,
                   simulatorBinary: String = "./simulator",
                   analyzerBinary: String = "./analyzer") extends PSSALibSimulator {
  override val argument = "pssacr"
}

