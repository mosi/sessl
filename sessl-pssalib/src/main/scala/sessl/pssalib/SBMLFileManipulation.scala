/*
 * Copyright 2018 University of Rostock
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package sessl.pssalib

import java.io.File

import org.sbml.jsbml.{SBMLDocument, SBMLReader, SBMLWriter}

/**
  * A small helper trait for copying and modifying SBML files.
  *
  * @author Tom Warnke
  */
trait SBMLFileManipulation {

  def readFile(path: String): SBMLDocument =
    SBMLReader.read(new File(path))

  def writeFile(doc: SBMLDocument, directory: File): File = {
    val file = new File(directory, generatedModelFileName)
    SBMLWriter.write(doc, file, "SESSL", "0.15")
    file
  }

  def overrideParamValues(doc: SBMLDocument, values: Map[String, Double]): Unit =
    for ((id, newValue) <- values) {
      val param = doc.getModel.getParameter(id)
      param.setValue(newValue)
    }

  private val generatedModelFileName: String = "model.xml"
}


