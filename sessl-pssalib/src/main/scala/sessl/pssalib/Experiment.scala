/*
 * Copyright 2017 University of Rostock
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package sessl.pssalib

import java.io.File
import java.nio.file._
import java.util.concurrent.{ExecutorService, Executors}

import sessl._

import scala.concurrent.{ExecutionContext, Future}

/**
  * A PSSALib experiment gets executed by invoking the native binary (which must be installed separately).
  * To configure the model with parameter values, the SBML model file is copied and modified before it becomes the input for the simulator.
  * A temporary directory is created for these model file copies.
  * The individual simulation runs are started in Futures to enable concurrency.
  *
  * @author Tom Warnke
  */
class Experiment extends AbstractExperiment with DynamicSimulationRuns with SBMLFileManipulation {

  override type RunOutput = Unit

  private val tmpDir = new File(Files.createTempDirectory("tmp-pssalib-exp").toUri)
  sys.addShutdownHook(util.Files.deleteFileOrDirectory(tmpDir))

  protected def dirForRun(runId: Int) = new File(tmpDir, s"run-$runId")

  protected lazy val sim: PSSALibSimulator = simulator.asInstanceOf[PSSALibSimulator]

  override protected def basicConfiguration(): Unit = {}

  override protected[sessl] def executeExperiment(): Unit =
    startBlocking()

  afterExperiment { _ =>
    logger.info("All jobs finished.")
    executor.shutdownNow()
  }

  override protected def startSimulationRun(runId: Int, seed: Long,
                                            assignment: Map[String, AnyRef]): Future[Unit] =

    Future {
      val runDir = dirForRun(runId)
      runDir.mkdir()

      val doubleAssignment: Map[String, Double] = assignment.collect {
        case (name, value: java.lang.Double) => (name, value.toDouble)
      }

      val originalModel = readFile(model)
      overrideParamValues(originalModel, doubleAssignment)
      val modelFile = writeFile(originalModel, runDir)

      import scala.sys.process._

      val logger = ProcessLogger(new File(runDir, "run.log"))

      val process = Process(
        Seq[String](
          sim.simulatorBinary,
          "--sbml-file", modelFile.getAbsolutePath,
          "--methods",  sim.argument,
          "--num-samples", "1",
          "--tend", stopTime.toLong.toString,
          "--output-path", runDir.getAbsolutePath) ++
        additionalOptions,
        sim.pathFile)

      process.!<(logger) // blocking process execution
      logger.flush()
    }

  protected val executor: ExecutorService = Executors.newSingleThreadExecutor()

  implicit private val execContext: ExecutionContext =
    ExecutionContext.fromExecutor(executor)

  protected def additionalOptions: Seq[String] = Seq.empty

}

