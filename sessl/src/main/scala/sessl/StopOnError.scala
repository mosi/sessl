/*
 * Copyright 2018 University of Rostock
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package sessl

/**
  * A trait to configure how an experiment reacts to exceptions in the event handlers.
  * By default, experiments do not stop when errors occur.
  * This behavior can be changed by setting stopOnError to true or by setting the environment
  * variable SESSL_STOP_ON_ERROR to "true".
  * Setting stopOnError takes precedence over the environment variable.
  *
  * @example {{{
  * val exp = new Experiment {
  *   // ...
  *   stopOnError = true
  * }
  * }}}
  *
  * @author Tom Warnke
  */
trait StopOnError extends ExperimentConfiguration {

  private var stopsOnError: Option[Boolean] = readEnvVar

  def stopOnError_=(stop: Boolean): Unit = stopsOnError = Some(stop)

  def stopOnError: Boolean = stopsOnError.getOrElse(false)

  /**
    * @return
    *         None, if StopOnError.envVar is not defined,
    *         Some(true) if it is set to StopOnError.enabled, and
    *         Some(false) if it is set to StopOnError.disabled
    */
  private def readEnvVar: Option[Boolean] = {
    sys.env.get(StopOnError.envVar).map {
      case StopOnError.enabled => true
      case StopOnError.disabled => false
    }
  }
}

object StopOnError {
  val envVar: String = "SESSL_STOP_ON_ERROR"
  val enabled: String = "true"
  val disabled: String = "false"
}
