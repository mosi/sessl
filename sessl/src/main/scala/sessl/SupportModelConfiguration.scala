/**
 * *****************************************************************************
 * Copyright 2012 Roland Ewald
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ****************************************************************************
 */
package sessl
import java.net.URI

import sessl.util.Logging

import scala.collection.mutable.ListBuffer

/**
 * Support for configuring the model. Supports to set fixed model parameters (via `set(...)`) as well as parameter scans (via `scan(...)`).
 *  @author Roland Ewald
 */
trait SupportModelConfiguration extends Logging {

  /** The model to be used. */
  protected[this] var modelLocation: Option[String] = None

  /** List of experiment variables to be scanned. */
  private[this] val varsToScan = ListBuffer[Variable[_]]()

  /** List of experiment variables to be set (for each run). */
  private[this] val varsToSet = ListBuffer[VarSingleVal[_]]()

  /**
   * Define the experiment variables to be scanned.
   *  @param variablesToScan a list of variables to scan, concatenate with 'and' to make them change values in unison (but only if the same number of values shall be scanned for them)
   *
   *  @example {{{
   *    scan ("x" <~ range(0,2,8)) // model runs with x = 0, 2, 4, 6, 8
   *  }}}
   *  @example {{{
   *    scan ("x" <~ range(1,1,10), "y" <~ range(2,2,20)) // model runs with (x,y) = (1,2), (1,4), ..., (1,20), (2,2), (2,4), ... (10,20)
   *  }}}
   *  @example {{{
   *    scan ("x" <~ range(1,1,10) and "y" <~ range(2,2,20)) // model runs with (x,y) = (1,2), (2,4), ..., (9,18), (10,20)
   *  }}}
   *  @example {{{
   *    scan ("x" <~ (1,2,3) and "y" <~ ("a", "b", "c"), "z" <~ range(1,1,100)) // model runs with (x,y,z) = (1,"a", 1), ..., (1,"a", 100), (2, "b", 1), ... (2,"b", 100), ... etc.
   *  }}}
   */
  def scan(variablesToScan: Variable[_]*): Unit = {
    varsToScan ++= variablesToScan
  }

  /**
    * Define a number of variable configurations to scan.
    * @param configs a list of parameter configurations to scan
    *
    * @example {{{
    *   configs(
    *     Config("x" <~ 1, "y" <~ "a"),
    *     Config("x" <~ 2, "y" <~ "b"),
    *     Config("x" <~ 3, "y" <~ "c")
    *   )
    * }}}
    */
  def configs(configs: Config*): Unit = scan(Config.combine(configs))

  /**
    * Sample a number of configurations in the given parameter space.
    * The configurations are sampled when the experiment is executed (only once).
    * @param num the number of random configurations to sample
    * @param vars the parameter intervals in which to sample
    *
    * @example {{{
    *   uniformRandomSampling(5,
    *     "a" <~ interval(1, 5),
    *     "b" <~ interval(11, 20)
    *   )
    * }}}
    */
  def uniformRandomSampling(num: Int, vars: Variable[_]*): Unit =
    scan(Config.random(num, vars))

  /**
   * Define the variables to be set for each run (fixed).
   *  @param variablesToSet a list of variables to set
   *  @example {{{
   *     set("x" <~ 1.0, "y" <~ 35, "z" <~ "something else")
   *  }}}
   */
  def set(variablesToSet: Variable[_]*): Unit = {
    for (variable <- variablesToSet) {
      val v = variable match {
          case v: VarSingleVal[_] => v
          case v: Variable[_] =>
            logger.debug(s"Note that variable '${v.name}' is set to a single value '${v.value}'")
            VarSingleVal[AnyVal](v.name, v.value.asInstanceOf[AnyVal])
        }
      varsToSet += v
    }
  }

  /** Creates variable setups (or list with single empty map, if none are defined). */
  private[sessl] def createVariableSetups(): Seq[Map[String, Any]] = {
    if (varsToScan.nonEmpty) {
      val fixedVarSetup = varsToSet.map(v => (v.name, v.value)).toMap
      Variable.createVariableSetups(varsToScan).map {
        scanSetup => scanSetup ++ fixedVarSetup
      }
    } else List(fixedVariables)
  }

  /** Get all defined variables that shall be fixed as a map. */
  protected lazy val fixedVariables = varsToSet.map(v => (v.name, v.value)).toMap

  /** Allow to specify a model URI. */
  def model_=(modelURI: URI) = { modelLocation = Some(modelURI.getPath) }

  /** Set string identifying the model to be simulated. */
  def model_=(modelString: String) = { modelLocation = Some(modelString) }

  /** Get string identifying the model to be simulated. */
  def model: String = modelLocation.get
}
