/*
 * Copyright 2017 University of Rostock
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package sessl

/**
  * Enables central composite experiment designs, fractional three level designs that consist of
  * a two level full factorial on the lower and upper level,
  * a center point, where all factors are on the middle level,
  * and a set of axial points, where all but one factor are on the middle level
  * while the remaining factor is on the upper or lower level.
  *
  * @author Oliver Reinhardt
  */
trait CentralCompositeDesign extends AbstractExperiment {

  /**
    * Creates a central composite design on the given parameters.
    *
    * @example {{{
    *  	centralComposite("x" <~ interval(0, 5), "y" <~ interval(0.0, 10.0), "z" <~ interval(5.0, 15.0))
    *  }}}
    *
    *
    */
  def centralComposite[T](vars: Variable[T]*): Unit = {
    // check and convert input
    require(
      vars.forall(_.isInstanceOf[VarInterval[_]])
    )
    val variables = vars map (_.asInstanceOf[VarInterval[_]])

    // coordinates of corner points (full factorial on upper on lower level)
    val cornerCoordinates = variables.map(v => Array(v.from, v.to)).foldLeft(Array(Array.empty[Double]))(
      (acc, next) =>
        for (a <- acc; n <- next) yield {
          a :+ n
        })

    // center point (all factors on middle level)
    val centerCoordinates = variables.map(v => (v.from + v.to) / 2).toArray

    // axial points (all but one factors on middle level, one factor on upper or lower level)
    val axialCoordinates = new Array[Array[Double]](variables.size * 2)
    for (i <- variables.indices) {
      axialCoordinates(2 * i) = centerCoordinates.updated(i, variables(i).from)
      axialCoordinates(2 * i + 1) = centerCoordinates.updated(i, variables(i).to)
    }

    // convert to variable
    val allCoordinates = cornerCoordinates ++ Array(centerCoordinates) ++ axialCoordinates
    val allVariables = MultipleVars((allCoordinates.transpose zip variables).map { case (s, v) => VarSeq(v.name, s) }.toList)

    scan(allVariables)
  }

}
