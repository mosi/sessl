/*
 * Copyright 2017 University of Rostock
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package sessl

import java.time.ZoneId

import sessl.util.math.ConfidenceIntervalWidth

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.{Await, Awaitable, Future, Promise}
import scala.util.{Failure, Success}


/**
  * A trait for bindings that connect SESSL to simulation packages that can dynamically issue new runs.
  * In particular, this enables the dynamic evaluation of replication conditions and subsequent execution of additional
  * simulation runs if necessary.
  * <br>
  * To use this trait, the method startSimulationRun must be overridden.
  * Additionally, the method processOutput can be overridden to do something with the output of
  * a simulation run.
  * This is probably most useful in Observation or PerformanceObservation traits to collect the results of a run.
  * <br>
  * Furthermore, this trait allows setting a root seed for generating the random number generator seeds for the
  * individual simulation runs. With the same root seed, the same results should be produced unless dynamic replication
  * conditions and simulation runs are executed in  parallel. In that case, race conditions between different
  * assignments may lead to different usage of the random number sequence.
  *
  * @author Tom Warnke
  */
trait DynamicSimulationRuns {
  this: AbstractExperiment =>

  type RunOutput

  /** this promise is useful to await the end of the experiment without polling */
  private[this] val finished: Promise[Unit] = Promise[Unit]

  /** call this to signal that the experiment has failed and should be aborted as soon as possible */
  protected def fail(t: Throwable) = {
    finished.failure(t)
    throw t
  }

  protected var theBatchSize: Option[Int] = None

  var seed: Long = expStartTime.atZone(ZoneId.systemDefault()).toInstant.toEpochMilli

  /** The number of simulation runs to execute in one batch */
  def batchSize = theBatchSize.getOrElse(1)

  /** Set the number of simulation runs to execute in one batch */
  def batchSize_=(b: Int) = theBatchSize = Some(b)

  /** All variables assignments specified */
  protected def variableAssignments: Seq[Map[String, AnyRef]] =
    createVariableSetups().map(_.mapValues(_.asInstanceOf[AnyRef]))

  /** Create and start a new simulation run with the given run id and assignment.
    * The result must be Future the finishes with the simulation run and contains its results. */
  protected def startSimulationRun(runId: Int, seed: Long, assignment: Map[String, AnyRef]): Future[RunOutput]

  /** Processes the output returned by a finished simulation run.
    * If you override this, start by calling super.processOutput(runOutput) */
  protected def processOutput(runId: Int, runOutput: RunOutput): Unit = {}

  private final def startAsync(): Awaitable[Unit] = {
    Future.sequence(assignmentFutures).onComplete {
      case Success(_) =>
        experimentDone()
        finished.success(())
      case Failure(exception) =>
        fail(exception)
    }
    finished.future
  }

  /** start the experiment and wait unitl it finishes all runs */
  protected final def startBlocking(): Unit =
    Await.result(startAsync(), scala.concurrent.duration.Duration.Inf)

  private def assignmentFutures: Seq[Future[Int]] =
    for (assignmentId <- variableAssignments.indices) yield
      startAssignment(assignmentId, List.empty)

  private def startAssignment(assignmentId: Int, finishedRuns: List[RunResults]): Future[Int] = {
    val runsToIssue = minReplicationNumber(finishedRuns)

    val assignment = variableAssignments(assignmentId)

    // first generate and register the ids of the runs that will be issued
    val runIds = for (_ <- Range(0, runsToIssue)) yield {
      // create unique run id and register it
      val (runId, seed) = RunIdGenerator()
      addAssignmentForRun(runId, assignmentId, assignment.toList)
      (runId, seed)
    }

    // then actually start the runs
    val runFutures = for ((runId, seed) <- runIds) yield {
      // create new job with given assignment and send it off
      val runFuture = startSimulationRun(runId, seed, assignment)
      logger.info("Submitted job nr " + runId + " with configuration " + assignment)
      runFuture.transformWith {
        case Success(output) =>
          logger.info(s"Run $runId is finished.")
          processOutput(runId, output)
          runDone(runId)
          Future.successful(Right(runId))
        case Failure(exception) =>
          logger.error(s"Run $runId has thrown an exception", exception)
          fail(exception)
          Future.successful(Left(exception))
      }
    }

    Future.sequence(runFutures).flatMap { results =>
      if (results.forall(_.isRight)) {
        // all runs finished correctly
        val newRuns =
          results.collect { case Right(id) => this.results.forRun(id) }
        if (!enoughReplications(finishedRuns ++ newRuns)) {
          // more runs needed
          startAssignment(assignmentId, finishedRuns ++ newRuns)
        } else {
          // done all needed runs
          replicationsDone(assignmentId)
          Future.successful(assignmentId)
        }
      } else {
        // an error occurred, but has already been logged
        // nothing more we can do
        Future.failed(new IllegalStateException())
      }
    }
  }

  /** creates unique run ids. Thread-safe */
  private[this] object RunIdGenerator {
    private[this] val numberStream = Stream.from(0).iterator
    private[this] val rng = new scala.util.Random(seed)

    def apply(): (Int, Long) = this.synchronized {
      (numberStream.next(), rng.nextLong())
    }
  }

  private def minReplicationNumber(finishedRuns: List[RunResults]): Int = minReplicationNumber(
    checkAndGetReplicationCondition(),
    finishedRuns)

  private def enoughReplications(finishedRuns: List[RunResults]): Boolean = enoughReplications(
    checkAndGetReplicationCondition(),
    finishedRuns)

  /** Calculates the minimal number of replications to execute.
    * Can be overridden for custom replication handling.
    * The already finished replications are given.
    * It is probably a good idea to combine this result with super.minReplicationNumber via math.max.
    * */
  protected def minReplicationNumber(replicationCondition: ReplicationCondition,
                                     finishedRuns: List[RunResults]
                                    ): Int = {
    val numFinishedRuns = finishedRuns.size

    replicationCondition match {
      case FixedNumber(r) => r - numFinishedRuns
      case m@MeanConfidenceReached(observable, _, _) =>
        require(this.isInstanceOf[AbstractObservation], "The replication criterion '" + m +
                "' is specified, which works on observed data, but no observation is defined. Use '... with " +
                "Observation'.")
        val self = this.asInstanceOf[AbstractObservation]
        require(self.observables.contains(observable), "The observable '" + observable +
                "' has not been bound to a model variable yet.")
        batchSize
      case ConjunctiveReplicationCondition(left, right) =>
        math.max(minReplicationNumber(left, finishedRuns),
          minReplicationNumber(right, finishedRuns))
      case DisjunctiveReplicationCondition(left, right) =>
        math.min(minReplicationNumber(left, finishedRuns),
          minReplicationNumber(right, finishedRuns))
      case _ => batchSize
    }
  }

  /** Determine if enough replications have been executed already.
    * Can be overridden for custom replication handling.
    * The already finished replications are given.
    * It is probably a good idea to combine this result with super.enoughReplications via &&.
    * */
  protected def enoughReplications(replicationCondition: ReplicationCondition,
                                   finishedRuns: List[RunResults]): Boolean = {

    replicationCondition match {
      case FixedNumber(r) => finishedRuns.size >= r
      case MeanConfidenceReached(observable, relativeHalfWidth, confidence) =>
        val widths = ConfidenceIntervalWidth.computeRelativeHalfWidth(observable, confidence,
          DynamicSimulationRuns.runResultsToObservationRunResultsAspects(finishedRuns))
        widths.forall(_ < relativeHalfWidth)
      case ConjunctiveReplicationCondition(left, right) =>
        enoughReplications(left, finishedRuns) && enoughReplications(right, finishedRuns)
      case DisjunctiveReplicationCondition(left, right) =>
        enoughReplications(left, finishedRuns) || enoughReplications(right, finishedRuns)
      case _ => true
    }
  }
}

object DynamicSimulationRuns {
  def runResultsToObservationRunResultsAspects(results: List[RunResults]):
  List[ObservationRunResultsAspect] = {
    for {
      r <- results
      a <- r.aspectFor(classOf[AbstractObservation])
      if a.isInstanceOf[ObservationRunResultsAspect]
    } yield a.asInstanceOf[ObservationRunResultsAspect]
  }
}