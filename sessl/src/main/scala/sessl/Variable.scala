/*
 * Copyright 2017 University of Rostock
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Basic support for variables.
 *  @author Roland Ewald
 */
package sessl

import scala.collection.mutable.ListBuffer

/** Super class for all experiment variables. */
sealed trait Variable[T] {

  /** The name of the variable. */
  def name: String

  /** The value of the variable. */
  def value: Any

}

/**
  * One model configuration, i.e., a list of parameters with a value.
  */
case class Config(vs: Variable[_]*) {
  require(
    vs.forall(_.isInstanceOf[VarSingleVal[_]])
  )
  def asSeq: Seq[(String, Any)] = vs.collect {
    case VarSingleVal(name, value) => (name, value)
  }
}

object Config {

  /**
    * Combines a number of configs to a SESSL-compatible format
    * @return a MultipleVars instance that represents the given configurations
    */
  def combine(configs: Seq[Config]): MultipleVars = {
    val cs = configs.map(c => c.asSeq)
    val t = cs.transpose
    val vs = for (v <- t) yield VarSeq(v.head._1, v.map(_._2))
    val mvs = vs.map(v => MultipleVars(List(v)))
    mvs.fold(MultipleVars(List()))(_ and _)
  }

  def random(num: Int, vars: Seq[Variable[_]]): MultipleVars = {
    require(vars.forall(_.isInstanceOf[VarInterval[_]]))
    def sample(v: VarInterval[_]): Double =
      (v.to - v.from) * math.random() + v.from
    val vs = for (_ <- 1 to num) yield
      vars.map(v => v.name <~ sample(v.asInstanceOf[VarInterval[_]]))
    val cs = vs.map(v  => Config(v: _*))
    combine(cs)
  }
}

/** Methods applicable to variables. */
object Variable {

  /**
   * Creates the setups for multiple variables.
   *
   *  @param variables the list of variables
   *  @param givenSetups the given setups
   *  @return the created setups
   */
  def createMultipleVarsSetups(variables: List[Variable[_]], givenSetups: Seq[Map[String, Any]] = Seq(Map())) = {
    val allNewSetups = variables.map(v => createVariableSetups(Seq(v), Seq(Map[String, AnyRef]())))
    require(allNewSetups.nonEmpty && allNewSetups.forall(_.length == allNewSetups.head.length), "The number of generated values needs to be the same for variables: " + variables.map(_.name).mkString(","))

    val newSetups = allNewSetups.head.toArray
    for (newSetup <- allNewSetups.tail; i <- newSetup.indices)
      newSetups(i) = newSetups(i) ++ newSetup(i)

    createCombinations(givenSetups, newSetups)
  }

  /**
   * Creates all defined variable setups.
   *
   *  @param variablesToScan the variables to scan
   *  @return the sequence of defined setups
   */
  protected[sessl] def createVariableSetups(variablesToScan: Seq[Variable[_]]): Seq[Map[String, Any]] = {
    createVariableSetups(variablesToScan, Seq(Map()))
  }

  /**
   * Creates the variable setups.
   *
   *  @param variables the list of all variables
   *  @param givenSetups the setups already given
   *  @return the created variable setups
   */
  private[this] def createVariableSetups(variables: Seq[Variable[_]], givenSetups: Seq[Map[String, Any]]): Seq[Map[String, Any]] = {

    if (variables.isEmpty)
      return givenSetups

    val rv = variables.head match {
      case mv: MultipleVars => createMultipleVarsSetups(mv.variables, givenSetups)
      case vr: VarRange[_] => createCombinations(givenSetups, vr.toList.map(value => Map(vr.name -> value)))
      case vs: VarSeq[_] => createCombinations(givenSetups, vs.values.map(value => Map(vs.name -> value)))
      case v: VarSingleVal[_] => createCombinations(givenSetups, Seq(Map(v.name -> v.value)))
      case x => throw new IllegalArgumentException("Variable " + x + " is not supported.")
    }

    createVariableSetups(variables.tail, rv)
  }

  /**
   * Yields all combinations of two sequences of maps.
   *  @param seq1 the first sequence of maps
   *  @param seq2 the second sequence of maps
   *  @return a sequence with all combinations of maps
   */
  private[this] def createCombinations[X, Y](seq1: Seq[Map[X, Y]], seq2: Seq[Map[X, Y]]): Seq[Map[X, Y]] =
    for (map1 <- seq1; map2 <- seq2) yield (map1 ++ map2)

}

/** Class that represents a semi-defined variable (only variable name is given so far). */
case class VarName(name: String) {

  def <~[T : Numeric](r: ValueRange[T]): Variable[T] = VarRange[T](name, r.from, r.step, r.to)
  def <~[T : Numeric](v: ValueInterval[T]): Variable[T] = VarInterval[T](name, v.from, v.to)
  def <~[T](ts: T*): Variable[T] =
    if(ts.length == 1) VarSingleVal[T](name, ts.head)
    else VarSeq[T](name, ts)

}

/**
 * This represents a range of possible values (for a variable).
 *
 *  @tparam T
 *            the generic type
 *  @param from
 *            the lower boundary
 *  @param step
 *            the step size
 *  @param to
 *            the upper boundary (inclusive)
 */
case class ValueRange[T](from: T, step: T, to: T)(implicit n: Numeric[T]) {

  /**
   * Converts a range to a list of values.
   *
   *  @return the list of concrete values
   */
  def toList: List[T] = {

    require(!n.eq(step, n.zero), "Step size must be != zero.")
    val posStepSize = n.gt(step, n.zero)

    require(n.lteq(from, to) || !posStepSize, "Lower bound (" + from + ") > upper bound (" + to + "), while having a positive step size (" + step + ")!")
    require(n.gteq(from, to) || posStepSize, "Lower bound (" + from + ") < upper bound (" + to + "), while having a negative step size (" + step + ")!")

    from match {
      case _: Double =>
        // when computing a range of Doubles, use BigDecimals when adding to avoid imprecision errors
        val decFrom = BigDecimal(from.asInstanceOf[Double])
        val decStep = BigDecimal(step.asInstanceOf[Double])
        val decTo = BigDecimal(to.asInstanceOf[Double])

        val rangeValues = ListBuffer[T]()
        rangeValues += from.asInstanceOf[T] // cast Double to Double
        var nextValue: BigDecimal = decFrom + decStep
        while ((nextValue <= decTo && posStepSize) || (nextValue >= decTo && !posStepSize)) {
          rangeValues += nextValue.toDouble.asInstanceOf[T] // cast Double to Double
          nextValue = nextValue + decStep
        }
        rangeValues.toList
      case _ =>
        val rangeValues = ListBuffer[T]()
        rangeValues += from
        var nextValue = n.plus(from, step)
        while ((n.lteq(nextValue, to) && posStepSize) || (n.gteq(nextValue, to) && !posStepSize)) {
          rangeValues += nextValue
          nextValue = n.plus(nextValue, step)
        }
        rangeValues.toList
    }
  }
}

/**
 * Provides syntax for creation of [[ValueRange]] instances. It is an alternative to the built-in type [[Range]],
 * which orders its parameters differently (from, to, stepSize).
 */
object range {

  /**
   * Specify a range of values.
   *  @param from the lower boundary
   *  @param step the step size
   *  @param to the upper boundary (inclusive)
   *
   *  @example{{{
   *  range(10,2,100) //ValueRange containing 10, 12, ..., 100
   *  }}}
   */
  def apply[T : Numeric](from: T, step: T, to: T) = ValueRange[T](from, step, to)

  /**
   * Simplifies the construction of ranges with step size one.
   *  @param from the lower boundary
   *  @param to the upper boundary (inclusive)
   *
   *  @example{{{
   *  range(10,100) //ValueRange containing 10, 11, ..., 100
   *  }}}
   */
  def apply[T](from: T, to: T)(implicit n: Numeric[T]) = ValueRange[T](from, n.one, to)

  /**
   * Special factory method to create string sequences for a range of values. For the string argument, the `printf` syntax applies.
   *  @param printString the string to be printed
   *  @param from the lower boundary
   *  @param step the step size
   *  @param to the upper boundary (inclusive)
   *
   *  @example{{{
   *  range("%ds", 2, 2, 8) //List of strings containing 2s, 4s, 6s, 8s
   *  }}}
   */
  def apply[T : Numeric](printString: String, from: T, step: T, to: T): Seq[String] = {
    VarRange("", from, step, to).toList.map(v => printString.format(v))
  }

  /**
   * Simplifies the construction of string sequences with step size one.
   *  @param printString the string with the `printf` syntax
   *  @param from the lower boundary
   *  @param to the upper boundary (inclusive)
   *
   *  @example{{{
   *  range("%ds", 2, 4) //List of strings containing 2s, 3s, 4s
   *  }}}
   */
  def apply[T](printString: String, from: T, to: T)(implicit n: Numeric[T]): Seq[String] = range(printString, from, n.one, to)
}

case class ValueInterval[T : Numeric](from: T, to: T)

object interval {

  def apply[T : Numeric](from: T, to: T) = ValueInterval[T](from, to)

}

/**
 * A variable associated with a range of values.
 *
 *  @tparam T the generic type
 *  @param name the name of the variable
 *  @param from the lower bound of the variable
 *  @param step the step size
 *  @param to the upper bound of the variable (inclusive)
 */
case class VarRange[T : Numeric](name: String, from: T, step: T, to: T) extends Variable[T] {

  override def value = range(from, step, to)

  /** Generate list of values specified by range. */
  def toList = value.toList

}

/** Sequence of elements. */
case class VarSeq[T](name: String, value: Seq[T]) extends Variable[T] {
  def values: Seq[T] = value
}

/** Single element. */
case class VarSingleVal[T](name: String, value: T) extends Variable[T]

/** Multiple variables on the same 'layer' of the experiment variables.*/
case class MultipleVars(value: List[Variable[_]]) extends Variable[AnyVal] {

  override def name = value.map(_.name).mkString(", ")

  /**
   * Adding two (lists of) variables yields a new list of variables (a concatenation).
   *  @param that the other variable
   */
  def and(that: MultipleVars) = MultipleVars(this.value ::: that.value)

  def variables = value
}

/** An interval of values, which can be converted to Doubles*/
case class VarInterval[T](name: String, lower: T, upper: T)(implicit n: Numeric[T]) extends Variable[T] {

  def from: Double = n.toDouble(lower)
  def to: Double = n.toDouble(upper)

  override def value = ValueInterval[T](lower, upper)
}
