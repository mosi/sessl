/*
 * Copyright 2017 University of Rostock
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package sessl

import sessl.AbstractObservation.Observable

/**
  * A trait to evaluate expressions on observed data after each run.
  * The expressions to evaluate can be defined using mathematical operations.
  * The resulting data can be used just like normal simulation observations.
  *
  * @example
  * {{{
  * val exp = Experiment with ExpressionObservation {
  *   // ...
  *   val x = observe(...)
  *   observe("y" ~ ...)
  *
  *   // observe the sum of x and y
  *   val z = observe(Expr(V => V(x) + V("y")))
  *
  *   withRunResult { result =>
  *     println(result.trajectory(z))
  *   }
  * }
  * }}}
  *
  * @author Tom Warnke
  */
trait ExpressionObservation extends AbstractObservation {
  this: AbstractExperiment =>

  override def collectRunResultsAspects(runId: Int): Unit = {
    super.collectRunResultsAspects(runId)

    val observations =
      results.forRun(runId).aspects(classOf[AbstractObservation]).asInstanceOf[ObservationRunResultsAspect]
    val exprObservations = evaluateAll(runId, observations)
    addRunResultsAspect(runId, new ObservationRunResultsAspect(runId, (observations, exprObservations)))
  }

  private def evaluateAll(runId: Int, results: ObservationRunResultsAspect):
  ObservationRunResultsAspect = {
    val expressions = observables.collect {
      case expression: Expr => expression
    }

    val trajectories = for {
      expression: Expr <- expressions
    } yield (expression.asInstanceOf[Observable[_]], expression.evaluate(results))

    new ObservationRunResultsAspect(runId, trajectories.toMap)
  }

  case class Expr(f: Map[Observable[Double], Double] => Double) extends Observable[Double] {

    def evaluate(data: ObservationRunResultsAspect): Trajectory[Double] = {

      // get all observations with double values
      val observationList = for {
        (expr: Observable[_], traj) <- data.all
        (time: Double, value: Double) <- traj
      } yield (time, (expr.asInstanceOf[Observable[Double]], value))

      // format observations as a map of time => a map of variable => value
      val observableValues = for ((time, iter) <- observationList.groupBy(_._1)) yield
        (time, iter.map(_._2).toMap)

      val trajectory = for ((time, values) <- observableValues) yield (time, f(values))

      trajectory.toList.sorted
    }
  }
}
