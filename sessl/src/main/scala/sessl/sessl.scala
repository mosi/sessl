/**
 * *****************************************************************************
 * Copyright 2012 Roland Ewald
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ****************************************************************************
 */

import java.net.URI

import scala.language.implicitConversions

/**
 * Some general definitions.
 *
 *  @author Roland Ewald
 *
 */
package object sessl {

  /** Type for time-stamped data. */
  type TimeStampedData[T] = (Double, T)

  /** Type for trajectories. */
  type Trajectory[T] = List[TimeStampedData[T]]

  object Trajectory {

    val timePointColumnName = "t"

    import java.io.File
    import java.nio.charset.Charset

    import org.apache.commons.csv.{CSVFormat, CSVParser}

    import scala.collection.JavaConverters._

    def fromCSV(fileName: String): Map[String, Trajectory[Double]] = fromCSV(new File(fileName))

    def fromCSV(uri: URI): Map[String, Trajectory[Double]] = fromCSV(new File(uri))

    def fromCSV(file: File): Map[String, Trajectory[Double]] = {
      val parser = CSVParser.parse(
        file,
        Charset.defaultCharset(),
        CSVFormat.DEFAULT.withHeader())
      val headerKeys = parser.getHeaderMap.keySet().asScala
      val variables = (headerKeys - timePointColumnName).toList

      val lines = for (record <- parser.iterator().asScala) yield {
        val time = record.get(timePointColumnName).toDouble
        for (v <- variables) yield (time, record.get(v).toDouble)
      }
      val columns = lines.toList.transpose
      variables.zip(columns).toMap
    }
  }

  /** The type to represent variable assignments. List should be sorted lexicographically by variable name. */
  type VariableAssignment = List[(String, Any)]

  /**
   * Support for map-like syntax to define experiment variables.
   *  @param name the string to be interpreted as a variable name
   *  @return the VarName instance
   */
  implicit def stringToVarName(name: String): VarName = VarName(name)

  /**
   * Support for variables to be converted in a one-element list of 'multiple' variables (to combine them).
   *  @param v the variable
   *  @return a one-element list of 'multiple' variables
   */
  implicit def varToMultiVar[T](v: Variable[T]): MultipleVars = MultipleVars(List(v))

  /**
   * Support for combining stopping conditions.
   *  @param s the stopping condition
   *  @return a 'combined' stopping condition with just the left-hand side defined
   */
  implicit def stopConditionToCombinedCondition(s: StoppingCondition): CombinedStoppingCondition = CombinedStoppingCondition(s)

  /**
   * Support for combining replication conditions.
   *  @param r the replication condition
   *  @return a 'combined' replication condition with just the left-hand side defined
   */
  implicit def replConditionToCombinedCondition(r: ReplicationCondition): CombinedReplicationCondition = CombinedReplicationCondition(r)

  /**
   * Execute experiments sequentially.
   *  @param exps the experiments to be executed
   */
  def execute(exps: AbstractExperiment*): Unit = AbstractExperiment.execute(exps: _*)
}
