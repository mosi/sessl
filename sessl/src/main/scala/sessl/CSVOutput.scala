/*
 * Copyright 2017 University of Rostock
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package sessl

import java.io.{File, FileWriter}
import java.time.format.DateTimeFormatter

import org.apache.commons.csv.CSVFormat
import sessl.AbstractObservation.Observable

/**
  * A trait for outputting simulation results to "comma-separated values" (CSV) files.
  * Sensible, overridable defaults are defined for where these files shall be written and for what
  * gets written to them.
  * The default setup will create a structure like the following:
  * {{{
  * results_DATETIME   <- DATETIME is set to the experiment start time (yyyy-MM-dd-HH-mm-ss-SSS)
  *   config-0
  *     config.csv     <- this contains the variable assignment of this configuration
  *     run-0.csv      <- these contain the observed trajectories of all variables for one simulation run
  *     run-1.csv
  *     run-2.csv
  *     ...
  *   config-1
  *     config.csv
  *     run-10.csv
  *     run-11.csv
  *     ...
  *   ...
  * }}}
  *
  * @example
  * {{{
  * val exp = new Experiment with Observation with CSVOutput {
  *   // ...
  *
  *   // set the output file paths and names
  *   // these are the defaults, so these three lines can be omitted
  *   csvOutputDirectory(() => s"results-${expStartTime.toString}"
  *   csvConfigDir(id => s"config-$id")
  *   csvFileName(id => s"run-$id.csv")
  *
  *   // by default all observed values are written to the files
  *   // to filter the columns that the output files shall contain:
  *   csvColumns("observable1", "observable2")
  *
  *   // finally, use one of these:
  *   withRunResult(writeCSV)           // write after each finished run
  *   withReplicationsResult(writeCSV)  // write after each finished replication
  *   withExperimentResult(writeCSV)    // write after the experiment finishes
  * }
  * }}}
  * @author Tom Warnke
  */
trait CSVOutput {
  this: AbstractExperiment with AbstractObservation =>

  private[this] var outputDirectory: () => String = () => s"results-$formattedTimeStamp"

  private[this] var fileName: Int => String =
    id => s"run-$id.csv"

  private[this] var configDirName: Int => String =
    id => s"config-$id"

  private[this] var columnFilter: String => Boolean = (_ => true)

  def csvOutputDirectory(f: => String): Unit = outputDirectory = () => f

  def csvFileName(f: Int => String): Unit = fileName = f

  def csvConfigDir(f: Int => String): Unit = configDirName = f

  def csvDirectoryPrefix(prefix: String): Unit = outputDirectory =
          () => s"results-$prefix-$formattedTimeStamp"

  def csvColumns(observables: String*): Unit = columnFilter = observables.contains

  def formattedTimeStamp: String =
    DateTimeFormatter.ofPattern("yyyy-MM-dd-HH-mm-ss-SSS").format(expStartTime)

  /** to be called as withRunResults(writeCSV) */
  def writeCSV(results: ObservationRunResultsAspect): Unit = {

    val outDir = getOutDir()
    val replId = runAssignment(results.runId)
    val replDir = getReplDir(outDir, replId, results.variableAssignment)
    writeResultFile(replDir, results)

  }

  /** to be called as withReplicationsResults(writeCSV) */
  def writeCSV(results: ObservationReplicationsResultsAspect): Unit = {

    val outDir = getOutDir()
    val replDir = getReplDir(outDir, results.assignId, results.variableAssignment)

    // for each run of this config ...
    for ((_: Int, run: ObservationRunResultsAspect) <- results.runsResults) {
      writeResultFile(replDir, run)
    }
  }

  /** to be called as withExperimentResults(writeCSV) */
  def writeCSV(results: ObservationExperimentResultsAspect): Unit = {

    // create directory for csv files
    val outDir = getOutDir()

    // for each configuration ...
    for ((replId: Int, config: ObservationReplicationsResultsAspect) <- results
            .replicationsResults) {

      // create subdirectory for this config
      val replDir = getReplDir(outDir, replId, config.variableAssignment)

      // for each run of this config ...
      for ((_: Int, run: ObservationRunResultsAspect) <- config.runsResults) {
        writeResultFile(replDir, run)
      }
    }
  }

  def getOutDir(): File = {
    val outDir = new File(outputDirectory())
    require(!outDir.exists() || outDir.isDirectory,
      s"Could not create output directory ${outDir.getAbsolutePath}")
    outDir.mkdirs()
    outDir
  }

  def getReplDir(outDir: File,
                         replId: Int,
                         assignment: VariableAssignment): File = {
    val subDir = new File(outDir, configDirName(replId))
    if (subDir.exists()) {
      require(subDir.isDirectory, s"Could not create directory ${subDir.getAbsolutePath}")
    } else {
      // create replications directory
      subDir.mkdirs()

      // write config file
      val configFile = new File(subDir, "config.csv")
      configFile.createNewFile()
      CSVConfigurationWriter.write(assignment, new FileWriter(configFile))
    }
    subDir
  }

  private def writeResultFile(replDir: File,
                              run: ObservationRunResultsAspect): File = {
    val file = new File(replDir, fileName(run.runId))
    require(!file.exists(), s"Could not create file ${file.getAbsolutePath}")
    file.createNewFile()
    val obsNames = namedObservables.filterKeys(columnFilter)
    CSVTrajectoryWriter.write(run, new FileWriter(file), obsNames)
    file
  }
}

object CSVConfigurationWriter {
  def write(assignment: VariableAssignment, out: Appendable): Unit = {

    val csvPrinter = CSVFormat.DEFAULT.withHeader("var", "value").print(out)

    for ((varName, value) <- assignment)
    // make sure the varargs method overload gets called
      csvPrinter.printRecord(Array(varName, value).asInstanceOf[Array[Object]]: _*)

    csvPrinter.close()
  }
}

object CSVTrajectoryWriter {
  def write(runResults: ObservationRunResultsAspect,
            out: Appendable,
            observableNames: Map[String, Observable[_]]): Unit =
    write(runResults.all, out, observableNames)

  def write(trajectories: Map[String, Trajectory[_]],
            out: Appendable): Unit = {
    val namedObs = for ((name, traj) <- trajectories) yield {
      val obs = new Observable[Any] {}
      (name, obs)
    }
    val ts = for ((name, obs) <- namedObs) yield {
      (obs, trajectories(name))
    }
    write(ts, out, namedObs)
  }

  def write(results: Iterable[(Observable[_], Trajectory[_])],
            out: Appendable,
            observableNames: Map[String, Observable[_]]): Unit = {

    // obtain sorted list of the observables to write to the file
    val observedVars: List[(String, Observable[_])] = observableNames.toList.sortBy(_._1)

    // initialize csv printer with headers
    val headers = "time" :: observedVars.map(_._1)
    val csvPrinter = CSVFormat.DEFAULT.withHeader(headers: _*).withSystemRecordSeparator().print(out)

    // get all observations
    val observationList = for {
      (variable, traj) <- results
      (time: Double, value) <- traj
    } yield (time, (variable, value))

    // format observations as a map of time => a map of observable => value
    val observableValues = for {
      (time, iter) <- observationList.groupBy(_._1)
    } yield {
      val ov = iter.map(_._2)
      val obsMap = for {
        (obs, ov2) <- ov.groupBy(_._1)
      } yield {
        val observedValues = ov2.map(_._2)
        val processed = if (observedValues.size == 1) {
          // only one value -> extract it from list
          observedValues.head
        } else if (observedValues.head.isInstanceOf[Map[Long, Any]]) {
          // maps of values -> merge them
          observedValues.foldLeft(Map.empty[Long, Any])((acc, m) => acc ++ m.asInstanceOf[Map[Long, Any]])
        } else observedValues
        (obs, processed)
      }
      (time, obsMap)
    }

    // get all times with observations, sorted
    val sortedObservedValues = observableValues.toSeq.sortBy(_._1)

    for ((t, data) <- sortedObservedValues) {
      for (values <- linesForTimePoint(data, observedVars, t))
        // make sure the varargs method overload gets called
        csvPrinter.printRecord(values.asInstanceOf[Array[Object]]: _*)
    }
    csvPrinter.close()
  }

  def linesForTimePoint(data: Map[Observable[_], Any], names: List[(String, Observable[_])], time: Double):
  Seq[Array[Any]] = {

    val mapColumns = columnsWithMapValues(data)

    val ids = idsFromColumnsWithMaps(mapColumns)

    if (ids.isEmpty) {
      // just create a single line
      val valuesWithTime = time :: names.map { case (_, obs) => data.getOrElse(obs, "") }
      // need to convert the value list to an array so that the apache library correctly unwraps
      // the values
      Seq(valuesWithTime.toArray)
    } else {
      // create one line for each id, where for columns with map values the value is taken
      // from the map with the id as key, and simple values are processed as usual
      for (id <- ids) yield {
        time :: names.map { case (name, obs) =>
        if (mapColumns.contains(obs))
            mapColumns(obs).getOrElse(id, "")
          else
            data.getOrElse(obs, "")
        }
      }.toArray
    }
  }

  private def columnsWithMapValues(data: Map[Observable[_], Any]): Map[Observable[_], Map[String, Any]] =
    data.collect {
      case (name, map: Map[_, _]) =>
        (name, map.map { case (key, value) => (key.toString, value) })
    }

  private def idsFromColumnsWithMaps(data: Map[Observable[_], Map[String, Any]]): Seq[String] =
    data.flatMap(_._2.keys).toSeq.distinct

}
