/*
 * Copyright 2017 University of Rostock
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package sessl

/**
  * A trait to measure the time it takes to execute the experiment.
  * This does not give any information about the performance of single replications etc.
  * The execution time can only be accessed after the experiment was executed.
  *
  * @example
  * {{{
  * val exp = new Experiment with TimeMeasurement {
  *   // ...
  * }
  * execute(exp)
  * println(exp.executionTime.get)
  * }}}
  * @author Tom Warnke
  */
trait TimeMeasurement extends AbstractExperiment {

  private[this] var theExecutionTime = Option.empty[Long]

  abstract override def executeExperiment(): Unit = {
    val before = System.currentTimeMillis()
    super.executeExperiment()
    theExecutionTime = Some(System.currentTimeMillis() - before)
  }

  /**
    * @return the execution time in milliseconds
    */
  def executionTime: Option[Long] = theExecutionTime
}
