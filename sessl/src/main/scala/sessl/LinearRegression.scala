/*
 * Copyright 2017 University of Rostock
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package sessl

import org.apache.commons.math3.stat.correlation.Covariance
import org.apache.commons.math3.stat.regression.OLSMultipleLinearRegression
import sessl.AbstractObservation.Observable
import sessl.LinearRegression.Factor

import scala.math.sqrt
import scala.language.implicitConversions

/**
  * A trait for fitting a meta-model to observed simulation output via regression.
  * Higher-order factors can be created by combining variable names with operators {@code *} and {@code ^}.
  * Relies on the OLSMultipleLinearRegression in the Apache Commons Math library.
  *
  * @example {{{
  *            	withExperimentResult { result =>
  *                 // a, b, and c are input parameters, y has been observed
  *             	  val regr = fitLinearModel(result)("a", "b" * "c")("y")
  *             	  println(regr.beta mkString " ")
  *              }
  *          }}}
  * @author Tom Warnke
  */
trait LinearRegression {
  this: AbstractExperiment with AbstractObservation =>

  def fitLinearModel[T : Numeric](result: ObservationExperimentResultsAspect)(inVars: Factor*)(outVar: Observable[T]): LinearRegression.RegressionResults = {

    require(observables.contains(outVar), "The regression output variable was not observed.")

    // obtain the results of all runs of this experiment
    val runResults = result.runs.toArray

    // fill the data structures for the apache library line by line, i.e., run by run
    val in = for(res <- runResults) yield arrayFromAssignment(inVars, res)
    val out = for(res <- runResults) yield LinearRegression.toDouble(res.apply(outVar))

    // invoke the apache commons library
    val regression = new OLSMultipleLinearRegression
    regression.newSampleData(out, in)
    val beta = regression.estimateRegressionParameters()
    val parametersVariance = regression.estimateRegressionParametersVariance
    val regressandVariance = regression.estimateRegressandVariance
    val rSquared = regression.calculateRSquared

    val inOut = for ((i, o) <- in.zip(out)) yield i :+ o

    val inputOutputVariance = new Covariance(inOut).getCovarianceMatrix.getData

    val standardizedBeta = for (i <- 1 until beta.length) yield {
      beta(i) * sqrt(inputOutputVariance(i-1)(i-1)) / sqrt(inputOutputVariance(beta.length-1)(beta.length-1))
    }

    // create the result object
    new LinearRegression.RegressionResults(
      (new Factor(Seq()) +: inVars).toArray,
      in,
      out,
      beta,
      rSquared,
      parametersVariance,
      regressandVariance,
      inputOutputVariance,
      standardizedBeta.toArray)
  }

  /** a method to create an array that contains the factor values in the order the factors are listed in inVars */
  private def arrayFromAssignment(inVars: Seq[Factor], res: ObservationRunResultsAspect): Array[Double] = {
    val assignment = res.variableAssignment.toMap
    val values = inVars map (_.calculate(assignment))
    values.toArray
  }

  implicit def stringToFactor(s: String): Factor = new Factor(Seq(s))
}

object LinearRegression {

  // cast an Any to Double to become input for the apache library
  private def toDouble(x: Any): Double = x match {
    case i: Int => i.toDouble
    case d: Double => d
    // might be necessary to add more cases at some point
  }

  /** a class for regression input factors including higher-order factors */
  class Factor(val vars: Seq[String]) {
    /** create a new factor by multiplying it with an additional variable */
    def *(next: String): Factor = new Factor(vars :+ next)

    /** calculate the value of a factor for a given variable assignment */
    def calculate(assignment: Map[String, Any]): Double = {
      val values = vars.map(assignment).map(toDouble)
      values.product
    }
  }

  class RegressionResults(val factors: Array[Factor],
                          val inValues: Array[Array[Double]],
                          val outValues: Array[Double],
                          val beta: Array[Double],
                          val rSquared: Double,
                          val parametersVariance: Array[Array[Double]],
                          val regressandVariance: Double,
                          val inputOutputVariance: Array[Array[Double]],
                          val standardizedBeta: Array[Double]
                         ) {

    def fittedFunction: String = {

      val polynomialComponents = for (i <- factors.indices) yield {
        val b = beta(i)
        val betaString = (if (b >= 0) "+ " else "- ") + math.abs(b)
        val fs = betaString +: factors(i).vars
        fs mkString " * "
      }

      polynomialComponents mkString " "
    }

  }
}