/**
 * *****************************************************************************
 * Copyright 2012 Roland Ewald
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ****************************************************************************
 */
package sessl

import sessl.AbstractObservation.Observable
import sessl.util.{ResultOperations, TrajectoryOperations}

import scala.collection.mutable
import scala.language.implicitConversions

/**
 * Support for observation of model output. The observation trait is also concerned with managing the observed data in simple way,
 *  ie. provides read-access to be used across other traits (mixed-in later).
 *
 *  @author Roland Ewald
 *
 */
trait AbstractObservation extends ExperimentConfiguration with TrajectoryOperations {
  this: AbstractExperiment =>

  import AbstractObservation._

  /** The exact times at which the state shall be observed.*/
  private[this] var times: Option[List[Double]] = None

  /** The range of times at which the state shall be observed.*/
  private[this] var timeRange: Option[ValueRange[Double]] = None

  /** The observables */
  private[this] val theObservables = mutable.Set.empty[Observable[_]]

  /** The observables that have been given names */
  private[this] val observableNames = mutable.Map.empty[String, Observable[_]]

  /**
   * Observe at specific time steps.
   * @param observationTimes list of time points at which observation snapshots shall be taken
   * @example {{{
   * 	observeAt(0.2, 10.2, 35.4) // Triggers observation after 0.2, 10.2, and 35.4 units simulation time
   * }}}
   */
  final def observeAt(observationTimes: Double*): Unit = {
    times = Some(observationTimes.toList)
  }

  /**
   * Observe at a specific range of times.
   *  @param r a [[ValueRange]]
   *  @example {{{
   *  	observeAt(range(10,5,100)) // Triggers observation after 10, 15, ..., 95, 100 units simulation time
   *  }}}
   */
  final def observeAt[T <: AnyVal](r: ValueRange[T])(implicit n: Numeric[T]): Unit = {
    timeRange = Some(ValueRange[Double](n.toDouble(r.from), n.toDouble(r.step), n.toDouble(r.to)))
  }

  /**
    * Observe a specific observable.
    * The observables are typically binding-specific and obtained from the Observation traits of
    * the individual bindings.
    *
    * @param obs the observable
    * @return the observable
    * @example {{{
    *   // observables is named "x"
    *   observe("x" ~ observableSpecification())
    *
    *   // observable is saved in a val
    *   val x = observe(observableSpecification())
    *
    *   // combination of the above
    *   val x = observe("x" ~ observableSpecification())
    * }}}
    */
  def observe[T](obs: Observable[T], obss: Observable[_]*): Observable[T] = {
    theObservables += obs
    theObservables ++= obss
    obs
  }

  /**
   * Add result handler that processes observed model output from a single run. The result is an [[ObservationRunResultsAspect]].
   *  @param f result handler
   *  @example {{{
   *  	withRunResult { result =>
   *   		println(result)
   *    }
   *  }}}
   */
  def withRunResult(f: ObservationRunResultsAspect => Unit): Unit = afterRun {
    r => f.apply(
      r.aspectFor(classOf[AbstractObservation]).get.asInstanceOf[ObservationRunResultsAspect])
  }

  /**
   * Add result handler that processes observed model output from a set of replications.
   * The result is an [[ObservationReplicationsResultsAspect]].
   * @param f result handler
   * @example {{{
   *  	withReplicationsResult { result=>
   *   		println(result)
   *    }
   *  }}}
   */
  def withReplicationsResult(f: ObservationReplicationsResultsAspect => Unit): Unit = {
    afterReplications {
      r => f.apply(r.aspectFor(classOf[AbstractObservation]).get
              .asInstanceOf[ObservationReplicationsResultsAspect])
    }
  }

  /**
   * Add result handler that processes observed model output from the whole experiment.
   *  The result is an [[ObservationExperimentResultsAspect]].
   * @param f result handler
   * @example {{{
   *  	withExperimentResult { result =>
   *   		println(result)
   *    }
   *  }}}
   */
  def withExperimentResult(f: ObservationExperimentResultsAspect => Unit): Unit = {
    afterExperiment {
      r => f.apply(r.aspectFor(classOf[AbstractObservation]).get
              .asInstanceOf[ObservationExperimentResultsAspect])
    }
  }

  /** All simulation time points at which the values of observed variables shall be stored. */
  lazy val observationTimes: Vector[Double] = {
    val unsortedTimes: List[Double] = {
      if (times.isDefined)
        times.get
      else if (timeRange.isDefined)
        timeRange.get.toList
      else {
        logger.warn("Neither specific times nor a time range is given for observation.")
        Nil
      }
    }

    val sortedTimes = unsortedTimes.sorted
    if (sortedTimes.nonEmpty)
      require(fixedStopTime.isEmpty || (sortedTimes.last <= fixedStopTime.get), "Observation time of '" + sortedTimes.last +
        "' cannot be reached (fixed stop time is: '" + fixedStopTime.get + "'). Ordered list of configured observation times: " + sortedTimes.mkString(", "))
    sortedTimes.toVector
  }

  /** Allows to checks whether *some* observation times have been defined. */
  def isObservationTimingDefined: Boolean = times.isDefined || timeRange.isDefined

  /** The observables */
  def observables: Set[Observable[_]] = theObservables.toSet

  /** Before the run is done, add all results of this run to the experiment. */
  override def collectRunResultsAspects(runId: Int): Unit = {
    super.collectRunResultsAspects(runId)
    addRunResultsAspect(runId, collectResults(runId, removeData = true))
  }

  /** Before the replications are done, add all results of this run to the experiment. */
  override def collectReplicationsResultsAspects(assignId: Int): Unit = {
    super.collectReplicationsResultsAspects(assignId)
    addReplicationsResultsAspect(assignId, collectReplicationsResults(assignId))
  }

  /** Before the experiment is done, add result aspect for observation. */
  override def collectExperimentResultsAspects(): Unit = {
    super.collectExperimentResultsAspects()
    addExperimentResultsAspect(new ObservationExperimentResultsAspect())
  }

  /**
   * Collects the results of the indicated run. If the removeData flag is set to true,
   *  the observation sub-system may regard the data as read-out (and hence delete it).
   *
   *  @param runID the ID of the run
   *  @param removeData flag to signal that the data will not be required again (and can hence be dismissed)
   *  @return the result aspect of the run (w.r.t. observation)
   */
  def collectResults(runID: Int, removeData: Boolean): ObservationRunResultsAspect

  /**
   * Signals that all results for the given configuration ID have been collected.
   *  Override to clean up auxiliary data structures.
   */
  def collectReplicationsResults(assignID: Int): ObservationReplicationsResultsAspect =
    new ObservationReplicationsResultsAspect(assignID)

  /**
    * This implicit class enables naming observables succinctly
    * @param name the name that is mapped to the observable
    *
    * @example {{{
    *   observe("x" ~ observableSpecification())
    * }}}
    *
    */
  implicit class ObservableName(name: String) {
    def ~[T](obs: Observable[T]): Observable[T] = {
      observableNames(name) = obs
      obs
    }
  }

  /**
    * @return the mapping of names to observables
    */
  def namedObservables: Map[String, Observable[_]] = observableNames.toMap

  /**
    * Get the observable that is mapped to a specific name or create one
    * @param name the name
    * @param obs the new observable
    * @return the observable that is mapped to the name, if it existed, or the newly created observable
    */
  protected def getOrElseUpdate[T](name: String, obs: => Observable[T]): Observable[T] =
    observableNames.getOrElseUpdate(name, obs).asInstanceOf[Observable[T]]

  /**
    * Obtain an observable that a name has been mapped to before.
    * @param name a name that is mapped to an observable
    * @return the according observable
    */
  implicit def lookupObservable[T](name: String): Observable[T] =
    observableNames(name).asInstanceOf[Observable[T]]

}

object AbstractObservation {

  /**
    * A specification of a specific observation during a simulation run.
    *
    * @tparam T the type of the observed values
    */
  trait Observable[+T]
}

/**
 * The [[RunResultsAspect]] for [[AbstractObservation]]. Additional methods to work on values are provided by trait [[util.ResultOperations]].
 *
 *  @param data the data recorded for a single run: observable => trajectory.
 */
class ObservationRunResultsAspect(val runId: Int, private var data: Map[Observable[_], Trajectory[_]])
        extends RunResultsAspect(classOf[AbstractObservation]) with ResultOperations {

  /** Auxiliary constructor to merge two result sets (e.g. recorded by different entities but for the same run).*/
  def this(runId: Int, aspects: (ObservationRunResultsAspect, ObservationRunResultsAspect)) = {
    this(runId, aspects._1.data ++ aspects._2.data)
  }

  /**
   * Get the _last_ recorded value of an observable.
   *  @param obs the observable
   *  @example {{{
   *  	withRunResult { result =>
   *   		println("last value: " + result("x"))
   *    }
   *  }}}
   */
  def apply[T](obs: Observable[T]): T = {
    val values = getVarData[T](obs)
    require(values.nonEmpty, "No values stored for observable '" + obs + "'!")
    values.last._2
  }

  /**
   * Checks whether an observable is defined in the results.
   *  @param obs the observable
   *  @return true if there is data stored for this observable
   *  @example {{{
   *  	withRunResult { result =>
   *   		if(result ? "x") { ... }
   *    }
   *  }}}
   */
  def ?(obs: Observable[_]): Boolean = data.contains(obs)

  /**
   * Get the [[Trajectory]] for a given observable
   *  @param obs the observable
   *  @return trajectory
   *  @example {{{
   *  	withRunResult { result =>
   *   		println(result.trajectory("x"))
   *    }
   *  }}}
   */
  def trajectory[T](obs: Observable[T]): Trajectory[T] = getVarData[T](obs)

  /**
   * Get tuple of an observable and [[Trajectory]].
   *  @param obs the observable
   *  @return tuple of an observable and [[Trajectory]]
   *  @example {{{
   *  	withRunResult { result =>
   *      reportSection("Example") {
   *   		linePlot(result ~ "x")(title = "Example line plot")
   *     }
   *    }
   *  }}}
   */
  def ~[T](obs: Observable[T]): (Observable[T], Trajectory[T]) = (obs, getVarData(obs))

  /**
   * Get all data in a manner that is easy to plot.
   *  @return sequence with tuples of observable and corresponding [[Trajectory]]
   *  @example {{{
   *  	withRunResult { result =>
   *      reportSection("Example") {
   *   		linePlot(result.all)(title = "Line plot for all variables")
   *     }
   *    }
   *  }}}
   */
  def all: Iterable[(Observable[_], Trajectory[_])] = names.map(this ~ _)

  /**
   * Get all times at which the data was observed.
   *  @param obs the observable
   *  @return all time stamps for which data is available
   */
  def times[T](obs: Observable[T]): Iterable[Double] = getVarData(obs).map(_._1)

  /**
   * Get all values that have been observed.
   *  @param obs the observable
   *  @return all values
   */
  def values[T](obs: Observable[T]): Iterable[T] = getVarData(obs).map(_._2)

  /**
   * Get the names of all defined observables.
   *  @return all variable names
   */
  lazy val names: List[Observable[_]] = data.keys.toList

  /** Checks whether the observable is defined and returns it if possible. */
  private def getVarData[T](obs: Observable[T]): Trajectory[T] = {
    require(this ? obs, "Observable '" + obs + "' is not defined!")

    def conv(d: TimeStampedData[_]): TimeStampedData[T] = {
      val (time, value) = d
      (time, value.asInstanceOf[T])
    }

    data(obs).map(conv)
  }

  override protected def getValuesFor[T](obs: Observable[T]) = values(obs)
}

/** The [[ReplicationsResultsAspect]] for [[AbstractObservation]]. */
class ObservationReplicationsResultsAspect(val assignId: Int) extends ReplicationsResultsAspect(classOf[AbstractObservation]) with ResultOperations {

  /**
    * Get the results of all observed runs
    * @return all observed runs
    */
  def runs: Iterable[ObservationRunResultsAspect] = runsResults.values.collect {
    case r: ObservationRunResultsAspect => r
  }

  def trajectories[T](obs: Observable[T]): Iterable[Trajectory[T]] =
    runs.map(run => run.trajectory(obs))

  /**
   * Get the _last_ recorded value of the specified observable for all runs.
   *  @param obs the observable
   *  @return the last recorded value for each run
   */
  def apply[T](obs: Observable[T]): Iterable[T] =
    runs.map(r => r[T](obs))

  /**
   * Apply name to the result, combine results in _named_ tuple.
   *  @param obs the observable
   *  @return tuple with variable name and all last observed results
   *  @example {{{
   *  withReplicationsResult { result =>
   *  	reportSection("Sample Histogram") {
   * 		histogram(result ~ ("x"))(title = "Last observed values of 'x'")
   * 	}
   *  }}}
   */
  def ~[T](obs: Observable[T]) = (obs, apply(obs))

  override protected def getValuesFor[T](obs: Observable[T]) = apply(obs)
}

/**
 * [[ExperimentResultsAspect]] for [[AbstractObservation]]. Additional methods are provided by [[util.ResultOperations]] and [[PartialExperimentResults]].
 */
class ObservationExperimentResultsAspect extends ExperimentResultsAspect(classOf[AbstractObservation]) with ResultOperations
  with PartialExperimentResults[ObservationExperimentResultsAspect] {

  /**
    * Get the results of all observed runs
    * @return all observed runs
    */
  def runs: Iterable[ObservationRunResultsAspect] = runsResults.values.collect {
    case r: ObservationRunResultsAspect => r
  }

  def trajectories[T](obs: Observable[T]): Iterable[Trajectory[T]] =
    runs.map(run => run.trajectory(obs))

  /**
    * Get the results of all observed replications
    * @return all observed replications
    */
  def replications: Iterable[ObservationReplicationsResultsAspect] = replicationsResults.values.collect {
    case r: ObservationReplicationsResultsAspect => r
  }

  /**
   * Get the _last_ recorded value of the specified observable for all runs.
   *  @param obs the observable
   *  @return the last recorded value for each run
   */
  def apply[T](obs: Observable[T]) = runs.map(r => r[T](obs)).toList

  /**
   * Apply name to the result, combine results in _named_ tuple.
   *  @param obs the observable
   *  @return tuple with observable and all last observed results
   *  @example {{{
   *  withExperimentResult { result =>
   *  	reportSection("Sample Histogram") {
   * 		histogram(result ~ ("x"))(title = "Last observed values of 'x'")
   * 	}
   *  }}}
   */
  def ~[T](obs: Observable[T]): (Observable[T], List[T]) = (obs, apply(obs))

  override protected def getValuesFor[T](obs: Observable[T]) = apply(obs)

  override def createPartialResult(runsResults: mutable.Map[Int, RunResultsAspect], replicationsResults: mutable.Map[Int, ReplicationsResultsAspect]):
  ObservationExperimentResultsAspect = {
    val aspect = new ObservationExperimentResultsAspect
    aspect.setResults(runsResults, replicationsResults)
    aspect
  }
}
