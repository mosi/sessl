/*
 * Copyright 2018 University of Rostock
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package sessl

import java.io.File
import java.net.URI
import java.nio.charset.Charset

import org.apache.commons.csv.{CSVFormat, CSVParser}

/**
  * This trait can be used to read in an experiment design from a CSV file.
  * The header of the CSV file should contain the variable names.
  * Each line after the header is used as a design point.
  *
  * @author Tom Warnke
  */
trait CSVInput {
  this: AbstractExperiment =>

  def designFromCSV(fileName: String): Unit = designFromCSV(new File(fileName))

  def designFromCSV(uri: URI): Unit = designFromCSV(new File(uri))

  def designFromCSV(file: File): Unit = {
    import scala.collection.JavaConverters._

    val parser = CSVParser.parse(file,
                                 Charset.defaultCharset(),
                                 CSVFormat.DEFAULT.withHeader())

    val headerMap = parser.getHeaderMap.asScala

    val designPoints = for {
      record <- parser.iterator().asScala
    } yield {
      val varValues = headerMap.map {
        case (varName, idx) =>
          VarSingleVal(varName, record.get(idx).toDouble)
      }.toList
      Config(varValues: _*)
    }

    configs(designPoints.toList: _*)
  }
}
