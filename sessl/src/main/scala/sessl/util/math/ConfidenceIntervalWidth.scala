/*
 * Copyright 2017 University of Rostock
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package sessl.util.math

import sessl.AbstractObservation.Observable
import sessl.ObservationRunResultsAspect
import sessl.util.Logging

import scala.util.Try

/**
  * @author Tom Warnke
  */
object ConfidenceIntervalWidth extends Logging {

  /** Computes the width of the confidence interval for the given confidence and the given trajectories at the given
    * time */
  def computeRelativeHalfWidth(obs: Observable[Double], confidence: Double, time: Double, results: List[ObservationRunResultsAspect]): Double = {

    val trajectories = results.map(_.trajectory(obs))
    val values = trajectories.map(t => t.toMap.apply(time).asInstanceOf[java.lang.Double])
    computeRelativeHalfWidth(values, confidence)

  }

  /** Computes the width of the confidence intervals for the given confidence and the given trajectories at all observed
    * times.
    * Assumes that all trajectories contain observations for the same list of time points. */
  def computeRelativeHalfWidth(obs: Observable[Double], confidence: Double, results: List[ObservationRunResultsAspect]): List[Double] = {

    val trajectories = results.map(_.trajectory(obs))
    val castedTrajectories = trajectories.map(_.toMap.mapValues(_.asInstanceOf[java.lang.Double]))

    // here we take the time points to use from the first trajectory...
    val times = castedTrajectories.head.keys.toList

    for (time <- times) yield
    // ... and assume that all trajectories contain an observation for these time points
      computeRelativeHalfWidth(castedTrajectories.map(_.apply(time)), confidence)
  }

  def computeMeanAndCI(values: List[Double], confidence: Double): (Double, Double, Double) =
    computeMeanAndCIHalfWidth(values.map(Double.box), confidence) match {
      case (mean, Some(halfWidth)) => (mean, mean - halfWidth, mean + halfWidth)
      case (mean, None) => (mean, Double.NaN, Double.NaN)
    }


  private def computeMeanAndCIHalfWidth(values: List[java.lang.Double], confidence: Double): (Double, Option[Double]) = {
    import org.apache.commons.math3.distribution.TDistribution
    import org.apache.commons.math3.stat.descriptive.SummaryStatistics

    val summaryStatistics = new SummaryStatistics()
    for (v <- values) summaryStatistics.addValue(v)

    val mean = summaryStatistics.getMean

    val marginOfError = Try {
      // taken from http://stattrek.com/estimation/confidence-interval.aspx
      val standardError = summaryStatistics.getStandardDeviation / Math.sqrt(summaryStatistics.getN)
      val alpha = 1 - confidence
      val critProbability = 1 - (alpha / 2)
      val degreesOfFreedom = summaryStatistics.getN - 1
      val tDistribution = new TDistribution(degreesOfFreedom)
      val critValue = tDistribution.inverseCumulativeProbability(critProbability)
      critValue * standardError
    }

    (mean, marginOfError.toOption.map { margin => margin/mean })
  }

  private def computeRelativeHalfWidth(values: List[java.lang.Double], confidence: Double): Double =
    computeMeanAndCIHalfWidth(values, confidence) match {
      case (_, Some(halfWidth)) => halfWidth
      case (_, None) => Double.NaN
    }

}
