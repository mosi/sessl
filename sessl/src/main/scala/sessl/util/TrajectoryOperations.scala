/*
 * Copyright 2018 University of Rostock
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package sessl.util

import sessl.Trajectory
import sessl.util.math.ConfidenceIntervalWidth

/**
  * @author Tom Warnke
  */
trait TrajectoryOperations {

  implicit class TrajectoryOps[T](val trajectory: Trajectory[T]) {
    def times: List[Double] = trajectory.map(_._1)
    def values: List[T] = trajectory.map(_._2)

    def mapValues[U](f: T => U): Trajectory[U] = trajectory.map {
      case (time, value) => (time, f(value))
    }

    /**
      * Creates a new trajectory of pairs of values of the two given trajectories.
      * The result contains only times that are present in both given trajectories.
      */
    def zipTimes[U](other: Trajectory[U]): Trajectory[(T, U)] = {
      val commonTimes = trajectory.times.filter(other.times.contains)
      val (map1, map2) = (trajectory.toMap, other.toMap)
      for (t <- commonTimes) yield (t, (map1(t), map2(t)))
    }

    def zipWith[U, V](other: Trajectory[U])(f: (T, U) => V): Trajectory[V] =
        this.zipTimes(other).mapValues(f.tupled)

    def +(other: Trajectory[T])(implicit T: Numeric[T]): Trajectory[T] =
      zipWith(other)(T.plus)

    def -(other: Trajectory[T])(implicit T: Numeric[T]): Trajectory[T] =
      zipWith(other)(T.minus)

    def *(other: Trajectory[T])(implicit T: Numeric[T]): Trajectory[T] =
      zipWith(other)(T.times)

    def /(other: Trajectory[T])(implicit T: Fractional[T]): Trajectory[T] =
      zipWith(other)(T.div)

    def abs(implicit T: Numeric[T]): Trajectory[T] =
      mapValues(T.abs)

    def +(t: T)(implicit T: Numeric[T]): Trajectory[T] =
      mapValues(T.plus(_, t))

    def -(t: T)(implicit T: Numeric[T]): Trajectory[T] =
      mapValues(T.minus(_, t))

    def *(t: T)(implicit T: Numeric[T]): Trajectory[T] =
      mapValues(T.times(_, t))

    def /(t: T)(implicit T: Fractional[T]): Trajectory[T] =
      mapValues(T.div(_, t))

    def <=(other: Trajectory[T])(implicit T: Ordering[T]): Boolean =
      zipWith(other)(T.compare(_, _) <= 0).forall(_._2)

    def <(other: Trajectory[T])(implicit T: Ordering[T]): Boolean =
      zipWith(other)(T.compare(_, _) < 0).forall(_._2)

    def >=(other: Trajectory[T])(implicit T: Ordering[T]): Boolean =
      zipWith(other)(T.compare(_, _) >= 0).forall(_._2)

    def >(other: Trajectory[T])(implicit T: Ordering[T]): Boolean =
      zipWith(other)(T.compare(_, _) > 0).forall(_._2)

    def <=(t: T)(implicit T: Ordering[T]): Boolean =
      values.forall(T.compare(_, t) <= 0)

    def <(t: T)(implicit T: Ordering[T]): Boolean =
      values.forall(T.compare(_, t) < 0)

    def >=(t: T)(implicit T: Ordering[T]): Boolean =
      values.forall(T.compare(_, t) >= 0)

    def >(t: T)(implicit T: Ordering[T]): Boolean =
      values.forall(T.compare(_, t) > 0)

    def pairs[U, V](f: (T, T) => U)(g: List[U] => V): V =
      g((values.zip(values.tail)).map(f.tupled))

    def increasing(implicit T: Ordering[T]): Boolean =
      pairs(T.compare)(_.forall(_ <= 0))

    def strictlyIncreasing(implicit T: Ordering[T]): Boolean =
      pairs(T.compare)(_.forall(_ < 0))

    def decreasing(implicit T: Ordering[T]): Boolean =
      pairs(T.compare)(_.forall(_ >= 0))

    def strictlyDecreasing(implicit T: Ordering[T]): Boolean =
      pairs(T.compare)(_.forall(_ > 0))
  }

  implicit class TrajectoryIterOps[T : Numeric](val trajectories: Iterable[Trajectory[T]]) {

    private def aggregate[U](f: Iterable[T] => U): Trajectory[U] = {
      if (trajectories.nonEmpty) {
        require(trajectories.forall(t => t.times == trajectories.head.times))

        def rec(ts: Iterable[Trajectory[T]]): Trajectory[U] = {
          if (ts.head.isEmpty)
            List.empty
          else {
            (ts.head.head._1, f(ts.map(_.head._2))) :: rec(ts.map(_.tail))
          }
        }
        rec(trajectories)
      } else
        List.empty
    }

    def mean: Trajectory[Double] = {
      val ev = implicitly[Numeric[T]]
      aggregate(ts => ev.toDouble(ts.sum) / ts.size)
    }

    def meanWithCI(confidence: Double = 0.95): Trajectory[(Double, Double, Double)] = {

      val ev = implicitly[Numeric[T]]
      aggregate { ts =>
        val values = ts.map(ev.toDouble).toList
        ConfidenceIntervalWidth.computeMeanAndCI(values, confidence)
      }

    }

  }

}

