/**
 * *****************************************************************************
 * Copyright 2012 Roland Ewald
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ****************************************************************************
 */
package sessl.util

import sessl.AbstractObservation.Observable

/**
 * Provides support for some basic operations on (numeric) result data.
 *
 *  @author Roland Ewald
 */
trait ResultOperations {

  /**
   * Retrieves the data for an observable.
   *  @param obs observable
   *  @return values stored for that name
   */
  protected def getValuesFor[T](obs: Observable[T]): Iterable[T]

  /**
   * Get the maximum value of an observable.
   *  @param obs observable
   *  @return the maximum
   */
  def max[T : Numeric](obs: Observable[T]): Double = aggregate(obs, Double.NegativeInfinity, scala.math.max)

  /**
   * Get the minimum value of an observable.
   *  @param obs observable
   *  @return the minimum
   */
  def min[T : Numeric](obs: Observable[T]): Double = aggregate(obs, Double.PositiveInfinity, scala.math.min)

  /**
   * Get the absolute minimum value of an observable.
   *  @param obs observable
   *  @return the minimal absolute value
   */
  def absmin[T : Numeric](obs: Observable[T]): Double = aggregate(obs, Double.PositiveInfinity, (x, y) => scala.math.min(scala.math.abs(x), scala.math.abs(y)))

  /**
   * Get the absolute maximum value of an observable.
   *  @param obs observable
   *  @return the maximum absolute value
   */
  def absmax[T : Numeric](obs: Observable[T]): Double = aggregate(obs, 0, (x, y) => scala.math.max(scala.math.abs(x), scala.math.abs(y)))

  /**
   *  Gets the mean value of an observable.
   *  @param obs observable
   *  @return the mean value
   */
  def mean[T : Numeric](obs: Observable[T]): Double = {
    val numOfSamples: Double = getValuesFor(obs).size
    aggregate(obs, 0, (x, y) => x + y / numOfSamples)
  }

  /**
   *  Gets the (unbiased) standard deviation of an observable.
   *  @param obs observable
   *  @return the standard deviation of the values
   */
  def stddev[T : Numeric](obs: Observable[T]): Double = scala.math.sqrt(variance(obs))

  /**
   *  Gets the (unbiased) variance of an observable.
   *  @param obs observable
   *  @return the variance of the values
   */
  def variance[T : Numeric](obs: Observable[T]): Double = {
    val m = mean(obs)
    val denominator: Double = getValuesFor(obs).size - 1
    require(denominator > 0, "Variance cannot be estimated based on a single value!")
    aggregate(obs, 0, (x, y) => { x + scala.math.pow(y - m, 2) / denominator })
  }

  /**
   * Calculate RMSE of observed results compared to specific sequence of values.
   * @param obs observable
   * @param comparisonData the sequence of comparison data, must be same size
   */
  def mse[T : Numeric](obs: Observable[T], comparisonData: Seq[Double]): Double = math.Misc.mse(getNumericValues(obs).toSeq, comparisonData)

  /**
   * Calculate MSE of observed results compared to specific sequence of values.
   * @param obs observable
   * @param comparisonData the sequence of comparison data, must be same size
   */
  def rmse[T : Numeric](obs: Observable[T], comparisonData: Seq[Double]): Double = Math.sqrt(mse(obs, comparisonData))

  /**
   * Aggregates observed values by a given function.
   *
   *  @param obs
   *          the observable
   *  @param startVal
   *          the start value
   *  @param aggregator
   *          the aggregation function (applied in a left fold)
   *  @return the aggregated result
   */
  private def aggregate[T : Numeric](obs: Observable[T], startVal: Double, aggregator: (Double, Double) => Double): Double = {
    val numbers = getNumericValues(obs)
    (startVal /: numbers)((x, y) => aggregator(x, y))
  }

  /**
   * Get data as numeric values.
   *
   *  @param obs observable
   */
  private def getNumericValues[T](obs: Observable[T])(implicit ev: Numeric[T]): Iterable[Double] = {
    val vals = getValuesFor(obs)
    vals.map(ev.toDouble)
  }
}
