/*******************************************************************************
 * Copyright 2012 Roland Ewald
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package sessl.util

import sessl.AbstractObservation.Observable
import sessl.{AbstractExperiment, AbstractObservation, ObservationRunResultsAspect, TimeStampedData, Trajectory}

import scala.collection.mutable

/** Provides a simple observation mechanism that supports the general contract of the observation trait.
 *
 *  @author Roland Ewald
 */
trait SimpleObservation extends AbstractObservation {
  this: AbstractExperiment =>

  /** A naive specification of an in-memory 'database': just a map from run ID => a map of observables to trajectories. */
  private[this] val inMemoryDatabase = mutable.Map.empty[Int, mutable.Map[Observable[_], Trajectory[_]]]

  /** Adds value to the internal in-memory 'database'.
   *
   *  @param runID the ID of the simulation run
   *  @param observable the observable
   *  @param value the observed value for the observable
   */
  protected[sessl] def addValueFor[T](runID: Int, observable: Observable[T], value: TimeStampedData[T]): Unit = inMemoryDatabase.synchronized {
    require(observables.contains(observable), "Observable '" + observable + "' is not known.")
    val runResults = inMemoryDatabase.getOrElseUpdate(runID, mutable.Map())
    runResults += ((observable, value :: runResults.getOrElse(observable, Nil)))
    logger.debug("Added results for run " + runID + ":" + value)
  }

  /** Collects run results from 'database'. */
  override def collectResults(runId: Int, removeData: Boolean = false): ObservationRunResultsAspect = inMemoryDatabase.synchronized {
    if (!inMemoryDatabase.contains(runId)) {
      logger.warn("Warning: no results were observed (have you configured the observation?)... creating empty results.")
      new ObservationRunResultsAspect(runId, Map[Observable[_], Trajectory[_]]())
    } else {
      val runData = if (removeData) inMemoryDatabase.remove(runId).get else inMemoryDatabase(runId)
      val correctlyOrderedData = runData.map(entry => (entry._1, entry._2.reverse))
      new ObservationRunResultsAspect(runId, correctlyOrderedData.toMap)
    }
  }
}
