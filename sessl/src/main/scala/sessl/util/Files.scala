/*
 * Copyright 2016 Danhua Peng
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package sessl.util

import java.io.File

/**
  * @author Danhua Peng
  */
object Files {

  def deleteFileOrDirectory(file: File): Unit = {
    if (file.isDirectory) {
      val fs = file.listFiles()
      fs foreach deleteFileOrDirectory
    }
    java.nio.file.Files.delete(file.toPath)
  }
}
