/*
 * Copyright 2017 University of Rostock
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package sessl

import org.junit.Assert._
import org.junit.Test
import sessl.reference.MockExperiment

/**
  * @author Tom Warnke
  */
@Test class TimeMeasurementTest {

  @Test def testTimeMeasurement: Unit = {

    val exp = new MockExperiment with TimeMeasurement {

      val x = observe(t => t * t)
      observeAt(range(0, 10, 100))

      scan("a" <~ range(1, 1, 5))

      replications = 3
    }

    assertTrue(exp.executionTime.isEmpty)
    execute(exp)
    assertTrue(exp.executionTime.isDefined)
  }
}
