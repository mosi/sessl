/*
 * Copyright 2018 University of Rostock
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package sessl

import org.junit.Assert._
import org.junit.Test
import sessl.reference.MockExperiment

import scala.collection.mutable

/**
  * @author Tom Warnke
  */
@Test class TestConfig {

  @Test def testConfig() = {

    val assignments = mutable.ArrayBuffer.empty[VariableAssignment]

    val exp = new MockExperiment {

      val x = observe(t => t * t)
      observeAt(0)

      replications = 1

      configs(
        Config("a" <~ 1, "b" <~ 11),
        Config("a" <~ 2, "b" <~ 12),
        Config("a" <~ 3, "b" <~ 13)
      )

      withRunResult { r =>
        assignments += r.variableAssignment
      }
    }

    execute(exp)

    assertTrue(assignments.size == 3)

    val expected: List[VariableAssignment] = List(
      List(("a", 1), ("b", 11)),
      List(("a", 2), ("b", 12)),
      List(("a", 3), ("b", 13))
    )

    assertEquals(expected.toSet, assignments.toSet)

  }

  @Test def testRandomSampling() = {

    val assignments = mutable.ArrayBuffer.empty[VariableAssignment]

    val exp = new MockExperiment {

      val x = observe(t => t * t)
      observeAt(0)

      replications = 1

      uniformRandomSampling(5, "a" <~ interval(1, 5), "b" <~ interval(11, 20))
      uniformRandomSampling(3, "c" <~ interval(0.0, 0.2))

      scan("d" <~ (1, 2, 3))

      withRunResult { r =>
        assignments += r.variableAssignment
      }
    }

    execute(exp)

    assertTrue(assignments.size == 45)
  }

}
