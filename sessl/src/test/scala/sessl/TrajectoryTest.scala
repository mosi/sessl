/*
 * Copyright 2018 University of Rostock
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package sessl

import org.junit.Assert._
import org.junit.Test
import sessl.util.TrajectoryOperations

/**
  * @author Tom Warnke
  */
@Test class TrajectoryTest {

  @Test def testReadTrajectory(): Unit = {

    val file = this.getClass.getResource("/trajectory.csv").toURI
    val t = Trajectory.fromCSV(file)
    assertEquals((0.0, 1.0), t("a").head)

  }

  @Test def zipTrajectories(): Unit = new TrajectoryOperations {

    val t1: Trajectory[String] = List((0.0, "a"), (1.0, "b"), (2.0, "c"))
    val t2: Trajectory[Boolean] = List((0.0, true), (2.0, false), (4.0, true))

    val zipped: Trajectory[(String, Boolean)] = t1.zipTimes(t2)

    assertEquals(2, zipped.length)
    assertEquals((0.0, ("a", true)), zipped(0))
    assertEquals((2.0, ("c", false)), zipped(1))

  }

  @Test def compareTrajectories(): Unit = new TrajectoryOperations {

    val t1: Trajectory[Int] = List((0, 0), (1, 1), (2, 2), (3, 3))
    val t2: Trajectory[Int] = List((0, 0), (1, 1), (2, 2), (3, 2))
    val t3: Trajectory[Int] = List((0, 0), (1, 1), (2, 2), (3, 0))

    assertTrue(t1.strictlyIncreasing)
    assertTrue(t1.increasing)

    assertFalse(t2.strictlyIncreasing)
    assertTrue(t2.increasing)

    assertFalse(t3.strictlyIncreasing)
    assertFalse(t3.increasing)

    assertTrue((t1 - t3) >= 0)
    assertFalse((t1 - t3) < 0)

    assertTrue((t1 >= t3))
    assertFalse((t1 > t3))

    assertTrue(t1 >= (t1 * -1))
    assertFalse(t1 <= (t1 * -1))

    assertTrue(t1 >= (t1 * -1).abs)
    assertTrue(t1 <= (t1 * -1).abs)

  }

}
