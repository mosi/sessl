/*
 * Copyright 2017 University of Rostock
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package sessl

import java.io.{File, StringWriter}

import org.junit.Assert._
import org.junit.Test
import sessl.AbstractObservation.Observable
import sessl.reference.MockExperiment
import sessl.util.Files

import scala.io.Source

/**
  * Testing the correct output of simulation results as CSV files
  *
  * @author Tom Warnke
  */
@Test class TestCSVOutput {

  @Test def testSimpleDataToLine(): Unit = {

    val a = new Observable[Int]{}
    val b = new Observable[String]{}

    val data = Seq(
      0.0 -> Map[Observable[_], Any](a -> 10, b -> "x"),
      1.0 -> Map[Observable[_], Any](a -> 11, b -> "y"),
      2.0 -> Map[Observable[_], Any](a -> 12, b -> "z")
    )

    val lines = data.flatMap { case (t, values) =>
      CSVTrajectoryWriter.linesForTimePoint(values, List("a" -> a, "b" -> b), t)
    }

    assertEquals(3, lines.length)
    assertTrue(Array(0.0, 10, "x") sameElements lines(0))
    assertTrue(Array(1.0, 11, "y") sameElements lines(1))
    assertTrue(Array(2.0, 12, "z") sameElements lines(2))
  }

  @Test def testDependentDataToLines(): Unit = {

    val id1 = "id1"
    val id2 = "id2"

    val a = new Observable[Int]{}
    val b = new Observable[String]{}
    val c = new Observable[Int]{}

    val data = Seq(
      0.0 -> Map[Observable[_], Any](
        a -> Map(id1 -> 10, id2 -> 20),
        b -> Map(id1 -> "x", id2 -> "xx"),
        c -> 0),
      1.0 -> Map[Observable[_], Any](
        a -> Map(id1 -> 11, id2 -> 21),
        b -> Map(id1 -> "y"),
        c -> 1
      )
    )

    val lines = data.flatMap { case (t, values) =>
      CSVTrajectoryWriter.linesForTimePoint(values, List("a" -> a, "b" -> b, "c" -> c), t)
    }

    assertEquals(4, lines.length)
    assertTrue(Array(0.0, 10, "x", 0) sameElements lines(0))
    assertTrue(Array(0.0, 20, "xx", 0) sameElements lines(1))
    assertTrue(Array(1.0, 11, "y", 1) sameElements lines(2))
    assertTrue(Array(1.0, 21, "", 1) sameElements lines(3))
  }

  @Test def outputGeneration(): Unit = {

    val trajWriter = new StringWriter()
    val configWriter = new StringWriter()

    // a "fake" experiment with predictable observations
    val exp = new MockExperiment with CSVOutput {

      val a = StringObservable[AnyVal]("a")
      val b = StringObservable[AnyVal]("b")

      withExperimentResult { result =>
        for ((_: Int, rr: ObservationRunResultsAspect) <- result.runsResults)
          CSVTrajectoryWriter.write(rr, trajWriter, Map("a" -> a, "b" -> b))
      }

      withExperimentResult { result =>
        for ((_: Int, rr: ObservationReplicationsResultsAspect) <- result.replicationsResults)
          CSVConfigurationWriter.write(rr.variableAssignment, configWriter)
      }

      observe(a)
      observe(b)

      // no observation for a at time 0.0
      addValueFor(1, b, (0.0, 12))

      addValueFor(1, a, (1.0, 11.0))
      // no observation for b at time 1.0

      addValueFor(1, a, (2.0, "foo"))
      addValueFor(1, b, (2.0, "bar"))

      addAssignmentForRun(1, 1, List(("x", 1), ("y", 2.0), ("z", "foo")))
      runDone(1)
      replicationsDone(1)
      experimentDone()
    }

    val trajOutput = trajWriter.toString.split("\n").map(_.trim)

    assertEquals("length", 4, trajOutput.length)
    assertEquals("header", "time,a,b", trajOutput(0))
    assertEquals("1st line", "0.0,,12", trajOutput(1))
    assertEquals("2nd line", "1.0,11.0,", trajOutput(2))
    assertEquals("3rd line", "2.0,foo,bar", trajOutput(3))

    val configOutput = configWriter.toString.split("\n").map(_.trim)

    assertEquals("length", 4, configOutput.length)
    assertEquals("header", "var,value", configOutput(0))
    assertEquals("1st line", "x,1", configOutput(1))
    assertEquals("2nd line", "y,2.0", configOutput(2))
    assertEquals("3rd line", "z,foo", configOutput(3))

  }

  @Test def testParameterScanPaths(): Unit = {

    val tmpDirPrefix = "test-results"

    class OutputTestExperiment extends MockExperiment with CSVOutput {
      stopTime = 100
      observeAt(range(0, 10, 100))
      replications = 2

      override def runDurationInMs: Long = 250L

      scan("x" <~ (1, 2))

      val obs1 = observe(t => t)
      val obs2 = observe(t => t)

      csvOutputDirectory(tmpDirPrefix + expStartTime.toString)
      csvConfigDir(id => s"config-$id")
      csvFileName(id => s"run-$id.csv")
    }

    def runTest(experiment: OutputTestExperiment): Unit = {

      execute(experiment)

      val resultDirectory = {
        val dirs = new File(".").listFiles().
                filter(_.isDirectory).
                filter(_.getName.contains(tmpDirPrefix))
        require(dirs.lengthCompare(1) == 0)
        dirs.head
      }

      // number of configurations
      assertEquals(2, resultDirectory.listFiles().length)

      // number of files produced per replication
      val directory = new File(resultDirectory, "config-0")
      assertTrue(directory.exists && directory.isDirectory)
      assertEquals(3, directory.listFiles().length)

      // config file produced
      val configFile = new File(directory, "config.csv")
      assertTrue(configFile.exists())

      // clean up
      Files.deleteFileOrDirectory(resultDirectory)
    }


    val exp1 = new OutputTestExperiment {
      withExperimentResult(writeCSV)
    }
    runTest(exp1)

    val exp2 = new OutputTestExperiment {
      withReplicationsResult(writeCSV)
    }
    runTest(exp2)

    val exp3 = new OutputTestExperiment {
      withRunResult(writeCSV)
    }
    runTest(exp3)

  }

  @Test def testColumnFilter(): Unit = {

    execute {
      new MockExperiment with CSVOutput {

        stopTime = 10
        observeAt(range(0, 1, 10))
        val a = observe(t => t)
        val b = observe(t => t)
        val c = observe(t => t)

        getOrElseUpdate("a", a)
        getOrElseUpdate("b", b)

        csvColumns("a", "b")
        csvOutputDirectory("filtered-results")

        withRunResult(writeCSV)
      }
    }

    val header = Source.fromFile("./filtered-results/config-0/run-0.csv").getLines().next()
    assertEquals("time,a,b", header)

    Files.deleteFileOrDirectory(new File("./filtered-results"))
  }

  @Test def testCustomOutput(): Unit = {

    val trajWriter = new StringWriter()

    execute {
      new MockExperiment with CSVOutput {

        val x = observe(t => t * t * (math.random() - 0.5))
        observeAt(range(0, 10, 100))

        replications = 10

        withReplicationsResult { r =>
          val m = r.trajectories(x).meanWithCI()
          CSVTrajectoryWriter.write(
            Map("mean" -> m.mapValues(_._1),
              "lower" -> m.mapValues(_._2),
              "upper" -> m.mapValues(_._3)),
            trajWriter
          )
        }
      }
    }

    val trajOutput = trajWriter.toString.split("\n").map(_.trim)

    assertEquals("length", 12, trajOutput.length)
    assertEquals("header", "time,lower,mean,upper", trajOutput(0))

  }
}
