/*
 * Copyright 2017 University of Rostock
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package sessl

import org.junit.Assert.assertEquals
import org.junit.Test
import sessl.reference.Experiment

/**
  * @author Oliver Reinhardt
  */
@Test
class TestCentralComposite {
  // central composite with n variables has:
  // 2^n corner points
  // 2*n axial points
  // 1 center point

  @Test
  def testCentralComposite2(): Unit = {
    val exp = new Experiment with CentralCompositeDesign {
      centralComposite("a" <~ interval(1, 2), "b" <~ interval(3, 4))
      assertEquals(9, createVariableSetups().size)
    }
  }

  @Test
  def testCentralComposite4(): Unit = {
    val exp = new Experiment with CentralCompositeDesign {
      centralComposite("a" <~ interval(1, 2), "b" <~ interval(3, 4), "c" <~ interval(5, 6), "d" <~ interval(7, 8))
      assertEquals(25, createVariableSetups().size)
    }
  }
}
