/*
 * Copyright 2018 University of Rostock
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package sessl

import org.junit.Assert._
import org.junit.Test
import sessl.reference.MockExperiment

/**
  * @author Tom Warnke
  */
@Test class CSVInputTest {

  @Test def testCSVInput(): Unit = {

    var assignments = List.empty[VariableAssignment]

    val exp = new MockExperiment with CSVInput {

      designFromCSV(this.getClass.getResource("/design.csv").toURI)

      stopTime = 1
      replications = 1

      withReplicationsResult { result =>
        assignments ::= result.variableAssignment
      }
    }

    execute(exp)

    assertEquals(4, assignments.length)
    assertTrue(assignments.contains(List(("a", 0), ("b", 0), ("c", 0))))
    assertTrue(assignments.contains(List(("a", 1), ("b", 1), ("c", 1))))
    assertTrue(assignments.contains(List(("a", 0), ("b", 1), ("c", 0))))
    assertTrue(assignments.contains(List(("a", 1), ("b", 0), ("c", 1))))
    assertFalse(assignments.contains(List(("a", 1), ("b", 0), ("c", 0))))

  }

}
