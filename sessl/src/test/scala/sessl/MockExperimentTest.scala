/*
 * Copyright 2017 University of Rostock
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package sessl

import org.junit.Assert._
import org.junit.Test
import sessl.reference.MockExperiment

import scala.collection.mutable

/**
  * @author Tom Warnke
  */
@Test class MockExperimentTest {

  @Test def createAndRunExperiment(): Unit = {

    val trajectories = mutable.ArrayBuffer.empty[Trajectory[Double]]

    val exp = new MockExperiment {

      val x = observe(t => t * t)
      observeAt(range(0, 10, 100))

      scan("a" <~ range(1, 1, 5))

      replications = 3

      withRunResult { r =>
        trajectories += r.trajectory(x)
      }
    }

    execute(exp)

    assertEquals(15, trajectories.size)
    trajectories.foreach(t => assertEquals(11, t.length))
  }

  @Test def testCIReplicationCondition(): Unit = {

    var mean = 0.0

    val exp = new MockExperiment {

      override def runDurationInMs: Long = 200

      val x = observe(t => 9.5 + math.random())
      observeAt(0)

      replicationCondition = MeanConfidenceReached(x)

      batchSize = 10

      withReplicationsResult{ r =>
        mean = r.mean(x)
        println(mean)
      }
    }

    execute(exp)

    assertTrue(mean != 0.0)
    assertTrue(mean > 9.5 && mean < 10.5)
  }
}
