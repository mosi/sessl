/*
 * Copyright 2017 University of Rostock
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package sessl

import org.junit.Test
import org.junit.Assert._
import sessl.reference.MockExperiment

/**
  * @author Tom Warnke
  */
@Test class ExpressionObservationTest {

  @Test def testExpressionObservation(): Unit = {

    var trajectory: Option[Trajectory[Double]] = None

    val exp = new MockExperiment with ExpressionObservation {

      val x = observe(t => t * t)
      val y = observe(t => math.sqrt(t))

      val z = observe(Expr(V => (V(x) + V(y)) * 5.0))

      observeAt(range(0, 1, 100))

      withRunResult { result =>
        trajectory = Some(result.trajectory(z))
      }
    }

    execute(exp)

    for ((time: Any, value: Any) <- trajectory.get) {
      val t = time.asInstanceOf[Double]
      val v = value.asInstanceOf[Double]
      assertEquals((t * t + math.sqrt(t)) * 5, v, 0)
    }

  }
}
