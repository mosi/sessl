/*
 * Copyright 2018 University of Rostock
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package sessl

import org.junit.Test
import sessl.reference.MockExperiment

/**
  * @author Tom Warnke
  */
@Test class StopOnErrorTest {

  private class TestException extends Exception

  @Test(expected = classOf[TestException]) def constructorExceptionStop(): Unit = {
    execute {
      new MockExperiment {
        stopOnError = true
        throw new TestException()
      }
    }
  }

  @Test(expected = classOf[TestException]) def constructorExceptionDoNotStop(): Unit = {
    execute {
      new MockExperiment {
        stopOnError = false
        throw new TestException()
      }
    }
  }

  @Test(expected = classOf[TestException]) def constructorExceptionDefault(): Unit = {
    execute {
      new MockExperiment {
        throw new TestException()
      }
    }
  }

  @Test(expected = classOf[TestException]) def eventExceptionStop(): Unit = {
    execute {
      new MockExperiment {
        stopOnError = true
        withRunResult { r =>
          throw new TestException()
        }
      }
    }
  }

  @Test def eventExceptionDoNotStop(): Unit = {
    execute {
      new MockExperiment {
        stopOnError = false
        withRunResult { r =>
          throw new TestException()
        }
      }
    }
  }

  @Test def eventExceptionDefault(): Unit = {
    execute {
      new MockExperiment {
        withRunResult { r =>
          throw new TestException()
        }
      }
    }
  }

}
