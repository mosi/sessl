/*
 * Copyright 2017 University of Rostock
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package sessl

import junit.framework.Assert._
import org.junit.Test
import sessl.LinearRegression.Factor
import sessl.reference.MockExperiment

/**
  * @author Tom Warnke
  */
@Test class TestLinearRegression {

  @Test def testRegressionExecution(): Unit = {

    var output = Option.empty[Array[Double]]

    val exp = new MockExperiment with LinearRegression {

      val y = observe((t, params) => 2.0 * params("b").asInstanceOf[Double] * params("c").asInstanceOf[Double])
      observeAt(0)

      scan("a" <~ (0.0, 3.0), "b" <~ (1.0, 2.0, 3.0), "c" <~ range(0.0, 0.1, 1.0))

      withExperimentResult { result =>
        val regr = fitLinearModel(result)("a", "b" * "c")(y)
        output = Some(regr.beta)
      }
    }

    execute(exp)

    assertTrue(output.isDefined)

    val expected = Array[Double](0.0, 0.0, 2.0)

    // the following can fail due to number precision issues
    // assert(expected.sameElements(output.get))
    // replacement:
    val actual = output.get
    assertEquals(3, actual.length)
    for(i <- 0 to 2)
      assertEquals(expected(i), actual(i), 0.01)
  }

  @Test def testFittedFunctionToString(): Unit = {

    val f1 = new Factor(Seq.empty)
    val f2 = new Factor(Seq("a", "a"))
    val f3 = new Factor(Seq("b"))

    val result = new LinearRegression.RegressionResults(
      Array(f1, f2, f3),
      Array.empty,
      Array.empty,
      Array(1, 3, -5),
      0.0,
      Array.empty,
      0.0,
      Array.empty,
      Array.empty
    )

    assertEquals("+ 1.0 + 3.0 * a * a - 5.0 * b", result.fittedFunction)
  }

}
