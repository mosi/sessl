/*
 * Copyright 2018 University of Rostock
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package sessl.analysis.sensitvity

import org.junit.Assert._
import org.junit.Test
import sessl.analysis.sensitivity._
import sessl._
import sessl.reference.MockExperiment

/**
  * @author Tom Warnke
  */
@Test class SensitivityAnalysisTest {

  @Test def testTwoLevelFullFactorial(): Unit = {

    var xEffect = Option.empty[Double]
    var yEffect = Option.empty[Double]
    var xyEffect = Option.empty[Double]

    analyze((params, objective) => execute(new MockExperiment {
      val obs = observe((t, va) => va("x").asInstanceOf[Double] * va("y").asInstanceOf[Double])
      observeAt(0)
      replications = 1

      for((name, value) <- params)
        set(name <~ value)

      withReplicationsResult(result =>
        objective <~ result.mean(obs)
      )
    })) using new TwoLevelFullFactorialSetup {
      baseCase(
        "x" <~ 1,
        "y" <~ 1
      )
      sensitivityCase(
        "x" <~ 3,
        "y" <~ 4
      )
      withAnalysisResult(result => {
        xEffect = Some(result.individualEffect("x"))
        yEffect = Some(result.individualEffect("y"))
        xyEffect = Some(result.interactionEffect("x", "y"))
      }
      )
    }

    assertEquals(2.0, xEffect.get, 0.0)  // 3 - 1
    assertEquals(3.0, yEffect.get, 0.0)  // 4 - 1
    assertEquals(6.0, xyEffect.get, 0.0) // 12 - 2 - 3 - 1

  }

}
