/*
 * Copyright 2018 University of Rostock
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package sessl.analysis

/**
  * @author Tom Warnke
  */
package object bifurcation {

  type Parameter = Double

  type State = String

  type ObservedValue = Double

  type ParamObsState = (Parameter, ObservedValue, State)

  /**
    * Perform a bifurcation analysis experiment.
    *
    * @example {{{
    *   analyze((setup, objective) =>
    *     execute(new Experiment with Observation {
    *       model = ...
    *       ...
    *
    *       val (param, initialState) = setup
    *       set("p" <~ param)
    *       set("init" <~ initialState)
    *
    *       observeFinalState()
    *       observeSteadyState()
    *
    *       withReplicationsResult { result =>
    *         val means = result[Double](steadyState)
    *         objective <~ ((means.sum / means.size, result(finalState).head.toString))
    *       }
    *     })) using new BifurcationAnalysisSetup {
    *       initialState = ...
    *       initialParameterValue = ...
    *       parameterGenerator = ...
    *       withAnalysisResult(result => println(result.mkString("\n")))
    *     }
    * }}}
    */
  def analyze(f: ObjectiveFunction[ParamObsState, ParamObsState])
    : InitializedObjectiveFunction[ParamObsState, ParamObsState] =
    sessl.analysis.analyze[ParamObsState, ParamObsState](f)

}
