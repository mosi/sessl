/*
 * Copyright 2018 University of Rostock
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package sessl.analysis.bifurcation

import sessl.analysis._

import scala.collection.mutable.ListBuffer

/**
  * A class to perform bifurcation analysis based on a simulation experiment.
  * The method <code>nextParamAndObs</code> can be overridden
  * to inject sophisticated analysis methods.
  *
  * @author Tom Warnke
  */
class BifurcationAnalysisSetup extends AbstractAnalysisSetup[ParamObsState, ParamObsState] {

  private[this] val resultActions =
    ListBuffer.empty[List[(Parameter, ObservedValue)] => Unit]
  private[this] var theInitialState = Option.empty[String]
  private[this] var theInitialParameterValue = Option.empty[Parameter]
  private[this] var theInitialObservedValue = Option.empty[ObservedValue]
  private[this] var theParameterGenerator = Option.empty[ParameterGenerator]

  def initialState_=(name: String): Unit = theInitialState = Some(name)

  def initialState: String = theInitialState.get

  def initialParameterValue_=(name: Parameter): Unit = theInitialParameterValue = Some(name)

  def initialParameterValue: Parameter = theInitialParameterValue.get

  def initialObservedValue_=(name: ObservedValue): Unit = theInitialObservedValue = Some(name)

  def initialObservedValue: ObservedValue = theInitialObservedValue.get

  def parameterGenerator_=(name: ParameterGenerator): Unit =
    theParameterGenerator = Some(name)

  def parameterGenerator: ParameterGenerator = theParameterGenerator.get

  def withAnalysisResult(f: List[(Parameter, ObservedValue)] => Unit): Unit =
    resultActions += f

  trait ParameterGenerator {
    def nextParamAndObs(param: Parameter, obs: ObservedValue): Option[(Parameter, ObservedValue)]
  }

  def execute(f: ObjectiveFunction[ParamObsState, ParamObsState]): Unit = {
    require(theParameterGenerator.isDefined,
      "Set the parameter generator with parameterGenerator = ...")
    require(theInitialState.isDefined,
      "Set the initial state with initialState = ...")
    require(theInitialParameterValue.isDefined,
      "Set the initial parameter value with initialParameterValue = ...")
    require(theInitialObservedValue.isDefined,
      "Set the observed value with initialObservedValue = ...")

    def target(setup: ParamObsState): ParamObsState =  evaluate(f)(setup)

    def nextPoint(previous: Option[ParamObsState]): Option[ParamObsState] =
      for {
        (prevParam, prevObs, prevState) <- previous
        (nextParam, nextObs) <- nextParamAndObs(prevParam, prevObs)
      } yield {
        target(nextParam, nextObs, prevState)
      }

    val start = target(initialParameterValue, initialObservedValue, initialState)

    lazy val pointStream: Stream[Option[ParamObsState]] =
      Stream.cons(Some(start), pointStream.map(nextPoint))

    val result = pointStream.takeWhile(_.isDefined).collect {
      case Some((param, obs, state)) => (param, obs)
    }.toList

    for (a <- resultActions)
      a(result)
  }

  def nextParamAndObs(param: Parameter, obs: ObservedValue): Option[(Parameter, ObservedValue)] = {
    theParameterGenerator.get.nextParamAndObs(param, obs)
  }

}
