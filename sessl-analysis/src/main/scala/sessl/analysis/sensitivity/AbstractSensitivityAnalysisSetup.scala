/*
 * Copyright 2018 University of Rostock
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package sessl.analysis.sensitivity

import sessl._
import sessl.analysis.AbstractAnalysisSetup

import scala.collection.mutable

/**
  * @author Tom Warnke
  */
abstract class AbstractSensitivityAnalysisSetup extends AbstractAnalysisSetup[Parameters, Double] {

  import java.{lang, util}

  import scala.collection.JavaConverters._

  protected def convert(scalaMap: Map[String, Double]): util.Map[String, lang.Double] =
    scalaMap.mapValues(Double.box).asJava

  protected def convert(javaMap: util.Map[String, lang.Double]): Map[String, Double] =
    javaMap.asScala.toMap.mapValues(_.doubleValue())

}

abstract class AbstractLocalSensitivityAnalysisSetup
    extends AbstractSensitivityAnalysisSetup {

  private[this] val baseCase = mutable.ListBuffer.empty[(String, Double)]

  private[this] val sensitivityCase = mutable.ListBuffer.empty[(String, Double)]

  def baseCase(vars: Variable[Double]*): Unit = {
    vars.foreach {
      case VarSingleVal(name, value) =>
        baseCase += ((name, value))
      case _ => throw new IllegalArgumentException()
      // TODO error message
    }
  }

  def sensitivityCase(vars: Variable[Double]*): Unit = {
    vars.foreach {
      case VarSingleVal(name, value) =>
        sensitivityCase += ((name, value))
      case _ => throw new IllegalArgumentException()
      // TODO error message
    }
  }

  protected def getParams: Seq[String] = baseCase.map(_._1)
  protected def getBaseCase: Map[String, Double] = baseCase.toMap
  protected def getSensitivityCase: Map[String, Double] = sensitivityCase.toMap

}
