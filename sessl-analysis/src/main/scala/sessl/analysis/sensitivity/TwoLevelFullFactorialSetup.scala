/*
 * Copyright 2018 University of Rostock
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package sessl.analysis.sensitivity

import java.{lang, util}

import org.jamesii.samo.IModel
import org.jamesii.samo.local.OneAtATimeSAMO
import sessl.analysis.Objective

import scala.collection.JavaConverters._
import scala.collection.mutable.ListBuffer

/**
  * @author Tom Warnke
  */
class TwoLevelFullFactorialSetup extends AbstractLocalSensitivityAnalysisSetup {

  var resultActions: ListBuffer[Result => Unit] = ListBuffer.empty

  def withAnalysisResult(f: Result => Unit): Unit = resultActions += f

  override def execute(f: (Parameters, Objective[Double]) => Unit): Unit = {

    val model = new IModel {
      override def getParameters: util.Set[String] = getParams.toSet.asJava

      override def value(map: util.Map[String, lang.Double]): Double = {
        val o = new Objective[Double]
        f(convert(map), o)
        o.getValue
      }
    }

    val samo = new OneAtATimeSAMO(model,
      convert(getBaseCase),
      convert(getSensitivityCase))

    val result = new Result(samo)

    for (a <- resultActions)
      a(result)
  }

  class Result(val samo: OneAtATimeSAMO) {

    private def checkParam(param: String): Unit =
      require(getParams.contains(param),
        s"Cannot compute the effect of $param, as it is not one of the tested parameters")

    def individualEffect(param: String): Double = {
      checkParam(param)
      samo.individualEffect(param)
    }
    def interactionEffect(params: String*): Double = {
      for (param <- params) checkParam(param)
      samo.interactionEffect(params.toSet.asJava)
    }
    def totalEffect(param: String): Double = {
      checkParam(param)
      samo.totalEffect(param)
    }
    def totalInteractionEffect(param: String): Double = {
      checkParam(param)
      samo.totalInteractionEffect(param)
    }
    def printCompleteReport(): Unit = {
      val params = getParams
      for (param <- params)
        print(
          s"""
            |parameter $param:
            |  - individual effect:         ${individualEffect(param)}
            |  - total effect:              ${totalEffect(param)}
            |  - total interaction effect:  ${totalInteractionEffect(param)}
          """.stripMargin)
      for (num <- 2 to params.length; subset <- params combinations num)
        print(
          s"""
            |interaction effect between ${subset.mkString(", ")}: ${interactionEffect(subset: _*)}
           """.stripMargin
        )
    }
  }

}
