/*
 * Copyright 2018 University of Rostock
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package sessl.analysis

/**
  * A class to collect results from an experiment to be used in further analysis.
  * Can be called in the <code>withRunResults</code> etc.
  *
  * @author Tom Warnke
  */
class Objective[T] {

  private[this] var value: Option[T] = None

  def <~(newValue: T): Unit = value = Some(newValue)

  def getValue: T = value.get

}