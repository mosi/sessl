SESSL 0.21

- a new binding with a simple simulator for an (s,S) inventory model
- minor changes and a bug fix

SESSL 0.20

- update various dependencies
- the default format for result directories works on Windows now
- the behavior when encountering errors in event handlers can now be configured

SESSL 0.19

- update to ML3 0.2.0
- revised the handling of concurrent simulation runs
- improved logging

SESSL 0.18.1

- fixed some bugs in the concurrent execution of simulation runs

SESSL 0.18

- the amount of console output that is produced when an error occurs can be controlled via the log level now
- added some operations on trajectories and collections of trajectories, for example to obtain a mean trajectory
- some minor updates to the bifurcation analysis package
- adapt to some changes in the ML-Rules package
    - the attribute observation can now also filter the entities to consider
    - the integrator of the hybrid simulator can be configured
- some refactoring of the initial state creation in the ML3 binding
- some bug fixes, particularly concerning concurrency
- the MockExperiment now executes the individual runs in parallel, which helps to test experiment traits
- the CSV output trait now uses the ISO-8601 time format with millisecond resolution

SESSL 0.17

- added the possibility to bind observations to scala vals instead of string - this adds some type safety
- when observing several attributes of an entity type, all observations of an individual entity are written to an individual line in the CSV output file
- some syntax for configuring the creation of the initial model state in the ML3 binding
- exceptions that are thrown while handling events (e.g., in withRunResult) will no longer make the entire experiment fail
- some internal refactoring and cleanup

SESSL 0.16

- added a binding to the pSSALib SBML simulator
- ported all ScalaTest tests to JUnit
- added a feature to import an experiment design from a CSV file
- some internal refactoring

SESSL 0.15

- CSV output columns can be configured
- CSV files can be written after each run, replication, or after the entire experiment
- added a trait to measure the time taken by an entire experiment
- added a way to enumerate model parameter configurations instead of listing scan commands
- added a analysis binding that included sensitivity and a preview of bifurcation analysis
- reworked the syntax for observations in the ML-Rules binding
- some minor fixes and cleanup

SESSL 0.14

- added a mock experiment to reproducibly test experimental methods that work on simulation results
- added a verification binding and a statistical model checking trait
- added the possibility to observe expressions
- some refactoring/optimizations in the observation code in the ML3 binding
- re-added the submodule for integration tests with experiments that use several bindings

SESSL 0.13

- updated to Scala 2.12
- added traits for linear regression meta-modeling and central composite experiment design
- Latin Hypercube can now be applied repeatedly during one experiment
- removed the various CSV writers and replaced them with a new generalized implementation
- the replication condition using confidence interval widths is now based on the _relative_
half-width of the interval instead of an absolute value
- code cleanup and refactoring

SESSL 0.12

- added ML3 binding
- readded SSJ binding
- some changes to the ML-Rules binding:
  - now uses the standard simulator as default if no simulator is set
  - less log messages
  - added tau-leaping simulator from latest ML-Rules release
- improved the CSV file output
- fixed a bug where Double parameters from ranges were inaccurate after repeated addition by using BigDecimal instead
- some minor changes and bug fixes
- started to write a changelog
