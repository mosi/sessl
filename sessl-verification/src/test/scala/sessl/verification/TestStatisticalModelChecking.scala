/*
 * Copyright 2017 University of Rostock
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package sessl.verification

import org.apache.commons.math3.random.{GaussianRandomGenerator, JDKRandomGenerator}
import org.junit.Assert._
import org.junit.Test
import sessl._
import sessl.reference.MockExperiment
import sessl.verification.mitl._

/**
  * @author Tom Warnke
  */
@Test class TestStatisticalModelChecking {

  @Test def testSingleSamplingPlan(): Unit = {

    var numRuns: Option[Int] = None
    var checkingResult: Option[Boolean] = None

    val exp = new MockExperiment with StatisticalModelChecking {

      observeAt(range(0, 1, 100))
      val x = observe(_ => 0.0)

      test = SingleSamplingPlan(
        p = 0.8,
        alpha = 0.05,
        beta = 0.05,
        delta = 0.05)

      prop = MITL(OutVar(x) === Constant(0))

      withReplicationsResult{ result =>
        numRuns = Some(result(x).size)
      }

      withCheckResult { result =>
        checkingResult = Some(result.satisfied)
      }

    }

    execute(exp)

    assertEquals(180, numRuns.get)
    assertEquals(true, checkingResult.get)
  }

  @Test def testSequentialProbabilityRatioTest(): Unit = {

    var numRuns: Option[Int] = None
    var checkingResult: Option[Boolean] = None

    val exp = new MockExperiment with StatisticalModelChecking {

      observeAt(range(0, 1, 100))
      val x = observe(_ => 0.0)

      test = SequentialProbabilityRatioTest(
        p = 0.8,
        alpha = 0.05,
        beta = 0.05,
        delta = 0.05)

      prop = MITL(OutVar(x) === Constant(0))

      withReplicationsResult{ result =>
        numRuns = Some(result(x).size)
      }

      withCheckResult { result =>
        checkingResult = Some(result.satisfied)
      }

    }

    execute(exp)

    assertEquals(true, checkingResult.get)
  }

}

object TestStatisticalModelChecking {

  private val noiseGenerator = new GaussianRandomGenerator(new JDKRandomGenerator())

  def gaussianNoise: Double = noiseGenerator.nextNormalizedDouble()
}
