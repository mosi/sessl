/*
 * Copyright 2017 University of Rostock
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package sessl.verification.mitl

import org.junit.Assert._
import org.junit.Test
import sessl.AbstractObservation.Observable
import sessl.Trajectory

/**
  * @author Tom Warnke
  */
@Test class MITLFormulaTest {

  import MITLFormulaTest._

  def check(formula: MITLFormula[Double]): Boolean = MITL(formula).check(Map(), testData)

  @Test def GAEqualsZero(): Unit =
    assertEquals(true, check(G(0.0, 3.0)(OutVar(obs) === Constant(0.0))))

  @Test def GAEqualsOne(): Unit =
    assertEquals(false, check(G(0.0, 3.0)(OutVar(obs) === Constant(1.0))))

  @Test def GALesserThanOne(): Unit = {
    assertEquals(true, check(G(0.0, 3.0)(OutVar(obs) < Constant(1.0))))
    assertEquals(true, check(G(0.0, 3.0)(OutVar(obs) <= Constant(1.0))))
  }

  @Test def GTrue(): Unit = {
    assertEquals(true, check(G(0.0, 0.0)(True())))
    assertEquals(true, check(G(0.0, 3.0)(True())))
    assertEquals(true, check(G(0.0, 4.0)(True())))
    assertEquals(false, check(G(0.0, 5.0)(True())))
    assertEquals(false, check(G(0.0, 100000.0)(True())))
  }

  @Test def GACompareTrajectories(): Unit = {
    assertEquals(true, check(G(0.0, 3.0)(Traj(as) < Traj(bs))))
    assertEquals(false, check(G(0.0, 3.0)(Traj(as) > Traj(bs))))
    assertEquals(false, check(G(0.0, 3.0)(Traj(as) === Traj(bs))))
    assertEquals(true, check(G(0.0, 3.0)(Traj(bs) === Traj(bs))))
    assertEquals(true, check(G(0.0, 3.0)(OutVar(obs) === Traj(as))))
  }

  @Test def GACalculate(): Unit = {
    assertEquals(true, check(G(0.0, 3.0)(Constant(3.0) + Constant(4.0) === Constant(7.0))))
    assertEquals(true, check(G(0.0, 3.0)(Constant(3.0) / Constant(4.0) < Constant(1.0))))
    assertEquals(true, check(G(0.0, 3.0)((Constant(3.0) - Constant(4.0)).abs >= Constant(1.0))))
  }

  // TODO tests for all operators

}

object MITLFormulaTest {
  val obs = new Observable[Double] {}

  val as = List[(Double, Double)]((0.0, 0.0), (1.0, 0.0), (3.0, 0.0), (4.0, 0.0), (5.0, 0.0))
  val bs = List[(Double, Double)]((0.0, 1.0), (1.0, 2.0), (3.0, 3.0), (4.0, 4.0), (5.0, 5.0))

  val testData = Map(obs.asInstanceOf[Observable[Any]] -> as.asInstanceOf[Trajectory[Any]])
}
