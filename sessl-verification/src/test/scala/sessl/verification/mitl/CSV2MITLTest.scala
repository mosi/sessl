package sessl.verification.mitl

import org.junit.Assert.assertEquals
import org.junit.Test
import sessl.AbstractObservation.Observable
import sessl.Trajectory

@Test class CSV2MITLTest {

  def check[A: Numeric](formula: MITLFormula[A], trajectories: Map[Observable[_], Trajectory[_]]): Boolean = {
    MITL(formula).check(Map.empty, trajectories)
  }

  @Test def checkFile(): Unit = {
    val file = this.getClass.getResource("/trajectory.csv").toURI
    val ts = Trajectory.fromCSV(file)
    val a = new Observable[Double] {}
    val b = new Observable[Double] {}
    val map = Map(a -> ts("a"), b -> ts("b")).asInstanceOf[Map[Observable[_], Trajectory[_]]]

    assertEquals(true, check(G(0.0, 2.0)(OutVar(a) > Constant(0.0)), map))
    assertEquals(false, check(G(0.0, 4.0)(OutVar(a) > Constant(0.0)), map))
    assertEquals(true, check(G(0.0, 4.0)(OutVar(a) >= Constant(0.0)), map))
    assertEquals(false, check(G(0.0, 7.0)(OutVar(a) >= Constant(0.0)), map))
    assertEquals(true, check(F(0.0, 7.0)(OutVar(a) > OutVar(b)), map))
  }

}
