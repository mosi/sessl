/*
 * Copyright 2017 University of Rostock
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package sessl.verification

import org.junit.Assert._
import org.junit.Test
import sessl.AbstractObservation.Observable
import sessl._
import sessl.reference.MockExperiment

/**
  * @author Tom Warnke
  */
@Test class VerificationTest {

  @Test def testSimpleVerification(): Unit = {

    def alwaysGreater(obs: Observable[_], limit: Double): ObservationRunResultsAspect => Boolean =
      r => r.values(obs).asInstanceOf[List[Double]].forall {
        _ > limit
      }

    val successExp = new MockExperiment with Verification {

      val x = observe(_ => 5.0)
      observeAt(range(0, 1, 100))

      verifyEachRun(alwaysGreater(x, 4), "x is not always greater than 4!")
    }

    val failureExp = new MockExperiment with Verification {

      val x = observe(_ => 5.0)
      observeAt(range(0, 1, 100))

      verifyEachRun(alwaysGreater(x, 6), "x is not always greater than 4!")
    }

    execute(successExp)
    execute(failureExp)

    assertEquals(true,
      successExp.verificationResult.forall { case VerificationResult(_, _, result) => result
      })

    assertEquals(true,
      failureExp.verificationResult.forall { case VerificationResult(_, _, result) => !result
      })

  }

}
