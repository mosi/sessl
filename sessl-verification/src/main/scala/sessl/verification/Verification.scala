/*
 * Copyright 2016 University of Rostock
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package sessl.verification

import sessl._

import scala.collection.mutable.ListBuffer

case class VerificationResult(source: String, message: String, result: Boolean) {
  override def toString: String = s"VerificationResult($source, $message, $result)"
}

trait Verification extends AbstractObservation {
  this: AbstractExperiment =>

  private[this] val verificationResults = ListBuffer.empty[VerificationResult]

  final def verificationResult: List[VerificationResult] = verificationResults.toList

  def verifyEachRun(verificationFunction: ObservationRunResultsAspect => Boolean,
                    msg: String): Unit = {
    withRunResult(r => {
      val result = verificationFunction.apply(r)
      verificationResults += VerificationResult("Run", msg, result)
    })
  }

  def verifyAfterReplications(verificationFunction: ObservationReplicationsResultsAspect => Boolean,
                              msg: String): Unit = {
    withReplicationsResult(r => {
      val result = verificationFunction.apply(r)
      verificationResults += VerificationResult("Replication", msg, result)
    })
  }

  def verifyAfterExperiment(verificationFunction: ObservationExperimentResultsAspect => Boolean,
                            msg: String): Unit = {
    withExperimentResult(r => {
      val result = verificationFunction.apply(r)
      verificationResults += VerificationResult("Experiment", msg, result)
    })
  }
}
