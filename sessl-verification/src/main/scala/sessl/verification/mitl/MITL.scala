/*
 * Copyright 2017 University of Rostock
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package sessl.verification.mitl

import java.util

import sessl.AbstractObservation.Observable
import sessl.Trajectory
import sessl.verification._

/**
  * A Metric Interval Temporal Logic (MITL) Property.
  * For more information about the checking algorithm, see:
  * Oded Maler and Dejan Nickovic: Monitoring Temporal Properties of Continuous Signals
  *
  * @author Tom Warnke
  */
case class MITL[T : Numeric](formula: MITLFormula[T]) extends PropertySpecification {

  override def check(inputs: Map[String, Any],
                     outputs: Map[Observable[_], Trajectory[_]]): Boolean = {

    val timePoints: List[Double] =
      (for {
        traj <- outputs.values
        (t, _) <- traj
      } yield t).toList.distinct.sorted

    val T = implicitly[Numeric[T]]

    def evaluate(value: Value[T], time: Double): Double = value match {
      case Constant(v) => T.toDouble(v)
      case OutVar(obs) =>
        val observed = outputs(obs.asInstanceOf[Observable[_]]).toMap[Double, Any].apply(time)
        observed match {
          case d: Double => d
          case i: Int => i.toDouble
          case _ =>
            throw new IllegalArgumentException("Can not evaluate non-numerical observations")
        }
      case InVar(name) => inputs(name).asInstanceOf[Double]
      case Traj(data) =>
        val values = data.toMap
        if (values.contains(time))
          T.toDouble(values(time))
        else
          throw new IllegalArgumentException(s"The trajectory $data has no value for the time $time")
      case Calculation(left, right, op) => op(evaluate(left, time), evaluate(right, time))
    }

    def validIntervals(f: MITLFormula[T]): IntervalSet = f match {

      case Comparison(left, right, op) =>
        val result = new IntervalSet(timePoints)
        val iterator = timePoints.iterator
        while (iterator.hasNext) {
          val interval = iterator
                  .dropWhile(t => !op(evaluate(left, t), evaluate(right, t)))
                  .takeWhile(t => op(evaluate(left, t), evaluate(right, t)))
                  .toList
          if (interval.nonEmpty)
            result.add(interval.head, interval.last)
        }
        result
      case Conjunction(left, right) =>
        val leftIntervals = validIntervals(left)
        val rightIntervals = validIntervals(right)
        val leftSplitIntervals = leftIntervals.splitIntervals(rightIntervals.getIntervalBorders)
        val rightSplitIntervals = rightIntervals.splitIntervals(leftIntervals.getIntervalBorders)
        leftSplitIntervals.retainAll(rightSplitIntervals).mergeIntervals
      case Disjunction(left, right) =>
        val leftIntervals = validIntervals(left)
        val rightIntervals = validIntervals(right)
        val leftSplitIntervals = leftIntervals.splitIntervals(rightIntervals.getIntervalBorders)
        val rightSplitIntervals = rightIntervals.splitIntervals(leftIntervals.getIntervalBorders)
        leftSplitIntervals.addAll(rightSplitIntervals).mergeIntervals
      case Negation(inner) => validIntervals(inner).invert
      case Globally(from, to, inner) =>
        validIntervals(inner).shiftIntervals(-from, -to).mergeIntervals
      case Eventually(from, to, inner) =>
        validIntervals(inner).shiftIntervals(-to, -from).mergeIntervals
      case Until(from, to, left, right) =>
        val l = validIntervals(left)
        val r = validIntervals(right)
        validIntervals(left).unitaryUntil(validIntervals(right), from, to)
      case True() => IntervalSet.wholeTimeRange(timePoints)
    }

    validIntervals(formula).includesZero

  }

  class IntervalSet(timePoints: List[Double]) {

    private val intervals = new util.TreeSet[Interval]

    def add(start: Double, end: Double): Unit = {
      this.add(Interval(start, end, timePoints))
    }

    def add(interval: Interval): Unit = {
      if (interval.from <= interval.to) intervals.add(interval)
    }

    def includesZero: Boolean = !intervals.isEmpty && intervals.first.from == 0.0

    def getIntervalBorders: util.TreeSet[Double] = {
      val result = new util.TreeSet[Double]
      intervals.forEach { interval =>
        result.add(interval.from)
        result.add(interval.to)
      }
      result
    }

    def splitIntervals(splitPoints: util.TreeSet[Double]): IntervalSet = {
      val result = new IntervalSet(timePoints)
      intervals.forEach { interval =>
        val currentSplitPoints = splitPoints.subSet(interval.from, false, interval.to, false)
        if (currentSplitPoints.isEmpty) result.add(interval)
        else {
          var start = interval.from
          currentSplitPoints.forEach { current =>
            result.add(start, current)
            start = current
          }
          result.add(start, interval.to)
        }
      }
      result
    }

    def mergeIntervals: IntervalSet = {
      val result = new IntervalSet(timePoints)
      if (!this.intervals.isEmpty) {
        val iterator = this.intervals.iterator
        var start: Double = -1
        var end: Double = -1
        intervals.forEach { interval =>
          if (start == -1 && end == -1) { // new interval
            start = interval.from
            end = interval.to
          }
          else if (interval.from <= end) { // merge connected or overlapping intervals
            end = Math.max(end, interval.to)
          }
          else { // found discontinuity
            result.add(start, end)
            start = interval.from
            end = interval.to
          }
        }
        result.add(start, end)
      }
      result
    }

    def retainAll(other: IntervalSet): IntervalSet = {
      val result = new IntervalSet(timePoints)
      result.intervals.addAll(this.intervals)
      result.intervals.retainAll(other.intervals)
      result
    }

    def addAll(other: IntervalSet): IntervalSet = {
      val result = new IntervalSet(timePoints)
      result.intervals.addAll(this.intervals)
      result.intervals.addAll(other.intervals)
      result
    }

    def invert: IntervalSet = {
      val result = IntervalSet.wholeTimeRange(timePoints)
      result.splitIntervals(this.getIntervalBorders)
      result.intervals.removeAll(this.intervals)
      result
    }

    def shiftIntervals(startShift: Double, endShift: Double): IntervalSet = {
      val result = new IntervalSet(timePoints)
      intervals.forEach { interval =>
        val tmp = interval.shift(startShift, endShift)
        if (tmp.from < tmp.to) result.add(tmp)
      }
      result
    }

    def unitaryUntil(other: IntervalSet, from: Double, to: Double): IntervalSet = {
      val result = new IntervalSet(timePoints)
      intervals.forEach { leftInterval =>
        other.intervals.forEach { rightInterval =>
          if (leftInterval.overlaps(rightInterval)) { // intervals overlap
            val intersection = leftInterval.intersection(rightInterval)
            val shiftedIntersection = intersection.shift(-to, -from)
            if (shiftedIntersection.length > 0 && shiftedIntersection.overlaps(leftInterval))
              result.add(shiftedIntersection.intersection(leftInterval))
          }
        }
      }
      result
    }

    override def toString: String = {
      var result = ""
      intervals.forEach { interval =>
        result += interval + "\n"
      }
      result
    }

  }

  object IntervalSet {
    def wholeTimeRange(timePoints: List[Double]): IntervalSet = {
      val result = new IntervalSet(timePoints)
      result.add(timePoints.head, timePoints.last)
      result
    }
  }

  case class Interval(from: Double, to: Double, timePoints: List[Double]) extends Comparable[Interval] {

    def intersection(other: Interval): Interval = {
      if (this.from > other.to || this.to < other.from) throw new IllegalArgumentException
      Interval(Math.max(this.from, other.from), Math.min(this.to, other.to), timePoints)
    }

    def overlaps(other: Interval): Boolean =
      (this.from <= other.to && this.from >= other.from) ||
              (this.to <= other.to && this.to >= other.from)

    def shift(startShift: Double, endShift: Double): Interval = {
      val newStart = Math.min(Math.max(from + startShift, timePoints.head), timePoints.last)
      val newEnd = Math.min(Math.max(to + endShift, timePoints.head), timePoints.last)
      Interval(newStart, newEnd, timePoints)
    }

    override def compareTo(other: Interval): Int = {
      if (this.from > other.from) 1
      else if (this.from < other.from) -1
      else { // this.start == o.start
        if (this.to > other.to) 1
        else if (this.to < other.to) -1
        else 0
      }
    }

    def length: Double = to - from

    override def toString: String = s"[$from, $to]"

  }

}



