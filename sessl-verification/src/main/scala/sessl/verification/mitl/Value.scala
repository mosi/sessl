/*
 * Copyright 2017 University of Rostock
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package sessl.verification.mitl

import sessl.AbstractObservation.Observable
import sessl.Trajectory

/**
 * @author Tom Warnke
 */
sealed abstract class Value[T: Numeric] {
  def <(other: Value[T]): MITLFormula[T] = Comparison(this, other, _ < _)

  def <=(other: Value[T]): MITLFormula[T] = Comparison(this, other, _ <= _)

  def ===(other: Value[T]): MITLFormula[T] = Comparison(this, other, _ == _)

  def >=(other: Value[T]): MITLFormula[T] = Comparison(this, other, _ >= _)

  def >(other: Value[T]): MITLFormula[T] = Comparison(this, other, _ > _)

  def +(other: Value[T]): Value[T] = Calculation(this, other, _ + _)

  def -(other: Value[T]): Value[T] = Calculation(this, other, _ - _)

  def *(other: Value[T]): Value[T] = Calculation(this, other, _ * _)

  def /(other: Value[T]): Value[T] = Calculation(this, other, _ / _)

  def abs: Value[T] = Calculation(this, this, (v, _) => v.abs)
}

case class Constant[T: Numeric](value: T) extends Value[T]

case class OutVar[T: Numeric](obs: Observable[T]) extends Value[T]

case class InVar[T: Numeric](name: String) extends Value[T]

case class Traj[T: Numeric](data: Trajectory[T]) extends Value[T]

case class Calculation[T: Numeric](left: Value[T], right: Value[T], op: (Double, Double) => Double) extends Value[T]
