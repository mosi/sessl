/*
 * Copyright 2017 University of Rostock
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package sessl.verification.mitl

/**
  * @author Tom Warnke
  */
sealed abstract class MITLFormula[T : Numeric] {

  def and(other: MITLFormula[T]): MITLFormula[T] = Conjunction[T](this, other)

  def or(other: MITLFormula[T]): MITLFormula[T] = Disjunction[T](this, other)

  def unary_!(): MITLFormula[T] = Negation[T](this)

  def U(from: Double, to: Double)(other: MITLFormula[T]): MITLFormula[T] = Until[T](from, to, this, other)

}

case class Comparison[T : Numeric](left: Value[T],
                      right: Value[T],
                      op: (Double, Double) => Boolean) extends MITLFormula[T]

case class Conjunction[T : Numeric](left: MITLFormula[T],
                       right: MITLFormula[T]) extends MITLFormula[T]

case class Disjunction[T : Numeric](left: MITLFormula[T],
                       right: MITLFormula[T]) extends MITLFormula[T]

case class Negation[T : Numeric](formula: MITLFormula[T]) extends MITLFormula[T]

case class Globally[T : Numeric](from: Double,
                    to: Double,
                    inner: MITLFormula[T]) extends MITLFormula[T]

case class Eventually[T : Numeric](from: Double,
                      to: Double,
                      inner: MITLFormula[T]) extends MITLFormula[T]

case class Until[T : Numeric](from: Double,
                 to: Double,
                 left: MITLFormula[T],
                 right: MITLFormula[T]) extends MITLFormula[T]

case class True[T : Numeric]() extends MITLFormula[T]

// some helper objects
object G {
  def apply[T : Numeric](from: Double, to: Double)
           (inner: MITLFormula[T]) = Globally[T](from, to, inner)
}

object F {
  def apply[T : Numeric](from: Double, to: Double)
           (inner: MITLFormula[T]) = Eventually[T](from, to, inner)
}