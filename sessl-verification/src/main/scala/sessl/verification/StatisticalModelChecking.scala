/*
 * Copyright 2017 University of Rostock
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package sessl.verification

import org.apache.commons.math3.distribution.BinomialDistribution
import sessl._

import scala.collection.mutable

/**
  * A trait for simulation-based verification with statistical model-checking.
  * A property to check on trajectories must be defined.
  * Currently, only MITL properties are supported.
  * Experiments with this trait will automatically execute a sufficient number of replications to
  * decide whether a random simulation run satisfies the given property.
  * How this satisfaction is defined depends on the statistical test method that can be configured.
  *
  * @example
  * {{{
  * import sessl._
  * import sessl.mlrules._ // or sessl.ml3._
  * import sessl.verification._
  * import sessl.verification.mitl._
  *
  * execute {
  *   new Experiment with Observation with StatisticalModelChecking {
  *     // ...
  *
  *     observe("x")
  *
  *     prop = MITL(G(0, 50)(OutVar("x") == Constant(0)))
  *
  *     withCheckResult { result =>
  *       println(s"Satisfaction status: ${result.satisfied}")
  *     }
  *   }
  * }
  * }}}
  *
  * @author Tom Warnke
  */
trait StatisticalModelChecking extends ExperimentConfiguration with DynamicSimulationRuns {
  this: AbstractExperiment with AbstractObservation =>

  /** maps assignment index to checking result for that assignment */
  private[this] val assignmentResult = mutable.Map.empty[VariableAssignment, CheckResult]

  /** maps run id to check result for that run */
  private[this] val runCheckResult = mutable.Map.empty[Int, Boolean]

  /** the property to check */
  private[this] var property: Option[PropertySpecification] = None

  /** the statistical test method */
  private[this] var testProcedure: TestProcedure =
    SingleSamplingPlan(
      p = 0.8,
      alpha = 0.05,
      beta = 0.05,
      delta = 0.05)

  def prop_=(p: PropertySpecification): Unit = property = Some(p)

  def prop: PropertySpecification = property.get

  def test_=(t: TestProcedure): Unit = testProcedure = t

  def test: TestProcedure = testProcedure


  /** Setting the flags that control a proper call hierarchy, calling event handlers if installed
    * . */
  override def configure(): Unit = {
    super.configure()
    require(property.isDefined,
      "Please use 'prop = ...' to define a property for Statistical Model checking.")
  }

  override protected def minReplicationNumber(replicationCondition: ReplicationCondition,
                                              finishedRuns: List[RunResults]): Int =
    math.max(testProcedure.minReplications, super.minReplicationNumber(replicationCondition, finishedRuns))

  override protected def enoughReplications(replicationCondition: ReplicationCondition,
                                            finishedRuns: List[RunResults]): Boolean = {

    finishedRuns.headOption.exists { run =>

      val assignment = run.assignment

      // this method caches results in the map runCheckResult to avoid recomputation
      def satisfiesProperty(runResult: ObservationRunResultsAspect): Boolean =
        runCheckResult
                .getOrElseUpdate(runResult.runId, prop.check(assignment.toMap, runResult.all.toMap))

      val runResults =
        DynamicSimulationRuns.runResultsToObservationRunResultsAspects(finishedRuns)

      // count how many runs satisfy the property
      val numSatisfied = runResults.count(satisfiesProperty)

      val enough = testProcedure.enoughReplications(numSatisfied, runResults.size)

      // if done, save result
      if (enough) assignmentResult(run.assignment) =
              CheckResult(testProcedure.isSatisfied(numSatisfied, runResults.size))

      // check if any other replication conditions need to be satisfied additionally to ours
      enough && super.enoughReplications(replicationCondition, finishedRuns)
    }
  }

  def withCheckResult(f: CheckResult => Unit): Unit = {
    withReplicationsResult(r => {
      val assignment = r.variableAssignment
      val checkResult = assignmentResult(assignment)
      f(checkResult)
    })
  }

  sealed trait TestProcedure {
    def p: Double

    def minReplications: Int

    def enoughReplications(satisfied: Int, total: Int): Boolean

    def isSatisfied(satisfied: Int, total: Int): Boolean = satisfied >= total * p
  }

  case class SingleSamplingPlan(p: Double,
                                alpha: Double,
                                beta: Double,
                                delta: Double) extends TestProcedure {

    override lazy val minReplications: Int = {
      // see Sen et.al: On Statistical Model Checking of Stochastic Systems, section 3.1
      //
      // assume that the number of satisfying runs Y is binomially distributed
      // compute an n for which the following two inequalities hold:
      // Prob[Y/n >= p] = Prob[Y >= p * n] = 1 - Prob[Y < p * n] <= alpha when Y ~ B(n, p - delta)
      // Prob[Y/n < p] = Prob[Y < p * n]<= beta when Y ~ B(n, p + delta)
      // start with some small n and increase it until both inequalities hold

      var n = 1
      var done = false

      do {
        n += 1
        val probGreater = 1 - new BinomialDistribution(n, math.max(p - delta, 0)).
                cumulativeProbability((p * n).toInt)
        val probLesser = new BinomialDistribution(n, math.min(p + delta, 1)).
                cumulativeProbability((p * n).toInt)
        done = probGreater <= alpha && probLesser <= beta
      } while (!done)

      n
    }

    override def enoughReplications(satisfied: Int, total: Int): Boolean = total >= minReplications
  }

  case class SequentialProbabilityRatioTest(p: Double,
                                            alpha: Double,
                                            beta: Double,
                                            delta: Double) extends TestProcedure {

    // see Legay et. al: Statistical Model Checking: An Overview, section 3.1, page 5
    //
    // the method is based on a quotient p1m/p0m that is computed after a certain number of runs
    // have been executed and the property has been checked on the results
    // if p1m/p0m >= a, the property is satisfied
    // if p1m/p0m <= b, the property is not satisfied
    // else, more runs are needed to come to a conclusion
    //
    // the algorithm is guaranteed to terminate

    val a = (1 - beta) / alpha
    val b = beta / (1 - alpha)
    val p0 = p + delta
    val p1 = p - delta

    def quotient(satisfied: Int, total: Int): Double = {
      val p1m = math.pow(p1, satisfied) * math.pow(1 - p1, total - satisfied)
      val p0m = math.pow(p0, satisfied) * math.pow(1 - p0, total - satisfied)
      p1m / p0m
    }

    override def minReplications: Int = batchSize

    override def enoughReplications(satisfied: Int, total: Int): Boolean = {
      val q = quotient(satisfied, total)

      q >= a || q <= b
    }

    override def isSatisfied(satisfied: Int, total: Int): Boolean = {
      val q = quotient(satisfied, total)

      if (q >= a) false
      else if (q <= b) true
      else throw new UnsupportedOperationException(
        "Could not determine whether the property is satisfied")
    }
  }
}

case class CheckResult(satisfied: Boolean)
