/*
 * Copyright 2018 University of Rostock
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package sessl.verification

import sessl.AbstractObservation.Observable
import sessl.Trajectory

/**
  * A specification of a property of a simulation run.
  * Concrete simulation input and output can be checked to see if it satisfies the property.
  *
  * @author Tom Warnke
  */
trait PropertySpecification {

  /** Check whether the given data satisfies this property */
  def check(inputs: Map[String, Any], outputs: Map[Observable[_], Trajectory[_]]): Boolean

}
