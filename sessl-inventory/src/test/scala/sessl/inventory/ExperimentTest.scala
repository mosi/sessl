package sessl.inventory

import org.junit.Test

@Test class ExperimentTest {

  @Test def testRun(): Unit = {

    import sessl._

    val experiment = new Experiment with Observation {

      configs(
        Config("s" <~ 20, "S" <~ 40),
        Config("s" <~ 20, "S" <~ 60),
        Config("s" <~ 20, "S" <~ 80),
        Config("s" <~ 20, "S" <~ 100),
        Config("s" <~ 40, "S" <~ 60),
        Config("s" <~ 40, "S" <~ 80),
        Config("s" <~ 40, "S" <~ 100),
        Config("s" <~ 60, "S" <~ 80),
        Config("s" <~ 60, "S" <~ 100)
      )

      // default 1
      replications = 100

      // default 120
      stopTime = 120

      observe(AverageTotalCost)
      observe(AverageOrderingCost)
      observe(AverageHoldingCost)
      observe(AverageShortageCost)

      withReplicationsResult { r =>
        val va = r.variableAssignment.toMap
        print(f"(${va("s").asInstanceOf[Int]}%3d,${va("S").asInstanceOf[Int]}%3d)\t")
        print(f"${r.mean(AverageTotalCost)}%6.2f\t")
        print(f"${r.mean(AverageOrderingCost)}%6.2f\t")
        print(f"${r.mean(AverageHoldingCost)}%6.2f\t")
        print(f"${r.mean(AverageShortageCost)}%6.2f\n")
      }

    }

    execute(experiment)

  }

}
