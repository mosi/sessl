package sessl.inventory

import sessl.AbstractObservation.Observable
import sessl.util.SimpleObservation

trait Observation extends Experiment with SimpleObservation {

  case object AverageOrderingCost extends Observable[Double]

  case object AverageHoldingCost extends Observable[Double]

  case object AverageShortageCost extends Observable[Double]

  case object AverageTotalCost extends Observable[Double]

  override protected def processOutput(runId: Int, runOutput: RunOutput): Unit = {
    val (endTime, orderingCost, holdingCost, shortageCost) = runOutput

    observables.foreach {
      case AverageOrderingCost =>
        addValueFor(runId, AverageOrderingCost, (endTime, orderingCost / endTime))
      case AverageHoldingCost =>
        addValueFor(runId, AverageHoldingCost, (endTime, holdingCost / endTime))
      case AverageShortageCost =>
        addValueFor(runId, AverageShortageCost, (endTime, shortageCost / endTime))
      case AverageTotalCost =>
        addValueFor(runId, AverageTotalCost, (endTime, (orderingCost + holdingCost + shortageCost) / endTime))
    }
  }
}
