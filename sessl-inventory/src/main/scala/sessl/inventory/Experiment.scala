package sessl.inventory

import sessl.{AbstractExperiment, DynamicSimulationRuns}

import scala.collection.mutable
import scala.concurrent.Future
import scala.util.Random

class Experiment extends AbstractExperiment with DynamicSimulationRuns {

  override type RunOutput = (Double, Double, Double, Double)

  model = ""

  override protected def basicConfiguration(): Unit = {}

  override protected[sessl] def executeExperiment(): Unit = {
    startBlocking()
  }

  override protected def startSimulationRun(runId: Int, seed: Long, assignment: Map[String, AnyRef]): Future[RunOutput] = {

    val s = assignment.getOrElse("s", 0).asInstanceOf[Int]
    val S = assignment.getOrElse("S", 0).asInstanceOf[Int]

    import scala.concurrent.ExecutionContext.Implicits.global
    Future {
      Experiment.doSimulationRun(s, S, seed, fixedStopTime.getOrElse(120))
    }
  }
}

object Experiment {

  val K = 32 // setup cost in dollars
  val i = 3 // incremental cost in dollars per item

  val h = 1 // holding cost in dollars per item and month
  val pi = 5 // backlog cost in dollars per item per month

  val initialInventory = 60 // in items
  val initialBacklog = 0 // in items

  // mean number of orders per month
  val orderArrivalRate = 10

  val orderLeadTime = (0.5, 1.0) // uniform distribution in months

  val demandSizeDistribution = Map(
    1 -> 2.0 / 6,
    2 -> 3.0 / 6,
    3 -> 4.0 / 6,
    4 -> 6.0 / 6
  ).toList

  sealed trait Event {
    def time: Double
  }
  case class SupplyArrived(size: Int, time: Double) extends Event
  case class OrderPlaced(size: Int, time: Double) extends Event
  case class NewMonth(time: Double) extends Event

  val eventOrd: Ordering[Event] = Ordering.by[Event, Double](_.time).reverse

  def doSimulationRun(s: Int, S: Int, seed: Long, endTime: Double): (Double, Double, Double, Double) = {

    val rng = new Random(seed)

    def drawLeadTime(): Double = orderLeadTime._1 + rng.nextDouble() * (orderLeadTime._2 - orderLeadTime._1)

    def drawDemandSize(): Int = {
      val r = rng.nextDouble()
      demandSizeDistribution.dropWhile(_._2 < r).head._1
    }

    def drawInterArrivalTime(rate: Double): Double = {
      val r = rng.nextDouble()
      -Math.log(r) / rate
    }

    var time = 0.0
    var inventory = initialInventory
    var backlog = initialBacklog
    var orderingCost = 0.0
    var holdingCost = 0.0
    var shortageCost = 0.0
    val queue = mutable.PriorityQueue.empty[Event](eventOrd)

    queue += OrderPlaced(drawDemandSize(), time + drawInterArrivalTime(orderArrivalRate))
    queue += NewMonth(time + 1.0)

    while (time < endTime) {
      val event = queue.dequeue()

      val newTime = event.time
      val interval = newTime - time
      shortageCost += backlog * pi * interval
      holdingCost += inventory * h * interval
      time = newTime
      // println(s"handling event $event")

      event match {
        case SupplyArrived(size, _) =>
          if (size > backlog) {
            inventory += size - backlog
            backlog = 0
          } else {
            backlog -= size
          }
        case OrderPlaced(size, _) =>
          queue += OrderPlaced(drawDemandSize(), time + drawInterArrivalTime(orderArrivalRate))
          if (inventory > size) {
            inventory -= size
          } else {
            backlog += size - inventory
            inventory = 0
          }
        case NewMonth(_) =>
          if (inventory <= s) {
            val orderSize = S - inventory
            queue += SupplyArrived(orderSize, time + drawLeadTime())
            orderingCost += K + i * orderSize
          }
          queue += NewMonth(time + 1.0)
      }
    }

    (endTime, orderingCost, holdingCost, shortageCost)

  }
}
